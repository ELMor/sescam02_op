/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/


/*Esta clase contiene las constantes usadas en la aplicaci�n*/

package conf;

/**
 * <p>Clase: Constantes </p>
 * <p>Descripcion: Clase que contiene la definicion de las constantes usadas en la aplicaci�n</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 21/10/2002
 */

public class Constantes
{
      /**
        * Nombre del fichero de configuraci�n "db.properties" con los parametros de conexion y acceso a la Base de Datos
        */
	static public  final String FICHERO_CONFIGURACION = "db";
        /**
          * Valor con el que se almacena una solicitud de garantia en estado solicitada
          */
        static public final int EST_SOLICITADA=1;
        /**
          * Valor con el que se almacena una solicitud de garantia en estado aprobada
          */
        static public final int EST_APROBADA=2;
        /**
          * Valor con el que se almacena una solicitud de garantia en estado denegada
          */
        static public final int EST_DENEGADA=3;
        /**
          * Valor con el que se almacena una renuncia
          */
        static public final int EST_RENUNCIADA=4;
        /**
          * Cadenas de texto que muestran el estado de una solicitud
          */
        static public final String[] EST_TEXTO={"Solicitada","Aprobada","Denegada","Renunciada"};

}