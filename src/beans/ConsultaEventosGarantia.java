/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaEventosGarantia</p>
 * <p>Descripcion: Bean que realiza la consulta de eventos realicionados a una solicitud, aprobacion, denagacion y renuncia de garantia</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 * @author Jose Vicente Nieto-Marquez Fdez-Medina
 */
public class ConsultaEventosGarantia {
    private long evto_pk;
    private int resultCount;

    /**
     * Metodo que se encarga de la construccion de la consulta,  y de su ejecucion.
     * Utiliza objetos de la clase EventosGarantia para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos EventosGarantia, en la que cada uno atender� a un evento obtenido
     * en la consulta.
     * @return Vector de objetos de la clase EventosGarantia
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor Jose Vicente Nieto-M�rquez Fdez-Medina
     */
    public Vector resultado() throws SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec_eventos = new Vector();
        PreparedStatement stm;
        Pool ref_pool = null;

        String sentBusqueda = "select g.gar_pk,gt.gar_tram_pk,g.gar_monto,g.gar_motivo_rech,g.gar_cq,g.pac_nss,to_char(gt.gar_fechor,'DD/MM/YYYY') fechor,gt.gar_repleg_dni, gt.gar_repleg_apenom, gt.gar_operacion,gt.oficina_pk, ev.tpoevto_pk ";
        sentBusqueda += "from Evento ev,ses_garantia g, ses_garan_tram gt";
        sentBusqueda += "  where ev.evto_pk = ? and ev.gar_pk = g.gar_pk and g.gar_pk = gt.gar_pk order by gt.gar_tram_pk ASC";

        // System.out.println(sentBusqueda);
        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            stm = conn.prepareStatement(sentBusqueda);
            stm.setLong(1, this.evto_pk);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                resultCount++;

                EventosGarantia evtogar = new EventosGarantia();
                evtogar.evtoGarPk = rs.getLong(1);
                evtogar.evtoGarTramPk = rs.getLong(2);
                evtogar.evtoMonto = rs.getDouble(3);

                if (rs.getString(4) != null) {
                    evtogar.evtoMotivo = rs.getString(4);
                } else {
                    evtogar.evtoMotivo = "";
                }

                if (rs.getString(5) != null) {
                    evtogar.evtoGarCQ = rs.getString(5);
                } else {
                    evtogar.evtoGarCQ = "";
                }

                if (rs.getString(6) != null) {
                    evtogar.evtonss = rs.getString(6);
                } else {
                    evtogar.evtonss = "";
                }

                evtogar.evtoFecha = rs.getString(7);

                if (rs.getString(8) != null) {
                    evtogar.evtodni = rs.getString(8);
                } else {
                    evtogar.evtodni = "";
                }

                if (rs.getString(9) != null) {
                    evtogar.evtoapenom = rs.getString(9);
                } else {
                    evtogar.evtoapenom = "";
                }

                evtogar.evtoGarOperacion = rs.getLong(10);
                evtogar.evtoOficinaPk = rs.getLong(11);
                evtogar.evtoTipoEvto = rs.getLong(12);
                vec_eventos.addElement(evtogar);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_eventos;
    }

    /**
     * Obtiene el valor del atributo evto de la clase.
     * @return long, Evento Pk.
     * @since 12/12/2002
     * @author JVN
     */
    public long getEvtoPk() {
        return evto_pk;
    }

    /**
    * Asigna el parametro al atributo evto de la clase.
    * @param tpoevto : long, evto_pk de la tabla EVENTOS.
    * @since 12/12/2002
    * @author JVN
    */
    public void setEvtoPk(long evto) {
        if (evto != 0) {
            evto_pk = evto;
        }
    }

    public long DameOperacion() throws SQLException {
        long igar_operacion = 0;
        Connection conn = null;
        PreparedStatement stm;
        Pool ref_pool = null;

        String sentBusqueda = "select ev.gar_operacion ";
        sentBusqueda += "from Evento ev";
        sentBusqueda += "  where ev.evto_pk = ?";

        //   System.out.println(sentBusqueda);
        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            stm = conn.prepareStatement(sentBusqueda);
            stm.setLong(1, this.evto_pk);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                igar_operacion = rs.getLong(1);
            }

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return igar_operacion;
    }
}


/*
  public static void main(String args[]){
          ConsultaPacienteSolicitud cps=new ConsultaPacienteSolicitud();
          Solilcitud sol;
          Vector vp;
          cps.setNombre("nom");
          cps.setApellido1("");
          cps.setApellido2("");
          cps.setCip("");
          cps.setCentro("");
          cps.setHicl("");
          cps.setOrden("cip");
          try{
            vp=cpb.resultado();
            if (vp==null){
              System.out.println("No se trajo nada");
            }
            else        {
              Enumeration e = vp.elements();
              while (e.hasMoreElements()) {
                pac1=(Paciente)e.nextElement();
                System.out.println(pac1);
              }
              System.out.println("Cantidad de registros:"+cpb.getResultCount());
            }
          }
          catch(Exception e){
            System.out.println("Error al ejecutar la Select.");
            e.printStackTrace();
          }
          }*/
