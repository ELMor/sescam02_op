/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaMapeosProc </p>
 * <p>Description: Bean que realiza la consulta de los servicios susceptibles de ser mapeados automaticamente.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Felix Alberto Bravo Hermoso
 */
public class ConsultaMapeosServ {
    private long centro;
    private String orden = "CODSERV";
    private int resultCount;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 2000;

    /**
       * Metodo que se encarga de la construccion de la consulta de busqueda de servicios susceptibles de ser mapeados automaticamente.
       * Utiliza objetos de la clase ServMapeados para almacenar los campos resultantes, para cada registro obtenido.
       * Por ello, como resultado se obtendra un vector de objetos ServMapeados, en el que cada uno atender� a un registro resultado
       * de la consulta.
       * @return Vector de objetos de la clase ServMapeados.
       * @throws Exception
     */
    public Vector ConsultaMapAutoServ() throws Exception {
        // Validaci�n de parametros
        String sentBusqueda = new String("");

        /*    sentBusqueda= "SELECT TRIM(s.CODSERV) SRV_CEN_COD, TRIM(s.MAES_SERV) SERV_SESCAM,ss.SESSRV_DESC SERV_SESCAM_DESC "+
                          "FROM SERVICIOS s, SES_SERVICIOS ss "+
                          "WHERE TRIM(s.MAES_SERV)=TRIM(ss.SESSRV_COD) and s.CENTRO="+centro+" AND "+
                          "TRIM(s.CODSERV) NOT IN (SELECT TRIM(SRV_CEN_COD) FROM SES_SRV_MAP WHERE CENTRO="+centro+")";*/
        if (centro != 0) {
            sentBusqueda = "SELECT s.CODSERV SRV_CEN_COD, s.MAES_SERV SERV_SESCAM,ss.SESSRV_DESC SERV_SESCAM_DESC,s.CENTRO CENTRO " +
                "FROM SERVICIOS s, SES_SERVICIOS ss " +
                "WHERE TRIM(s.MAES_SERV)=TRIM(ss.SESSRV_COD) and s.CENTRO=" +
                centro + " AND " +
                "TRIM(s.CODSERV) NOT IN (SELECT TRIM(SRV_CEN_COD) FROM SES_SRV_MAP WHERE CENTRO=" +
                centro + ")";
        } else {
            sentBusqueda = "SELECT s.CODSERV SRV_CEN_COD, s.MAES_SERV SERV_SESCAM,ss.SESSRV_DESC SERV_SESCAM_DESC,s.CENTRO CENTRO " +
                "FROM SERVICIOS s, SES_SERVICIOS ss " +
                "WHERE TRIM(s.MAES_SERV)=TRIM(ss.SESSRV_COD) AND " +
                "TRIM(s.CODSERV) NOT IN (SELECT TRIM(SRV_CEN_COD) FROM SES_SRV_MAP)";
        }

        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_Proc = new Vector();

        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            sentBusqueda = sentBusqueda + " Order by " + orden;

            PreparedStatement st = conn.prepareStatement(sentBusqueda);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                ServMapeados srvmap = new ServMapeados();
                srvmap.CodServMap = rs.getString(1);
                srvmap.CodServSESCAM = rs.getString(2);
                srvmap.DescServSESCAM = rs.getString(3);
                srvmap.CodCentroMapeo = rs.getLong(4);

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_Proc.addElement(srvmap);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_Proc;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param centro :long, l_centro por el que filtramos la consulta.
     * @since 10/02/2003
     * @author JVN
     */
    public void setCentro(long l_centro) {
        if (l_centro > 0) {
            this.centro = l_centro;
        }
    }

    /**
     * Obtiene el parametro al atributo centro de la clase.
     * @return long, centro.
     * @since 10/02/2003
     * @author JVN
     */
    public long GetCentro() {
        return this.centro;
    }

    /**
     * Asigna el parametro al atributo orden de la clase.
     * @param Orden :String, sorden Ordenacion que lleva los registros obtenidos por la consulta.
     * @since 28/02/2003
     * @author ABH
     */
    public void setOrden(String sorden) {
        if (!sorden.equals("")) {
            this.orden = sorden;
        }
    }

    /**
     * Obtiene el parametro al atributo Orden de la clase.
     * @return String, Orden.
     * @since 28/02/2003
     * @author ABH
     */
    public String GetOrden() {
        return this.orden;
    }

    /*

      public static void main(String args[]){
              ConsultaNodosProcedimientos cp=new ConsultaNodosProcedimientos();
              NodoProcedimiento np;
              Vector vp=null;
              cp.setCodigo("12");
              cp.setDesc("");
              cp.setPkPadre(3);
              try{
              vp=cp.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cp.getCodigo()+"/ - HICL /"+cp.GetPkPadre()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            np=(NodoProcedimiento)e.nextElement();
                            System.out.println(np.NodoDesc);
                            }
                      }
              }
    */
}
