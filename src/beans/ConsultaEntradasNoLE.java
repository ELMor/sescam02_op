/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Enumeration;
import java.util.Vector;


/**
 * <p>Clase: ConsultaEntradasNoLE </p>
 * <p>Description: Bean que realiza la consulta de entradas en NOLESP.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Felix Alberto Bravo Hermoso
 * @version 1.0
 */
public class ConsultaEntradasNoLE {
    private String cip;
    private int resultCount;
    private long garpk;
    private int tipo = 0; //  0-Documentos (Consulta por nnolesp)
                          //  1-Demas (Consulta por cip);
    public final int MAX_RESULT_SET = 500;
    public Vector vec_EntradasNoLE;

    /**
     * Contructor de la clase ConsultaEntradaLE
     */
    public ConsultaEntradasNoLE() {
        vec_EntradasNoLE = new Vector();
    }

    /**
     * Dados los parametros de entrada localiza una entrada en la lista de Espera.
     * @param pCentro  long , Centro.
     * @param pNOrden  long , Norden.
     * @param cq  String, Indica si el registro es de LESP ("Q") o de LESPCEX ("C").
     * @return devuelve un objeto EntradaLE si lo encuentra y sino null.
     * @since 15/10/2002
     * @author IEC
     */
    public EntradaNoLE existeEntrada(long Nnolesp) {
        EntradaNoLE entnoLE;

        if (vec_EntradasNoLE == null) {
            return null;
        }

        Enumeration e = vec_EntradasNoLE.elements();

        while (e.hasMoreElements()) {
            entnoLE = (EntradaNoLE) e.nextElement();

            if (entnoLE.fNnolesp == Nnolesp) {
                return entnoLE;
            }
        }

        //Si no se encontro devuelve -1
        return null;
    }

    /**
     * Metodo que convierte el parametro si es nulo a cadena vacia.
     * @param str ,String, cadena para forzar no nulo.
     * @return String, "" si es nulo y el mismo paramtro si no los es.
     * @since 25/11/2002
     * @author Felix Alberto Bravo Hermoso
     */
    private String ForzarNoNulo(String str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    }

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de registros de Lista de Espera y de su ejecucion.
     * Utiliza objetos de la clase EntradasLE para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos EntradasLE, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase EntradasLE.
     * @throws Exception
     * @since 15/10/2002
     * @author IEC
     */
    public Vector resultado() throws Exception {
        vec_EntradasNoLE.clear();

        // Validaci�n de parametros
        Connection conn = null;
        Pool ref_pool = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        PreparedStatement st;
        ResultSet rs;
        String sentBusqueda =
            
            /*         "select nnolesp,centro,nhc,apellido1 ape1,apellido2 ape2,cip,nombre,domicilio,Nvl (to_char(fecha_nac,'DD/MM/YYYY') , 'S/D') fecha_nac, "+
                     "gar_oficina,poblacion,provincia,telefono,gar_repleg_apenom,gar_repleg_dni,descentrolarga descentro, Nvl (to_char(gar_fechor,'DD/MM/YYYY') , 'S/D') fecha  "+
                     "from nolesp, centrosescam "+
                     "where centro=codcentro ";*/
            "select l.nnolesp,l.centro,l.nhc,l.apellido1 ape1,l.apellido2 ape2,cip,l.nombre,domicilio,Nvl (to_char(fecha_nac,'DD/MM/YYYY'),'SD') fecha_nac, " +
            "gar_oficina,poblacion,p.provincia,telefono,gar_repleg_apenom,gar_repleg_dni,descentrolarga descentro,Nvl (to_char(gar_fechor,'DD/MM/YYYY'),'SD') fecha, " +
            "o.oficina_nombre,su.nombre||' '||su.apellido1||' '||su.apellido2 ofi_coordinador,p.provincia ofi_prov, l.gar_mot_rechazo motrechazo, prestacion, nss  " +
            "from nolesp l, centrosescam c, ses_oficina o, sys_usu su, ses_provincias p , ses_localidades loc " +
            "where CENTRO=CODCENTRO and " + "o.OFICINA_PK=l.GAR_OFICINA and " +
            "o.PK_LOCALIDAD = loc.PK_LOCALIDAD and " +
            "su.SYSLOGIN (+)= o.SYSLOGIN and " +
            "loc.PROVINCIA_PK = p.PROVINCIA_PK ";

        // Si el tipo es 0 buscamos por nnolesp(garpk) para los documentos,
        // sino buscamos por cip para obtener todas las entradas en nolesp.
        if (tipo == 0) {
            sentBusqueda += " and nnolesp=?";
        } else {
            sentBusqueda += " and cip=?";
        }

        //System.out.println(sentBusqueda);
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            st = conn.prepareStatement(sentBusqueda);

            if (tipo == 0) {
                st.setLong(1, garpk);
            } else {
                st.setString(1, cip);
            }

            rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                EntradaNoLE entnoLE = new EntradaNoLE();
                entnoLE.setFNnolesp(rs.getLong("nnolesp"));
                entnoLE.setFCentro(rs.getLong("centro"));
                entnoLE.setFHicl(rs.getLong("nhc"));
                entnoLE.setFApellido1(ForzarNoNulo(rs.getString("ape1")));
                entnoLE.setFApellido2(ForzarNoNulo(rs.getString("ape2")));
                entnoLE.setFCip(ForzarNoNulo(rs.getString("cip")));
                entnoLE.setFNombre(ForzarNoNulo(rs.getString("nombre")));
                entnoLE.setFDomicilio(ForzarNoNulo(rs.getString("domicilio")));
                entnoLE.setFFechaNac(ForzarNoNulo(rs.getString("fecha_nac")));
                entnoLE.setFOficina(rs.getLong("gar_oficina"));
                entnoLE.setFPoblacion(ForzarNoNulo(rs.getString("poblacion")));
                entnoLE.setFProvincia(ForzarNoNulo(rs.getString("provincia")));
                entnoLE.setFTelefono(ForzarNoNulo(rs.getString("telefono")));
                entnoLE.setFResp_apenom(ForzarNoNulo(rs.getString(
                            "gar_repleg_apenom")));
                entnoLE.setFResp_dni(ForzarNoNulo(rs.getString("gar_repleg_dni")));
                entnoLE.setFDesCentro(ForzarNoNulo(rs.getString("descentro")));
                entnoLE.setFFecha(ForzarNoNulo(rs.getString("fecha")));
                entnoLE.setFOfi_nombre(ForzarNoNulo(rs.getString(
                            "oficina_nombre")));
                entnoLE.setFOfi_coordinador(ForzarNoNulo(rs.getString(
                            "ofi_coordinador")));
                entnoLE.setFOfi_provincia(ForzarNoNulo(rs.getString("ofi_prov")));
                entnoLE.setFMotivo_rechazo(ForzarNoNulo(rs.getString(
                            "motrechazo")));
                entnoLE.setFPrestacion(ForzarNoNulo(rs.getString("prestacion")));
                entnoLE.setFNSS(ForzarNoNulo(rs.getString("nss")));

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_EntradasNoLE.addElement(entnoLE);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            System.out.println(e);
            vec_EntradasNoLE.clear();

            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_EntradasNoLE;
    }

    /**
     * Asigna el parametro al atributo cip de la clase.
     * @param pCip String,CIP.
     * @since 15/10/2002
     * @author IEC
     */
    public void setCip(String pCip) {
        if (pCip != null) {
            this.cip = pCip;
        }
    }

    /**
     * Obtiene el valor del atributo centro de la clase.
     * @return String,CIP.
     * @since 15/10/2002
     * @author IEC
     */
    public String getCip() {
        return cip;
    }

    /**
     * Obtiene el valor del atributo resultCount de la clase.
     * @return int,
     * @since 15/10/2002
     * @author IEC
     */
    public int getResultCount() {
        return resultCount;
    }

    public static void main(String[] args) {
        ConsultaEntradasNoLE cpb = new ConsultaEntradasNoLE();
        EntradaNoLE entnoLE;
        Vector vp = null;
        cpb.setCip("#1002049");
        cpb.setGarpk(9);

        try {
            vp = cpb.resultado();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (vp == null) {
            System.out.println("No hay registros con las condiciones: Cip /" +
                cpb.getCip());
        } else {
            Enumeration e = vp.elements();

            while (e.hasMoreElements()) {
                entnoLE = (EntradaNoLE) e.nextElement();
                System.out.println("Fecha:" + entnoLE.getFFecha());
                System.out.println("Fecha nacimiento:" +
                    entnoLE.getFFechaNac());
                System.out.println("Oficina Nombre:" +
                    entnoLE.getFOfi_nombre());
                System.out.println("Oficina Coordinador:" +
                    entnoLE.getFOfi_coordinador());
                System.out.println("Oficina Provincia:" +
                    entnoLE.getFOfi_provincia());
                System.out.println("Oficina Provincia:" +
                    entnoLE.getFMotivo_rechazo());
            }

            System.out.println("Cantidad de registros:" + cpb.getResultCount());
        }
    }

    public long getGarpk() {
        return garpk;
    }

    public void setGarpk(long garpk) {
        this.garpk = garpk;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
