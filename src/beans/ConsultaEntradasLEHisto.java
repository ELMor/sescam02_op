/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Enumeration;
import java.util.Vector;


/**
 * <p>Clase: ConsultaEntradasLEDet </p>
 * <p>Description: Bean que realiza la consulta de la antiguedad de las entradas en la lista de espera.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class ConsultaEntradasLEHisto {
    
    private String cip;
    private int resultCount;
    public final int MAX_RESULT_SET = 500;
    public Vector vec_EntradasLEHisto;

    /**
     * Contructor de la clase ConsultaEntradaLE
     */
    public ConsultaEntradasLEHisto() {
        vec_EntradasLEHisto = new Vector();
    }

    /**
     * Dados los parametros de entrada localiza una entrada en la lista de Espera.
     * @param pCentro  long , Centro.
     * @param pNOrden  long , Norden.
     * @param cq  String, Indica si el registro es de LESP ("Q") o de LESPCEX ("C").
     * @return devuelve un objeto EntradaLE si lo encuentra y sino null.
     * @since 15/10/2002
     * @author IEC
     */
    public EntradaLEHisto dameEntrada(long pCentro, long pNOrden, String cq) {
        EntradaLEHisto entLEh;

        if (vec_EntradasLEHisto == null) {
            return null;
        }

        Enumeration e = vec_EntradasLEHisto.elements();

        while (e.hasMoreElements()) {
            entLEh = (EntradaLEHisto) e.nextElement();

            if ((entLEh.fCentro == pCentro) && (entLEh.fNorden == pNOrden) &&
                    (entLEh.fTipoCQ.equals(cq))) {
                return entLEh;
            }
        }

        //Si no se encontro devuelve -1
        return null;
    }
    
    
    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de registros de Lista de Espera y de su ejecucion.
     * Utiliza objetos de la clase EntradasLE para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos EntradasLE, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase EntradasLE.
     * @throws Exception
     * @since 15/10/2002
     * @author IEC
     */
    public Vector resultado() throws Exception {
        vec_EntradasLEHisto.clear();

        // Validaci�n de parametros
        if ((cip == null) || (cip.equals(""))) {
            return (null);
        }

        Connection conn = null;
        Pool ref_pool = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        PreparedStatement st;
        ResultSet rs;
        String sentBusqueda = "select  distinct l.cip cip, "+
							  	"l.centro centro, "+
								"l.norden norden, "+
								"l.cpr1 cproc, "+
								"l.dpr1 dproc, "+
								"l.cdi1 cdiag,"+
								"l.ddi1 ddiag,"+
								"l.hicl hicl, "+
								"l.moti moti, "+ 
								"l.fsal fsal, "+
								"l.LINK LINK, "+
								"'Q' tipo_cq, "+
								"l.catalog spac, "+
								"l.secc serv " + 
								"FROM lesp_histo l,lesp_histo ld "+ 
								"WHERE ld.cip =? AND "+ 
  								"l.norden=legase.CLAVE_INICIAL_HISTO(ld.CENTRO,1,ld.norden) and "+ 
  								"l.centro=ld.centro "+
								"UNION ALL "+
								"SELECT cip, "+
								"cega centro, ncita norden, prestaci cproc, prestaci_des||' '||servproc_des dproc, prestaci cdiag, prestaci_des||' '||servproc_des ddiag,hicl, motisali moti, fsalida fsal, "+ 
  								"0 LINK,l.IDENTPRES tipo_cq , l.catalog spac, l.servreal serv "+
								"FROM lespcex_histo l "+
								"WHERE CIP=? ";
		
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            st = conn.prepareStatement(sentBusqueda);
            st.setString(1, cip);
            st.setString(2, cip);
            rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                EntradaLEHisto entLEh = new EntradaLEHisto();
                entLEh.fNorden = rs.getInt("norden");
                entLEh.fCentro = rs.getInt("centro");
                entLEh.fHicl = rs.getInt("hicl");
                entLEh.fCProc = rs.getString("cproc");
                entLEh.fCip = rs.getString("cip");
                entLEh.fDProc = rs.getString("dproc");
                entLEh.fCDiag = rs.getString("cdiag");
                entLEh.fDDiag = rs.getString("ddiag");
                entLEh.fSalida = rs.getString("fsal");
                try {
                    entLEh.fMotivo = rs.getInt("moti");
                } catch (Exception ei) {
                    entLEh.fMotivo = 0;
                }
                entLEh.fLink = rs.getInt("link");
                entLEh.fTipoCQ = rs.getString("tipo_cq");
                entLEh.fServicio = rs.getString("serv");

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_EntradasLEHisto.addElement(entLEh);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            vec_EntradasLEHisto.clear();

            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }
        return vec_EntradasLEHisto;
    }

    
    /**
     * Asigna el parametro al atributo cip de la clase.
     * @param pCip String,CIP.
     * @since 15/10/2002
     * @author IEC
     */
    public void setCip(String pCip) {
        if (pCip != null) {
            this.cip = pCip;
        }
    }

    /**
     * Obtiene el valor del atributo centro de la clase.
     * @return String,CIP.
     * @since 15/10/2002
     * @author IEC
     */
    public String getCip() {
        return cip;
    }

    /**
     * Obtiene el valor del atributo resultCount de la clase.
     * @return int,
     * @since 15/10/2002
     * @author IEC
     */
    public int getResultCount() {
        return resultCount;
    }

    public static void main(String[] args) {
        ConsultaEntradasLE cpb = new ConsultaEntradasLE();
        EntradaLE entLE;
        Vector vp = null;
        cpb.setCip("#1002049");

        try {
            vp = cpb.resultado();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (vp == null) {
            System.out.println("No hay registros con las condiciones: Cip /" +
                cpb.getCip());
        } else {
            Enumeration e = vp.elements();

            while (e.hasMoreElements()) {
                entLE = (EntradaLE) e.nextElement();
            }

            System.out.println("Cantidad de registros:" + cpb.getResultCount());
        }
    }
}
