/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.exceptions.*;

import isf.negocio.*;

import isf.persistencia.SesClasificaciones;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaEstadisticaLECEX</p>
 * <p>Descripcion: Bean que realiza diversas estadisticas sobre la tabla Lista de Espera de CEX</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 * @author Jos� Vicente Nieto-Marquez
 */
public class ConsultaEstadisticaLECEX {
    private String periodo;
    private int tramos;
    private int resultCount;
    public final int MAX_RESULT_SET = 10000; //Maximo n� de registros resultantes por consulta.

    /**
     * Metodo que se encarga de la construccion de la consulta para los pacientes por tramos, centros y para los procedimientos que son tecnicas.
     * Utiliza objetos de la clase DatosEstadisticasLECEX para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos DatosEstadisticasLECEX, en la que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase DatosEstadisticasLECEX.
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor Jose Vicente Nieto
     */
    public Vector ConsultaTramoCentroTecnicas()
        throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec_estadistica = new Vector();
        Pool ref_pool = null;
        PreparedStatement stm;

        String sentBuscarTramoCentro = " select des.descentro, ";

        for (int int_pos = 0; int_pos < tramos; int_pos++) {
            int pos = int_pos + 1;
            sentBuscarTramoCentro = sentBuscarTramoCentro +
                " sum(decode(c.POSICION," + pos + ",EST_CNT_PAC,0)) tramo" +
                pos + ", ";
        }

        sentBuscarTramoCentro = sentBuscarTramoCentro +
            "  sum(EST_CNT_PAC) Total, " +
            "  sum(EST_CNT_DIAS)/sum(EST_CNT_PAC) media " +
            "  from ses_estadisticas c,centrosescam des " +
            "  where c.EST_TLISTA = 'C' " + "  and c.EST_PERIODO = ? " +
            "  and c.CODCENTRO = des.CODCENTRO " + "  group by des.descentro ";

        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            stm = conn.prepareStatement(sentBuscarTramoCentro);
            stm.setString(1, periodo);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                resultCount++;

                DatosEstadisticasLECEX estadistica_centro = new DatosEstadisticasLECEX();
                estadistica_centro.DatoGroupby = rs.getString(1);
                estadistica_centro.tramos = new int[tramos];

                for (int int_pos = 0; int_pos < tramos; int_pos++) {
                    estadistica_centro.tramos[int_pos] = rs.getInt(int_pos + 2);
                }

                estadistica_centro.pacientes = rs.getInt(tramos + 2);
                estadistica_centro.demoramedia = rs.getDouble(tramos + 3);
                vec_estadistica.addElement(estadistica_centro);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_estadistica;
    }

    /**
     * Metodo que se encarga de la construccion de la consulta para los pacientes por tramos, centros y para los procedimientos que son consultas .
     * Utiliza objetos de la clase DatosEstadisticasLECEX para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos DatosEstadisticasLECEX, en la que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase DatosEstadisticasLECEX.
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor Jose Vicente Nieto
     */
    public Vector ConsultaTramoCentroConsultas()
        throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec_estadistica = new Vector();
        Pool ref_pool = null;
        PreparedStatement stm;

        String sentBuscarTramoCentro = " select des.descentro, ";

        for (int int_pos = 0; int_pos < tramos; int_pos++) {
            int pos = int_pos + 1;
            sentBuscarTramoCentro = sentBuscarTramoCentro +
                " sum(decode(c.POSICION," + pos + ",EST_CNT_PAC,0)) tramo" +
                pos + ", ";
        }

        sentBuscarTramoCentro = sentBuscarTramoCentro +
            "  sum(EST_CNT_PAC) Total, " +
            "  sum(EST_CNT_PAC)                      Total, " +
            "  sum(EST_CNT_DIAS)/sum(EST_CNT_PAC) media " +
            "  from ses_estadisticas c,centrosescam des " +
            "  where c.EST_TLISTA = 'C' " + "  and c.EST_PERIODO = ? " +
            "  and c.CODCENTRO = des.CODCENTRO " + "  group by des.descentro ";

        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            stm = conn.prepareStatement(sentBuscarTramoCentro);
            stm.setString(1, periodo);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                resultCount++;

                DatosEstadisticasLECEX estadistica_centro = new DatosEstadisticasLECEX();
                estadistica_centro.DatoGroupby = rs.getString(1);
                estadistica_centro.tramos = new int[tramos];

                for (int int_pos = 0; int_pos < tramos; int_pos++) {
                    estadistica_centro.tramos[int_pos] = rs.getInt(int_pos + 2);
                }

                estadistica_centro.pacientes = rs.getInt(tramos + 2);
                estadistica_centro.demoramedia = rs.getDouble(tramos + 3);
                vec_estadistica.addElement(estadistica_centro);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_estadistica;
    }

    /**
      * Metodo que se encarga de la construccion de la consulta para los pacientes por tramos, servicios los procedimientos que son consultas .
      * Utiliza objetos de la clase DatosEstadisticasLECEX para almacenar los campos resultantes, para cada registro obtenido.
      * Por ello, como resultado se obtendra un vector de objetos DatosEstadisticasLECEX, en la que cada uno atender� a un registro resultado
      * de la consulta.
      * @return Vector de objetos de la clase DatosEstadisticasLECEX.
      * @throws SQLException
      * @throws ExceptionSESCAM
      * @autor Jose Vicente Nieto
     */
    public Vector ConsultaTramoServicioConsultas()
        throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec_estadistica = new Vector();
        Pool ref_pool = null;
        PreparedStatement stm;

        String sentBuscarServicioConsultas = " select serv.SESSRV_DESC ";

        for (int int_pos = 0; int_pos < tramos; int_pos++) {
            int pos = int_pos + 1;
            sentBuscarServicioConsultas = sentBuscarServicioConsultas +
                " sum(decode(c.POSICION," + pos + ",EST_CNT_PAC,0)) tramo" +
                pos + ", ";
        }

        sentBuscarServicioConsultas = sentBuscarServicioConsultas +
            "  sum(EST_CNT_PAC) Total, " +
            " sum(EST_CNT_DIAS)/sum(EST_CNT_PAC) media " +
            " from ses_estadisticas c,ses_srv_map srmp, ses_servicios serv " +
            " where c.EST_TLISTA = 'C' " + " and c.EST_PERIODO = ? " +
            " and c.SERV_CEN_COD = srmp.SRV_CEN_COD and srmp.SESSRV_COD = serv.SESSRV_COD " +
            " group by serv.SESSRV_DESC ";

        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            stm = conn.prepareStatement(sentBuscarServicioConsultas);
            stm.setString(1, periodo);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                resultCount++;

                DatosEstadisticasLECEX estadistica_serv = new DatosEstadisticasLECEX();
                estadistica_serv.DatoGroupby = rs.getString(1);
                estadistica_serv.tramos = new int[tramos];

                for (int int_pos = 0; int_pos < tramos; int_pos++) {
                    estadistica_serv.tramos[int_pos] = rs.getInt(int_pos + 2);
                }

                estadistica_serv.pacientes = rs.getInt(tramos + 2);
                estadistica_serv.demoramedia = rs.getDouble(tramos + 3);

                vec_estadistica.addElement(estadistica_serv);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_estadistica;
    }

    public Vector ConsultaTramoTecnicas() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec_estadistica = new Vector();
        Pool ref_pool = null;

        String sentBuscarTecnicas = " select sesclas_desc, ";

        for (int int_pos = 0; int_pos < tramos; int_pos++) {
            int pos = int_pos + 1;
            sentBuscarTecnicas = sentBuscarTecnicas +
                " sum(decode(c.POSICION," + pos + ",EST_CNT_PAC,0)) tramo" +
                pos + ", ";
        }

        sentBuscarTecnicas = sentBuscarTecnicas +
            "        sum(pacientes)                      Total, " +
            "        sum(pacientes*espera)/sum(pacientes) media " +
            "    from ses_tramos t, AGRUPA_TECNICA_VIEW_CEX s " +
            "    where (t.inicio<=s.espera or t.inicio is null) " +
            "          and (t.fin>=s.espera  or t.fin is null) and t.CODTRAMO=3 " +
            "    group by sesclas_desc ";

        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sentBuscarTecnicas);

            while (rs.next()) {
                resultCount++;

                DatosEstadisticasLECEX estadistica_tec = new DatosEstadisticasLECEX();
                estadistica_tec.DatoGroupby = rs.getString(1);
                estadistica_tec.tramos = new int[tramos];

                for (int int_pos = 0; int_pos < tramos; int_pos++) {
                    estadistica_tec.tramos[int_pos] = rs.getInt(int_pos + 2);
                }

                estadistica_tec.pacientes = rs.getInt(tramos + 2);
                estadistica_tec.demoramedia = rs.getDouble(tramos + 3);

                vec_estadistica.addElement(estadistica_tec);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_estadistica;
    }

    public Vector DameFamilias() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec = new Vector();
        Pool ref_pool = null;

        String sentBuscarTecnicas = " select sesclas_pk,sesclas_desc " +
            "    from ses_clasificaciones " +
            "    where sesclas_pkpadre is null ";

        // Si no tiene condiciones de busqueda no se hace la consulta
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sentBuscarTecnicas);

            while (rs.next()) {
                resultCount++;

                SesClasificaciones clas = new SesClasificaciones();
                clas.setSesclasPk(rs.getLong(1));
                clas.setSesclasDesc(rs.getString(2));
                vec.addElement(clas);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec;
    }

    /**
    * Asigna el parametro al atributo periodo de la clase.
    * @param sper :String, periodo por el que filtramos la consulta.
    * @since 25/02/2003
    * @author JVN
    */
    /**
     * Obtiene el parametro al atributo periodo de la clase.
     * @return String, periodo del procedimiento.
     * @since 25/02/2003
     * @author JVN
     */
    public String GetPeriodo() {
        return this.periodo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
}
