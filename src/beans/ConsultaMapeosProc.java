/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaMapeosProc </p>
 * <p>Description: Bean que realiza la consulta de los procedimientos susceptibles de ser mapeados automaticamente.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Jose Vicente Nieto-Marquez Fdez-Medina
 * @version 1.0
 */
public class ConsultaMapeosProc {
    private long centro;
    private String cod_sescam = "";
    private String descrip = "";
    private String cod_mapeo = "";
    private String descripmapeo = "";
    private String orden = "COD_SESCAM";
    private int tipo;
    private int tipomapeo;
    private String codtipoMapeo = "";
    private String codtipoSescam = "";
    private int resultCount;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 2000;

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de procedimientos mapeados.
     * Utiliza objetos de la clase ProcMapeados para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos ProcMapeados, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase ProcMapeados.
     * @throws Exception
     */
    public Vector MapeosProcedimientos() throws Exception {
        String sentWhere = "";
        String sentClausula = " WHERE ";

        // Validaci�n de parametros
        String sentBusqueda = new String("");
        sentBusqueda = " select * from mapeo_procs ";

        if (centro > 0) {
            sentBusqueda = sentBusqueda + sentClausula + " centro = " + centro;
            sentClausula = " AND ";
        }

        if ((cod_sescam != null) && (!cod_sescam.equals(""))) {
            cod_sescam = "%" + cod_sescam + "%";
            sentBusqueda = sentBusqueda + sentClausula + " cod_sescam like '" +
                cod_sescam + "'";
            sentClausula = " AND ";
        }

        if ((descrip != null) && (!descrip.equals(""))) {
            descrip = "%" + descrip + "%";
            sentBusqueda = sentBusqueda + sentClausula + " descrip like '" +
                descrip + "'";
            sentClausula = " AND ";
        }

        if ((cod_mapeo != null) && (!cod_mapeo.equals(""))) {
            cod_mapeo = "%" + cod_mapeo + "%";
            sentBusqueda = sentBusqueda + sentClausula + " cod_mapeo like '" +
                cod_mapeo + "'";
            sentClausula = " AND ";
        }

        if ((codtipoMapeo != null) && (!codtipoMapeo.equals(""))) {
            sentBusqueda = sentBusqueda + sentClausula + " codtipomapeo = '" +
                codtipoMapeo + "'";
            sentClausula = " AND ";
        }

        if ((codtipoSescam != null) && (!codtipoSescam.equals("")) &&
                (!codtipoSescam.equals("0"))) {
            sentBusqueda = sentBusqueda + sentClausula + " codtipo = '" +
                codtipoSescam + "'";
            sentClausula = " AND ";
        }

        if ((descripmapeo != null) && (!descripmapeo.equals(""))) {
            descripmapeo = "%" + descripmapeo + "%";
            sentBusqueda = sentBusqueda + sentClausula +
                " descripmapeo like '" + descripmapeo + "'";
        }

        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_Proc = new Vector();

        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            sentBusqueda = sentBusqueda + " Order by " + orden;

            PreparedStatement st = conn.prepareStatement(sentBusqueda);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                ProcedimientosMapeados proc = new ProcedimientosMapeados();
                proc.CodProcSESCAM = rs.getString(1);
                proc.ProcDesc = rs.getString(2);
                proc.CodProcMapeado = rs.getString(3);
                proc.ProcDescMapeado = rs.getString(4);
                proc.CodTipoSescam = rs.getString(5);
                proc.Tipo = rs.getInt(6);
                proc.CodTipoMapeado = rs.getString(7);
                proc.centro = rs.getLong(8);

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_Proc.addElement(proc);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_Proc;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param centro :long, l_centro por el que filtramos la consulta.
     * @since 10/02/2003
     * @author JVN
     */
    public void setCentro(long l_centro) {
        if (l_centro > 0) {
            this.centro = l_centro;
        }
    }

    /**
     * Obtiene el parametro al atributo centro de la clase.
     * @return long, centro.
     * @since 10/02/2003
     * @author JVN
     */
    public long GetCentro() {
        return this.centro;
    }

    /**
     * Asigna el parametro al atributo tipo de la clase.
     * @param tipo :int, i_tipo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setTipo(int i_tipo) {
        if (i_tipo > 0) {
            this.tipo = i_tipo;
        }
    }

    /**
     * Obtiene el parametro al atributo tipo de la clase.
     * @return int, tipo.
     * @since 17/02/2003
     * @author JVN
     */
    public int Gettipo() {
        return this.tipo;
    }

    /**
     * Asigna el parametro al atributo cod_sescam de la clase.
     * @param cod_sescam :String, ccodigo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setCodigoSescam(String ccodigo) {
        if (!ccodigo.equals("")) {
            this.cod_sescam = ccodigo;
        }
    }

    /**
     * Obtiene el parametro al atributo CodSescam de la clase.
     * @return String, cod_sescam.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetCodigoSescam() {
        return this.cod_sescam;
    }

    /**
     * Asigna el parametro al atributo descrip de la clase.
     * @param descrip :String, sdesc por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setDescripSescam(String sdesc) {
        if (!sdesc.equals("")) {
            this.descrip = sdesc;
        }
    }

    /**
     * Obtiene el parametro al atributo descrip de la clase.
     * @return String, descripcion del procedimiento.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetDescripSescam() {
        return this.descrip;
    }

    /**
     * Asigna el parametro al atributo cod_mapeo de la clase.
     * @param cod_mapeo :String, ccodigo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setCodigoMapeo(String ccodigo) {
        if (!ccodigo.equals("")) {
            this.cod_mapeo = ccodigo;
        }
    }

    /**
     * Obtiene el parametro al atributo CodMapeo de la clase.
     * @return String, cod_mapeo.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetCodigoMapeo() {
        return this.cod_mapeo;
    }

    /**
     * Asigna el parametro al atributo codtipoSescam de la clase.
     * @param codtipoSescam :String, ctipo por el que filtramos la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    /**
     * Obtiene el parametro al atributo codtipoSescam de la clase.
     * @return String, codtipoSescam.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetTipoSescam() {
        return this.codtipoSescam;
    }

    /**
     * Asigna el parametro al atributo orden de la clase.
     * @param Orden :String, sorden Ordenacion que lleva los registros obtenidos por la consulta.
     * @since 17/02/2003
     * @author JVN
     */
    public void setOrden(String sorden) {
        if (!sorden.equals("")) {
            this.orden = sorden;
        }
    }

    /**
     * Obtiene el parametro al atributo Orden de la clase.
     * @return String, Orden.
     * @since 17/02/2003
     * @author JVN
     */
    public String GetOrden() {
        return this.orden;
    }

    /**
     * Obtiene el parametro al atributo tipo_mapeo de la clase
     * @return tipo_mapeo
     * @since 19/02/2003
     * @author ABH
     */
    /**
     * Asigna el parametro al atributo tipo_mapeo de la clase
     * @param tipo_mapeo
     * @since 19/02/2003
     * @author ABH
     */
    public void setCodtipoMapeo(String codtipoMapeo) {
        this.codtipoMapeo = codtipoMapeo;
    }

    public void setCodtipoSescam(String codtipoSescam) {
        this.codtipoSescam = codtipoSescam;
    }

    public void setTipomapeo(int tipo_mapeo) {
        this.tipomapeo = tipo_mapeo;
    }

    public int getTipomapeo() {
        return tipomapeo;
    }

    public void setDescripmapeo(String descripmapeo) {
        this.descripmapeo = descripmapeo;
    }

    /*
      public static void main(String args[]){
              ConsultaNodosProcedimientos cp=new ConsultaNodosProcedimientos();
              NodoProcedimiento np;
              Vector vp=null;
              cp.setCodigo("12");
              cp.setDesc("");
              cp.setPkPadre(3);
              try{
              vp=cp.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cp.getCodigo()+"/ - HICL /"+cp.GetPkPadre()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            np=(NodoProcedimiento)e.nextElement();
                            System.out.println(np.NodoDesc);
                            }
                      }*/

    /***************** PRUEBA TRADUCCION MAPEO *********************/

    /*
    ConsultaMapeosProc cmp=new ConsultaMapeosProc();
    cmp.setCentro(4509);
    cmp.setCodigoMapeo("04.81");
    cmp.setCodtipoMapeo("Q");
    TraduccionProc tp = new TraduccionProc();
    try {
      tp = (TraduccionProc) cmp.Traducir();
    }catch (Exception e){}
    System.out.println("Codigo Sescam:"+tp.codigoSESCAM);
    System.out.println("Desc Sescam:"+tp.descSESCAM);
    System.out.println("Tipo SESCAM:"+tp.tipoSESCAM);
    System.out.println("Monto:"+tp.monto);

    }
    */
}
