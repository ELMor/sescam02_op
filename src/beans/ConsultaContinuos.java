package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import isf.util.log.Log;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaContinuos </p>
 * <p>Description: Bean que realiza la consulta de tramos en lista de espera.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author F�lix Alberto Bravo Hermoso
 * @version 1.0
 */
public class ConsultaContinuos {
    private String centro;
    private String norden;
    private String cq;
    private int tipo;
    private int resultCount;
    private NegEvento negevent;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 500;

    /**
      * Instancia el objeto de clase NegEvento
      * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
      * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
      */
    public void SetNegEvento(NegEvento evento) {
        negevent = evento;
    }

    public Vector resultado() throws Exception {
        Centros c = new Centros();
        Log fichero_log = null;
        int nfilas = 0;

        // Validaci�n de parametros
        if ((centro == null) || (centro.equals(""))) {
            return (null);
        }

        if ((norden == null) || (norden.equals(""))) {
            return (null);
        }

        if (cq == null) {
            return (null);
        } else if (cq.equals("Q")) {
            tipo = 1;
        } else {
            tipo = 2;
        }

        String sentBusqueda = new String("");
        String sentBusquedaCount = new String("");
        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_Continuos = new Vector();

        /*  sentBusquedaCount="select count(*) nfilas "+
                       "from continuos where centro=? and claveinicial=? and tipo=? ";
          ref_pool = Pool.getInstance();
          conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          try{
            conn.setAutoCommit(false);
            PreparedStatement st=conn.prepareStatement(sentBusquedaCount);
            st.setInt(1,Integer.parseInt(centro));
            st.setInt(2,Integer.parseInt(norden));
            st.setInt(3,tipo);
            ResultSet rs=st.executeQuery();
            while(rs.next()) {
              nfilas=(rs.getInt(1));
            }
            conn.commit();
            try {
                rs.close();
                st.close();
              }catch (Exception ex){}
            }catch(Exception e){
              try{conn.rollback();} catch(Exception esql){}
              e.printStackTrace();
              throw e;
            }
        */
        sentBusqueda = "select clave clave, " + "to_char(fi,'" + "DD/MM/YYYY" +
            "') fini , " + "to_char(ff,'" + "DD/MM/YYYY" + "') ffin, " +
            "spac situacion, " + "acu dias_acumulados " +
            "from continuos where centro=? and claveinicial=? and tipo=? and spac is not null " +
            "order by fi";
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            PreparedStatement st = conn.prepareStatement(sentBusqueda);

            /*try {
              if (negevent != null) {
                negevent.addVar(83,centro);
                negevent.addVar(84,norden);
                negevent.addVar(85,cq);
                negevent.guardar(conn);
              }
            }
            catch (Exception ex) {
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha realizado el evento de consulta de Continuos con centro � cega " + centro + " y norden o ncita " + norden +   " Error -> " + ex.toString());
            }*/
            st.setInt(1, Integer.parseInt(centro));
            st.setInt(2, Integer.parseInt(norden));
            st.setInt(3, tipo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                ContinuosDet CDet = new ContinuosDet();
                CDet.setClave(rs.getInt("clave"));

                if (rs.getString("fini") != null) {
                    CDet.setFecha_inicio(rs.getString("fini"));
                } else {
                    CDet.setFecha_inicio("");
                }

                if (rs.getString("ffin") != null) {
                    CDet.setFecha_fin(rs.getString("ffin"));
                } else {
                    CDet.setFecha_fin("");
                }

                if (rs.getString("situacion") != null) {
                    CDet.setSituacion(rs.getString("situacion"));
                } else {
                    CDet.setSituacion("EN LISTA");
                }

                CDet.setDias_acumulados(rs.getInt("dias_acumulados"));

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_Continuos.addElement(CDet);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_Continuos;
    }

    public String getCentro() {
        return centro;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    public String getCq() {
        return cq;
    }

    public void setCq(String cq) {
        this.cq = cq;
    }

    public String getNorden() {
        return norden;
    }

    public void setNorden(String norden) {
        this.norden = norden;
    }
}
