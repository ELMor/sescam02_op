/*

*   Copyright ( 1999 Uni�n Fenosa International Software Factory,

*   S.A.. All Rights Reserved. 

*   

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/

package isf.db;

import java.sql.*;
import conf.Constantes;


/** 
   Clase:      Conexion
   Autor:      JDI - 11/06/2001
   Prop�sito:  Objeto conexi�n para ser utilizado en el bean de entrada.jsp
*/ 
public class Conexion {
	
	// objeto conexi�n a la BD
	private Pool 		refPool		= null;
	private Connection 	conexion 	= null;
	
	/** 
   Clase:      Constructor
   Autor:      JDI - 11/06/2001
   Prop�sito:  Objeto conexi�n para ser utilizado en el bean de entrada.jsp
	*/ 
	public Conexion() {
		refPool = Pool.getInstance();
		conexion = refPool.getConnection(Constantes.FICHERO_CONFIGURACION);			
	}
	
	public Connection getConexion(){
		return conexion;
	}

	public void setConexion(Connection con){
		conexion = con;
	}

	public void cerrarConexion() 
	{
		refPool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
	}
}