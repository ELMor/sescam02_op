/*

*   Copyright ( 1999 Uni�n Fenosa International Software Factory,

*   S.A.. All Rights Reserved.

*

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/
package isf.db;

import java.io.*;

import java.sql.*;


/**
   Prop�sito: Tratamiento de los campos CLOBs en bd. Sql Server.
   @author PSN, JDZ
   @version 2.0
*/
public class CampoCLOBSqlServer extends CampoCLOB {
    /**
            Recupera el contenido de un campo CLOB
            @param                        OutputStream out         Stream por el que sale el CLOB
            @param                        Connection conexion conexi�n de BD a utilizar
            @param                        String Str_tabla                 Tabla de la que se que quiere leer
            @param                        String Str_campo                 nombre del campo que tiene el CLOB
            @param                        String Str_condicion         condicion que debe cumplir la fila que se quiere modificar
            @return                        void
            @exception                SQLException, Exception
            @since                        29-11-2000

    */
    public void obtenerCampoCLOB(Writer out, Connection conexion,
        String Str_tabla, String Str_campo, String Str_condicion)
        throws SQLException, Exception {
        Statement sentencia = null;
        ResultSet resultado = null;
        char[] buffer;
        int int_i = 0;
        int int_bytesLeidos = 0;
        InputStream CLOBInputStream = null;
        String Str_sentencia = null;

        Str_sentencia = "SELECT " + Str_campo + " FROM " + Str_tabla +
            " WHERE " + Str_condicion;

        try {
            sentencia = conexion.createStatement();
            resultado = sentencia.executeQuery(Str_sentencia);

            if (resultado.next()) {
                Reader CLOBString = resultado.getCharacterStream(1);

                buffer = new char[254];

                while ((int_bytesLeidos = CLOBString.read(buffer, 0, 254)) != -1) {
                    out.write(buffer, 0, int_bytesLeidos);
                }
            }
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (resultado != null) {
                resultado.close();
            }

            if (CLOBInputStream != null) {
                CLOBInputStream.close();
            }

            sentencia.close();
        }
    }
     // obtenerCampoCLOB

    /**
      Recupera el contenido de un campo CLOB y lo inserta en otro
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tablaOrigen                 tabla origen
      @param                String Str_campoOrigen                  nombre del campo origen que tiene el CLOB
      @param                String Str_condicionOrigen         condicion de la fila de origen
      @param                String Str_tablaDestino                 tabla origen
      @param                String Str_campoDestino                  nombre del campo destino que tiene el CLOB
      @param                String Str_condicionDestino         condicion de la fila de origen
      @return                void
      @exception        SQLException, Exception
      @since                29-11-2000
    */

    /*        public void copiarCampoCLOB(        Connection                conexion,
                                                                            String                        Str_tablaOrigen,
                                                                            String                        Str_campoOrigen,
                                                                            String                        Str_condicionOrigen,
                                                                            String                        Str_tablaDestino,
                                                                            String                        Str_campoDestino,
                                                                            String                        Str_condicionDestino) throws SQLException, Exception
            {
                    byte                                   buffer[] = new byte[10 * 1024];
                byte                                   bufferIntermedio[];
                  int                                    int_i = 0;
                  int                                    int_bytesLeidos = 0;
                  InputStream                            streamDeEntrada = null;
                  ResultSet                              resOrigen = null;
                    ResultSet                                resDestino = null;
                    Statement                                sentencia = null;
                    PreparedStatement                sentenciaPreparada = null;
                    String                                        Str_sentenciaOrigen = null;
                    String                                        Str_sentenciaDestino = null;

                    Str_sentenciaDestino = "UPDATE " + Str_tablaDestino + " SET "
                                                            +        Str_campoDestino + "=? WHERE "
                                                            +        Str_condicionDestino;

                    Str_sentenciaOrigen =         "SELECT " + Str_campoOrigen
                                                            +         " FROM " + Str_tablaOrigen
                                                            +        " WHERE " + Str_condicionOrigen;
                  try
                    {
                      sentencia = conexion.createStatement();
                       resOrigen = sentencia.executeQuery(Str_sentenciaOrigen);
                     if (resOrigen.next())
                            {
                        streamDeEntrada = resOrigen.getBinaryStream(1);
                                    sentenciaPreparada = conexion.prepareStatement(Str_sentenciaDestino);
                                    sentenciaPreparada.setBinaryStream((int) 1,
                                                                                                            streamDeEntrada,
                                                                                                            (int) streamDeEntrada.available());
                                    sentenciaPreparada.executeUpdate();
                     }
                    }
                  catch(SQLException eSql)
                  {
                     eSql.printStackTrace();
                throw eSql;
                  }
                  catch(Exception e)
                  {
                          e.printStackTrace();
                     throw e;
                  }
                  finally
                  {
                    if (sentenciaPreparada != null)
                                    sentenciaPreparada.close();
                            if (resOrigen != null)
                                    resOrigen.close();
                            if (streamDeEntrada != null)
                                    streamDeEntrada.close();
                            sentencia.close();
                  }
            } // copiarCampoCLOB

    */

    /**
      Actualiza el contenido de un campo CLOB
      @param                String Str_fichero         Fichero a insertar
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tabla        tabla donde est� el campo CLOB
      @param                String Str_campo          nombre del campo que tiene el CLOB
      @param                String Str_condicion condicion de la fila a modificar
      @return                void
      @exception        SQLException, Exception
      @since                16-11-2000

    */
    public void escribirCampoCLOB(String Str_Datos, Connection conexion,
        String Str_tabla, String Str_campo, String Str_condicion)
        throws SQLException, Exception {
        char[] buffer;
        int int_i = 0;
        int int_bytesLeidos = 0;
        File fichero;
        InputStream inputStream = null;
        StringReader buferdatos;
        int offset = 1;
        int int_tamanoOptimo = 3000; // tama�o �ptimo 3000 bytes
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        String Str_sentencia = null;

        Str_sentencia = "UPDATE " + Str_tabla + " SET " + Str_campo +
            "=? WHERE " + Str_condicion;

        try {
            // Inicializar el buffer
            buffer = new char[254];
            sentencia = conexion.prepareStatement(Str_sentencia);
            buferdatos = new StringReader(Str_Datos);

            sentencia.setCharacterStream(1, buferdatos, (int) 254);
            sentencia.executeUpdate();
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            sentencia.close();

            if (inputStream != null) {
                inputStream.close();
            }
        }
    }
     // escribirCampoCLOB

    /**
      Actualiza el contenido de un campo CLOB
      @param                ByteArrayOutputStream, outputStream de un multiparte
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tabla        tabla donde est� el campo CLOB
      @param                String Str_campo          nombre del campo que tiene el CLOB
      @param                String Str_condicion condicion de la fila a modificar
      @return                void
      @exception        SQLException, Exception
      @since                16-11-2000

    */

    /*        public void escribirCampoCLOB(
                                                            ByteArrayOutputStream        out,
                                                            Connection                                conexion,
                                                            String                Str_tabla,
                                                            String                Str_campo,
                                                            String                Str_condicion) throws SQLException, Exception
            {
                    byte                                   buffer[];
                  int                                    int_i = 0;
                  int                                    int_bytesLeidos = 0;
                  int                                    offset = 1;
                  int                                    int_tamanoOptimo = 3000; // tama�o �ptimo 3000 bytes
                    PreparedStatement                sentencia = null;
                    ResultSet                                resultado = null;
                    String                                        Str_sentencia = null;
                    ByteArrayInputStream        inputStream = null;

                    Str_sentencia = "UPDATE " + Str_tabla + " SET "
                                            +        Str_campo + "=? WHERE " + Str_condicion;

                  try
                    {
                            // Se pasa el stream de salida a uno de entrada
                            buffer = out.toByteArray();
                            inputStream = new ByteArrayInputStream(buffer);

                            // Inicializar el buffer
                            sentencia = conexion.prepareStatement(Str_sentencia);
                      sentencia.setBinaryStream((int) 1, inputStream, (int) inputStream.available());
                            sentencia.executeUpdate();
                    }
                    catch(SQLException eSql)
                    {
                            eSql.printStackTrace();
                             throw eSql;
                    }
                    catch(Exception e)
                    {
                            e.printStackTrace();
                             throw e;
                    }
                  finally
                  {
                           sentencia.close();
                            if (inputStream != null)
                                    inputStream.close();
                  }
        }// escribirCampoCLOB
    */
}
 // CampoCLOBSqlServer
