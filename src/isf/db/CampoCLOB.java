/*

*   Copyright ( 1999 Uni�n Fenosa International Software Factory,

*   S.A.. All Rights Reserved.

*

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/

package isf.db;

import java.sql.*;
import java.io.*;
import java.util.*;

/**
   Prop�sito: Tratamiento de los campos CLOBs.
   @author PSN, JDZ
   @version 2.0
*/
public abstract class CampoCLOB{

	private static final int	DESCONOCIDA = 0;
	private static final int	ORACLE = 1;
	private static final int	SQL_SERVER = 2;


	/**
		Devuelve una instancia de un objeto CampoCLOBOracle o
		CampoCLOBSqlServer dependiendo del valor tipo de base
		de datos del fichero de configuraci�n.
		@return	CampoCLOB,	objeto CampoCLOBOracle o CampoCLOBSqlServer
		@exception	IOException
		@since	16-11-2000
	*/
	public static CampoCLOB getInstance(String str_profile) throws IOException
	{
		CampoCLOB instancia = null;
		Properties	propiedades = null;
		int	int_tipoBaseDeDatos = DESCONOCIDA;

		// Si no se conoce el tipo de base de datos
		// se lee del fichero
		propiedades = new Properties();
		PoolLoader refPoolLoader = new PoolLoader();
		InputStream ficheroPropiedades = refPoolLoader.loadPoolProperties(str_profile);
		propiedades.load(ficheroPropiedades);
		int_tipoBaseDeDatos = Integer.parseInt(propiedades.getProperty("tipo"));

		switch (int_tipoBaseDeDatos)
		{
			case ORACLE 	: 	instancia = new CampoCLOBOracle();
								break;
			case SQL_SERVER: 	instancia = new CampoCLOBSqlServer();
								break;
			default			:	break;
		}//fin del switch

		return instancia;
	} // getInstance.

   //****************************************************************************************
   //A CONTINUACI�N VIENEN LOS M�TODOS QUE SE DEBEN SOBREESCRIBIR POR LOS QUE HEREDEN DE ESTA
   //****************************************************************************************


	/**
		Recupera el contenido de un campo BLOB
		@param			OutputStream out 	Stream por el que sale el BLOB
		@param			Connection conexion conexi�n de BD a utilizar
		@param			String Str_tabla 		Tabla de la que se que quiere leer
		@param			String Str_campo 		nombre del campo que tiene el BLOB
		@param			String Str_condicion 	condicion que debe cumplir la fila que se quiere modificar
		@return			void
		@exception		SQLException, Exception
		@since			29-11-2000

	*/
  	public void obtenerCampoCLOB(Writer	out,
									Connection		conexion,
									String			Str_tabla,
									String			Str_campo,
									String			Str_condicion
									) throws SQLException, Exception
	{
	}


	/**
	  Recupera el contenido de un campo BLOB y lo inserta en otro
	  @param		Connection conexion conexi�n de BD a utilizar
	  @param		String Str_tablaOrigen 		tabla origen
	  @param		String Str_campoOrigen 	 	nombre del campo origen que tiene el CLOB
	  @param		String Str_condicionOrigen 	condicion de la fila de origen
	  @param		String Str_tablaDestino 		tabla origen
	  @param		String Str_campoDestino 	 	nombre del campo destino que tiene el CLOB
	  @param		String Str_condicionDestino 	condicion de la fila de origen
	  @return		void
	  @exception	SQLException, Exception
	  @since		29-11-2000
	*/
  	public void copiarCampoCLOB(	Connection		conexion,
									String			Str_tablaOrigen,
									String			Str_campoOrigen,
									String			Str_condicionOrigen,
									String			Str_tablaDestino,
									String			Str_campoDestino,
									String			Str_condicionDestino)
									throws SQLException, Exception
	{
	}


	/**
	  Actualiza el contenido de un campo BLOB
      @param		String Str_fichero 	Fichero a insertar
	  @param		Connection conexion conexi�n de BD a utilizar
	  @param		String Str_tabla	tabla donde est� el campo CLOB
	  @param		String Str_campo 	 nombre del campo que tiene el CLOB
	  @param		String Str_condicion condicion de la fila a modificar
	  @return		void
	  @exception	SQLException, Exception
	  @since		16-11-2000

	*/
  	public void escribirCampoCLOB(	String		Str_Datos,
										Connection	conexion,
										String		Str_tabla,
										String		Str_campo,
										String		Str_condicion)
										throws SQLException, Exception
	{
	}


	/**
      Actualiza el contenido de un campo BLOB
      @param		ByteArrayOutputStream, outputStream de un multiparte
	  @param		Connection conexion conexi�n de BD a utilizar
	  @param		String Str_tabla	tabla donde est� el campo CLOB
	  @param		String Str_campo 	 nombre del campo que tiene el CLOB
	  @param		String Str_condicion condicion de la fila a modificar
	  @return		void
	  @exception	SQLException, Exception
	  @since		16-11-2000

	*/
  	public void escribirCampoCLOB(
							ByteArrayOutputStream	out,
							Connection				conexion,
							String		Str_tabla,
							String		Str_campo,
							String		Str_condicion)
							throws SQLException, Exception
	{
	}


	/**
      Actualiza el contenido de un campo BLOB
      @param		ByteArrayOutputStream	out stream a insertar
	  @param		String Str_tabla	tabla donde est� el campo CLOB
	  @param		String Str_campo 	 nombre del campo que tiene el CLOB
	  @param		String Str_condicion condicion de la fila a modificar
	  @return		void
	  @exception	SQLException, Exception
	  @since		16-11-2000

	*/
  	public void escribirCampoCLOB(
							ByteArrayOutputStream	out,
							String		Str_tabla,
							String		Str_campo,
							String		Str_condicion)
							throws SQLException, Exception
	{
	}

} // CampoCLOB

