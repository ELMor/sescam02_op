package isf.util;
import java.util.*;

public class FrameTokenizer implements Enumeration{

  private int CntFrameLength;
  private int frameLength;
  private StringTokenizer sto,st;
  private boolean hasElement;
  private boolean frameCalculated;
  private String remainingWord;
  String frame;

  public FrameTokenizer(String pStr,int pFrameLength) {
    frameLength=pFrameLength;
    frameCalculated=false;
    hasElement=false;
    CntFrameLength=0;
    remainingWord="";
    frame="";
    st= new StringTokenizer(pStr," ",false);
  }

 private boolean loadFrame(){
   String wordBuf="";
   int wbl;
   CntFrameLength=0;
   frame="";
   if (remainingWord.length()>0){
     wordBuf=remainingWord;
   }else if(st.hasMoreTokens()){
       wordBuf=st.nextToken()+" ";
   }else{
     wordBuf="";
   }
   wbl=wordBuf.length();
   hasElement=false;
   while(wbl>0){
     if(wordBuf.equals("\n")){
        hasElement=true;
        break;
     }
     else if (wbl>frameLength){
        frame+=wordBuf.substring(0,frameLength-CntFrameLength);
        remainingWord=wordBuf.substring(frameLength-CntFrameLength);
        hasElement=true;
        break;
      }else if(frameLength>=wbl+CntFrameLength){
        frame+=wordBuf;
        CntFrameLength+=wbl;
        remainingWord="";
        hasElement=true;
      }else{
        remainingWord=wordBuf;
        break;
      }
   if(st.hasMoreTokens()){
     wordBuf=st.nextToken()+" ";
   }else{
     wordBuf="";
   }
   wbl=wordBuf.length();
   }
   return hasElement;
 }

 public boolean hasMoreElements(){
 if (!frameCalculated){
   frameCalculated=true;
   this.loadFrame();
 }
  return hasElement;
 }

 /**
  *
  * @return
  * @throws NoSuchElementException
  */
 public Object nextElement() throws NoSuchElementException{
   if (!frameCalculated)
     this.loadFrame();
   else
     frameCalculated=false;
   if (!hasElement) throw new NoSuchElementException("No hay m�s lineas");
   return (Object) frame;
 }

public static void main(String[] args){

    StringTokenizer st= new StringTokenizer(
"Your book makes all the other Java books I�ve read or flipped through seem doubly useless and insulting.\n"+
"Brett g Porter, Senior \n"+
"\t Programmer, Art "+
"I_have_been_reading_your_book_for_a_week_or_two_and_compared_to_the_books "+
"I have read earlier on Java, your book seems to have given me a great start."+
"I have recommended this book to a lot of my friends and they "+
"have rated it excellent. Please accept my congratulations for coming out "+
"with an excellent book. Rama Krishna Bhupathi, Software "+
"Engineer, TCSI Corporation, San Jose ","\n",false);

   while (st.hasMoreElements()) {
         FrameTokenizer ft= new  FrameTokenizer((String)st.nextElement(),30);
         while (ft.hasMoreElements()) {
           System.out.println("/"+(String)ft.nextElement()+"/");
         }
     }

}


}