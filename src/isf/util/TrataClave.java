/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.util;

import java.util.*;
import isf.negocio.*;
import isf.persistencia.*;
import conf.*;
import isf.util.log.Log;
import isf.exceptions.ExceptionSESCAM;
/**
 * <p>Title: TrataClave</p>
 * <p>Description: Clase que realiza el tratamiento de claves (encriptacion, verificacion..)</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author F�lix Alberto Bravo Hermoso
 * @version 1.0
 */

public class TrataClave {

  private String clave_enc;
  private String clave_des;
  private String login;


  /**
   * Metodo que compara si dos String son iguales, uno encriptado y otro no.
   * @param textoPlano , String, clave sin encriptar
   * @param textoEncriptado ,String, clave encriptada
   * @return verdadero o falso segun sean o no iguales los argumentos tras la encriptacion.
   */
  private boolean sonIguales (String textoPlano, String textoEncriptado){

    boolean resultado = false;		// Resultado de la ejecuci�n del m�todo.
    String textoComparacion = null;	// String auxiliar utilizado para almacenar la encriptacion.
    UtilCipher ciph = new UtilCipher();
    // Encriptar el texto plano recibido.
    textoComparacion=ciph.desencriptar(textoEncriptado);
     if (textoComparacion.equals(textoPlano)) {
            resultado = true;
    }

    return resultado;
  }

  /**
   * Metodo que se encarga de realizar la encriptacion de la clave.
   * @param textoPlano, String, texto a encriptar.
   * @return String, texto encriptado.
   */
  public String Encriptar (String clave) {
    Cifrado zc=new UtilCipher();
    ExpandText et=new SimpleExpandText();
    String textoEncriptado = et.expand(zc.encriptar(clave));
    return textoEncriptado;
  }

  /**
   * Metodo que se encarga de realizar la desencriptacion de la clave.
   * @param clave
   * @return String, texto desencriptado.
   */
  public String DesEncriptar (String clave){
    Cifrado zc=new UtilCipher();
    ExpandText et=new SimpleExpandText();
    String textoDesEncriptado;
    textoDesEncriptado = zc.desencriptar(et.compress(clave));
    return textoDesEncriptado;
  }

  /**
   * Metodo para inicializar el objeto, mediante el cual se toma la clave de la BD correspondiente
   * al usuario.
   * @param usuario, Usuario del cual se va a tratar la clave.
   * @return boolean, verdadero si el usuario tiene alguna clave, falso si no tiene.
   */

  public boolean initClave(String usuario){
      boolean bret=false;
      Usuarios_clave sUc=new Usuarios_clave();
      Vector v=new Vector();
      login=usuario;
      v = sUc.busquedaUsuClaves("SYSLOGIN='"+usuario+"'","sysclv_pk DESC");
      if (v.size()>0){
          SysUsuClaves sUsuClaves=(SysUsuClaves)v.elementAt(0);
          clave_enc = sUsuClaves.getSysclvClave();
          clave_des = DesEncriptar(clave_enc);
          bret=true;
      }
      return bret;
  }

  /**
   * Metodo que devuelve la clave desencriptada existente en la base de datos.
   * @return String, clave desencriptada.
   */
  public String getClaveDes(){
    return clave_des;
  }

  /**
   * Metodo que devuelve la clave encriptada existente en la base de datos.
   * @return String, clave encriptada
   */
  public String getClaveEnc(){
  return clave_enc;
  }

  /**
   * Metodo que chequea si la clave ya existe en la base para ese usuario.
   * @param clave
   * @return
   */
  public boolean esClaveRepetida (String clave){
    boolean bret=false;
    String clave_comparada;
    Vector v = new Vector();
    Usuarios_clave usuclave = new Usuarios_clave();
    v=usuclave.busquedaUsuClaves("SYSLOGIN='"+login+"'","sysclv_pk DESC");
    for (int i_cont=0;(i_cont<20)&&(i_cont<v.size());i_cont++){
       SysUsuClaves sUC = (SysUsuClaves)v.elementAt(i_cont);
       clave_comparada = DesEncriptar(sUC.getSysclvClave());
       if (clave_comparada.equals(clave)){
         bret=true;
         break;
       }
    }
    return bret;
  }

  /**
   * Metodo que compara si dos cadenas poseen al menos 4 caracteres iguales en identicas posiciones.
   * @param textoPlano1, String, cadena.
   * @param textoPlano2, cadena.
   * @return verdadero si coinciden mas de 4 caracteres y sus posiciones, falso si no.
   */
  public boolean ComparaCaracteres (String textoPlano1, String textoPlano2){
    boolean bret=false;
    int aciertos=0;
    Vector v1 = new Vector();
    Vector v2 = new Vector();
    for (int i_cont=0;i_cont<textoPlano1.length();i_cont++){
      v1.addElement(textoPlano1.substring(i_cont,i_cont+1));
    }
    for (int i_cont=0;i_cont<textoPlano2.length();i_cont++){
      v2.addElement(textoPlano2.substring(i_cont,i_cont+1));
    }
    for (int i_contj=0;i_contj<v1.size()&&i_contj<v2.size();i_contj++){
      if(v1.elementAt(i_contj).equals(v2.elementAt(i_contj))){
        aciertos++;
      }
    }
    if (aciertos>=textoPlano2.length()/2){
        bret=true;
    }
    return bret;
  }


  /**
   * Metodo que desencripta la clave y realiza el ComparaCaracteres junto con la nueva clave.
   * @param clave_nueva, String, nueva clave.
   * @return verdadero si existen mas de 4 caracteres iguales entre las claves, falso si no.
   */
  public boolean CaracteresIguales (String clave_nueva){
      return ComparaCaracteres(clave_nueva,clave_des);
    }

    /**
     * Metodo que chequea si la clave del usuario ha caducado o no. Se utiliza para ello la entrada "duracion" en
     * el clave.properties para determinar el tiempo de caducidad de las claves.
     * @param usuario, String, login de la clave a chequear.
     * @return verdadero, si la clave est� caducada, falso si no lo est�.
     */
    public boolean esClaveCaducada(String usuario){
      boolean bret=false;
      Usuarios_clave sUc=new Usuarios_clave();
      Vector v=new Vector();
      int dias_clave=0,duracion;
      FicheroClave fc = new FicheroClave();
      v = sUc.busquedaUsuClaves("SYSLOGIN='"+usuario+"'","sysclv_pk DESC");
      duracion = fc.DameDuracionClave();
      if (v.size()>0){
          SysUsuClaves sUsuClaves=(SysUsuClaves)v.elementAt(0);
          try{
            dias_clave=Utilidades.getDiferenciaFechas(Utilidades.fechaActual(),sUsuClaves.getSysclvFecha());
          }catch(Exception e){
            System.out.println("Error al calcular la diferencia de fechas");
          }
          if (dias_clave > duracion) bret=true; //clave caducada
      }
      return bret;
  }

  /**
   * Metodo que se encarga de guardar la nueva clave, encriptandola.Ademas se asegura de que no existan mas
   * de 20 claves por usuario borrando las sobrantes.
   * @param clave_nueva, String, clave que va a ser encriptada y posteriormente guardada.
   */
  public void GuardarClaveNueva(String clave_nueva) throws ExceptionSESCAM {
      Log fichero_log = null;
      Usuarios_clave uC = new Usuarios_clave();
      SysUsuClaves sysUsuC = new SysUsuClaves();
      Vector v = new Vector();
      v = uC.busquedaUsuClaves("SYSLOGIN='"+login+"'","sysclv_pk DESC");
      if (v.size()>=20){
        try{
          SysUsuClaves sysUsuC_borrar=(SysUsuClaves)v.elementAt(v.size()-1);
          uC.eliminarUsuClaves(sysUsuC_borrar);
        }catch (Exception e){
          fichero_log = Log.getInstance();
          fichero_log.error(" No se ha eliminar la clave mas antigua del usuario con Login  " + login  + " Error -> " + e.toString());
          ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
          throw es;
          //System.out.println("Error al eliminar Clave nueva.");
        }
      }
      try{
      sysUsuC.setSyslogin(login);
      sysUsuC.setSysclvClave(Encriptar(clave_nueva));
      uC.insertarUsuClaves(sysUsuC);
      }catch(Exception e){
        fichero_log = Log.getInstance();
        fichero_log.error(" No se ha realizado la insercion de la nueva clave del usuario con Login " + login + " Error -> " + e.toString());
        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
        throw es;
//        System.out.println("Error al insertar Clave nueva.");
      }
  }

/*  public static void main(String args[]){
    TrataClave enc = new TrataClave();
    String Texto1="admin";
    String Texto2="jsj_23d-";
    String Texto1Encriptado=enc.Encriptar(Texto1);
    String Texto2Encriptado=enc.Encriptar(Texto2);
    System.out.println("Texto 1:"+Texto1);
    System.out.println("Texto 1 Encriptado:"+Texto1Encriptado);
    System.out.println("Texto 2:"+Texto2);
    System.out.println("Texto 2 Encriptado:"+Texto2Encriptado);
    if (enc.sonIguales(Texto1,Texto1Encriptado)){
      System.out.println("Funciona el metodo sonIguales().");
    }else System.out.println("No funciona el metodo sonIguales().");
    if (enc.ComparaCaracteres(Texto1,Texto2)){
      System.out.println("Texto erroneo: se repiten mas de 4 caracteres.");
    }else{
      System.out.println("Texto correcto.");
    }
  }*/
}
