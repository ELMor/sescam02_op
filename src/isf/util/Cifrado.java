package isf.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public interface Cifrado {

  /**
   * Encripta texto
   *
   * @param texto Texto a cifrar
   *
   * @return texto cifrado
   *
   */
  public String encriptar(String texto);

  /**
   * Descifra el texto
   *
   * @param text texto a descifrar
   *
   * @return Texto descifrado
   *
   */
  public String desencriptar(String text);

}