/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.exceptions;

/**
 * <p>Clase: ExceptionSESCAM </p>
 * <p>Descripcion: Clase que define las excepciones propias de la aplicacion </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 */

public class ExceptionSESCAM extends Exception  {
	static public final boolean RECUPERABLE = true;
  	static public final boolean NO_RECUPERABLE = false;

	static public final int ERROR = 0;
	static public final int DESCONOCIDO = 1;
	static public final int CAMPO_VACIO = 2;
	static public final int NO_CLAVE = 3;
	static public final int CLAVE_WRITE = 4;
	static public final int CLAVE_READ = 5;
	static public final int NO_DATA = 6;
	static public final int FILE_NOT_FOUND = 7;
	static public final int PARAMETER_NOT_FOUND = 8;
	static public final int NOMBRE_CAMPO_O_TABLA_INCORRECTO = 9;
	static public final int CLAVE_DUPLICADA = 10;
	static public final int ACCESO_A_DATOS = 11;
  	static public final int CONFIGURACION = 12;
  	static public final int GETTER_NO_ENCONTRADO = 13;
	static public final int ACCESO_DENEGADO = 14;
	static public final int NO_REGISTRADO = 15;
	static public final int TRANSACCION_INTERRUMPIDA = 16;
        /**
         * Excepcion producida cuando se quiere eliminar un permiso que esta asignado a algun perfil � Rol
         */
	static public final int PERFIL_USADO = 17;
        /**
         * Excepcion producida cuando se quiere eliminar un Rol que tiene permisos asociados
         */
	static public final int PERFIL_CON_PERMISOS = 18;
        /**
         * Excepcion producida cuando se quiere asignar a un usuario un login que ya esta asignado a otro
         */
	static public final int LOGIN_DUPLICADO = 19;
        /**
         * Excepcion producida cuando se quiere eliminar una oficina con usuarios asignados a ella
         */
	static public final int OFICINA_USADA = 20;
        /**
         * Excepcion producida cuando se quiere eliminar una provincia con localidades asignadas
         */
        static public final int PROVINCIA_USADA = 21;
        /**
         * Excepcion producida cuando se quiere eliminar una localidad que esta asignada a alguna oficina
         */
        static public final int LOCALIDAD_USADA = 22;
        /**
         * Excepcion producida cuando se quiere eliminar un cargo con usuarios que lo poseen
         */
        static public final int CARGO_UTILIZADO = 23;
        /**
          * Excepcion producida cuando se quiere eliminar el cargo "RESPONSABLE"
          */
        static public final int CARG0_RESPONSABLE = 24;
        /**
         * Excepcion cuando intentamos asignar a un campo numero un valor invalido
         */
        static public final int FORMATO_NUMERO_INCORRECTO = 25;
        /**
         * Excepcion cuando intentamos eliminar una oficina en la cual se han realizado solicitudes de garantias
         */
        static public final int OFICINA_TIENE_TRAMO_GARANTIA = 26;
        /**
         * Excepcion producida cuando se quiere eliminar un Rol o Perfil que esta asignado a alg�n usuario
         */
        static public final int PERFIL_TIENE_USUARIOS = 27;
        /**
         * Excepcion producida cuando obtienen mas registros en una consulta que el maximo de los definidos para la misma en la constante MAX_RESULT_SET
         */
        static public final int MAX_RESULTSET = 28;
        /**
          * Excepcion producida cuando se va realizar una operacion INSERT sobre la BD.
          */
        static public final int ERROR_AL_INSERTAR = 29;
        /**
         * Excepcion producida cuando se va realizar una operacion UPDATE sobre la BD.
         */
        static public final int ERROR_AL_MODIFICAR = 30;
        /**
         * Excepcion producida cuando se va realizar una operacion DELETE sobre la BD.
         */
        static public final int ERROR_AL_BORRAR = 31;
        /**
         * Excepcion producida cuando se quiere eliminar una Especialidad con servicios asignados a ella
         */
        static public final int ESPECIALIDAD_USADA = 32;
        /**
         * Excepcion producida cuando se quiere insertar una Mapeo ya insertado anteriormente.
         */
        static public final int ERROR_MAPEO_EXISTENTE = 33;
        /**
         * Excepcion producida cuando se quiere eliminar una Especialidad con servicios asignados a ella
         */
        static public final int PROC_TIPO_USADO = 34;



        private int int_codigo;
        private boolean tipo;

	private static String [] str_mensajes = {
	 // 0	NO_ERROR
	 "No Error.",
	 // 1	ERROR_DESCONOCIDO
	 "Error desconocido.",
	 // 2	CAMPO_VACIO
	 "campo(s) vacio(s).",
	 // 3	NO_CLAVE
	 "falta clave de eliminaci�n.",
	 // 4	CLAVE_WRITE
	 "error al escribir la clave en la tabla CLAVES en la bbdd.",
	 // 5	CLAVE_READ
	 "error al leer la clave en la tabla CLAVES en la bbdd.",
	 // 6	NO_DATA
	 "ningun resultado en la consulta.",
         // 7   FILE_NOT_FOUND
         "fichero buscado no encontrado.",
         // 8   PARAMETER_NOT_FOUND
         "parametro de configuracion no encontrado.",
	 // 9	NOMBRE_CAMPO_O_TABLA_INCORRECTO
	 "Nombre del campo o de la tabla incorrecto.",
	 // 10	CLAVE_DUPLICADA
	 "El registro ya existe en la base de datos.",
         // 11   ACCESO_A_DATOS
         "ERROR accediendo a la base de datos.",
 	 // 12	CONFIGURACION
	 "ERROR obteniendo la configuraci�n.",
         // 13   CONFIGURACION
         "Metodo get no encontrado.",
         // 14 ACCESO_DENEGADO
         "Debe validarse como usuario del sistema.",
         // 15 NO_REGISTRADO
         "No est� registrado a�n como usuario del sistema.",
         // 16 TRANSACCION_INTERRUMPIDA
         "Se ha producido una excepci�n en mitad de una transacci�n",
         // 17 NO SE PUEDE ELIMINAR.EL PERMISO ESTA ASOCIADO A UN PERFIL.
	 "No se puede eliminar. El permiso est� asociado a un perfil",
	 // 18 NO SE PUEDE ELIMINAR.EL PERFIL TIENE PERMISOS ASOCIADOS.
	 "No se puede eliminar. El perfil tiene permisos asociados.",
	 //19 LOGIN DUPLICADO
	 "Ya existe un usuario con ese LOGIN en la BD",
	 //20 OFICINA_USADA
	 "La Oficina a eliminar tiene usuarios asociados.No se puede eliminar.",
	 //21 PROVINCIA_USADA
	 "La Provincia a eliminar tiene localidades asociadas.No se puede eliminar.",
	 //22 LOCALIDAD_USADA
	 "La Localidad a eliminar tiene oficinas asociadas.No se puede eliminar.",
	 //23 CARGO_UTILIZADO
	 "El cargo a eliminar tiene usuarios asociados.No se puede eliminar",
         //24 CARGO_RESPONSABLE
         "No se puede eliminar el cargo 'RESPONSABLE',todas las oficinas tienen un responsable",
         //25 FORMATO_NUMERO_INCORRECTO
         "El monto no tiene un valor numerico. No se va guardar la informaci�n",
         //26 OFICINA_TIENE_TRAMO_GARANTIA
         "La oficina a eliminar tiene garantias asociadas. No se puede eliminar",
         // 27 PERFIL_TIENE_USUARIOS
         "El Perfil a eliminar tiene usuarios asociados. No se puede eliminar ",
         // 28 MAX_RESULTSET
         "Por favor, restrinja la busqueda. El numero de registros resultantes de la consulta es demasiado elevado ",
          // 29 ERROR_AL_INSERTAR
         "Error al guardar la informaci�n, No se puede realizar la inserci�n de los datos",
          // 30 ERROR_AL_MODIFICAR
         "Error al  guardar la informaci�n, No se puede realizar la modificaci�n de los datos",
          // 31 ERROR_AL_BORRAR
         "Error al guardar la informaci�n, No se puede realizar la eliminaci�n de los datos",
          // 32 ESPECIALIDAD_USADA
         "La Especialidad a eliminar tiene servicios asociados.No se puede eliminar.",
          // 33 ERROR_MAPEO_EXISTENTE
         "Error.Este mapeo ya ha sido realizado.Cheque los datos.",
          // 34 PROC_TIPO_USADO
         "El Tipo a eliminar tiene procedimientos asociados.No se puede eliminar."
	} ;

        /**
        * Constructor de la clase que define la excepcion que se ha producido como RECUPERABLE
        * @param int int_codigo Codigo de Excepcion que se ha producido
        */
	public ExceptionSESCAM(int int_codigo)	{
		this.int_codigo = int_codigo;
                this.tipo = RECUPERABLE;
	}
        /**
         * Constructor de la clase que define la excepcion que se ha producido con un tipo especifico
         * @param int int_codigo Codigo de Excepcion que se ha producido
         * @param boolean tipo Tipo de Excepcion que se ha producido
         */

        public ExceptionSESCAM(int int_codigo,boolean tipo) {
  		this(int_codigo);
         	this.tipo = tipo;
        }
        /**
         * Metodo que me dice el tipo de la excepcion producida (RECUPERABLE � NO RECUPERABLE)
         */

	 public boolean esRecuperable() {
   	  return tipo;
         }

        /**
          * Metodo que devuelve el mensaje asociado al error producido
	  * @return Devuelve el mensaje asociado a la excepcion
          */
	public String getMensaje()	{
	    return str_mensajes[int_codigo];
	} // getMensaje



	public String getMessage()	{
	    return getMensaje();
	} // getMessage

	/**
	  * Devuelve el codigo de la excepcion que identifica el error producido
	  * @return Devuelve el codigo de la excepcion que identifica el error producido
	  */

        public int getErrorCode()	{
	    return int_codigo;
	} // getMensaje

}
