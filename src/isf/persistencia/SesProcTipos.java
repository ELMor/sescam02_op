//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesProcTipos
// Table: SES_PROC_TIPOS
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_PROC_TIPOS
 * Columns for the table:
 * 	(proc_tipo - LONG), 
 * 	(proc_tcod - STRING), 
 * 	(proc_tdesc - STRING), 
 * Primary columns for the table:
 * 	(proc_tipo - LONG) 
 */
public class SesProcTipos
{
	// Properties
	private long procTipo;
	private boolean procTipoNull=true;
	private boolean procTipoModified=false;
	private String procTcod;
	private boolean procTcodNull=true;
	private boolean procTcodModified=false;
	private String procTdesc;
	private boolean procTdescNull=true;
	private boolean procTdescModified=false;
	// Access Method
	/** ProcTipo
	 * Get the value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public long getProcTipo() {
		return procTipo;
	}
	/** ProcTipo
	 * Set the value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public void setProcTipo(long _procTipo) {
		this.procTipo=_procTipo;
		this.procTipoModified=true;
		this.procTipoNull=false;
	}
	/** ProcTipo
	 * Set Null the value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public void setNullProcTipo() {
		this.procTipo=0;
		this.procTipoModified=true;
		this.procTipoNull=true;
	}
	/** ProcTipo
	 * Sumatory of value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	static public double sumProcTipo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_TIPO");
	}
	/** ProcTipo
	 * Indicates if the value of the property ProcTipo is null or not.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public boolean isNullProcTipo() {
		return procTipoNull;
	}
	/** ProcTcod
	 * Get the value of the property ProcTcod.
	 * The column for the database is 'PROC_TCOD'ProcTcod.
	 */
	public String getProcTcod() {
		if (procTcod==null) return "";
		if (procTcod.compareTo("null")==0) return "";
		return procTcod;
	}
	/** ProcTcod
	 * Set the value of the property ProcTcod.
	 * The column for the database is 'PROC_TCOD'ProcTcod.
	 */
	public void setProcTcod(String _procTcod) {
		this.procTcod=_procTcod;
		this.procTcodModified=true;
		this.procTcodNull=false;
	}
	/** ProcTcod
	 * Set Null the value of the property ProcTcod.
	 * The column for the database is 'PROC_TCOD'ProcTcod.
	 */
	public void setNullProcTcod() {
		this.procTcod=null;
		this.procTcodModified=true;
		this.procTcodNull=true;
	}
	/** ProcTcod
	 * Indicates if the value of the property ProcTcod is null or not.
	 * The column for the database is 'PROC_TCOD'ProcTcod.
	 */
	public boolean isNullProcTcod() {
		return procTcodNull;
	}
	/** ProcTdesc
	 * Get the value of the property ProcTdesc.
	 * The column for the database is 'PROC_TDESC'ProcTdesc.
	 */
	public String getProcTdesc() {
		if (procTdesc==null) return "";
		if (procTdesc.compareTo("null")==0) return "";
		return procTdesc;
	}
	/** ProcTdesc
	 * Set the value of the property ProcTdesc.
	 * The column for the database is 'PROC_TDESC'ProcTdesc.
	 */
	public void setProcTdesc(String _procTdesc) {
		this.procTdesc=_procTdesc;
		this.procTdescModified=true;
		this.procTdescNull=false;
	}
	/** ProcTdesc
	 * Set Null the value of the property ProcTdesc.
	 * The column for the database is 'PROC_TDESC'ProcTdesc.
	 */
	public void setNullProcTdesc() {
		this.procTdesc=null;
		this.procTdescModified=true;
		this.procTdescNull=true;
	}
	/** ProcTdesc
	 * Indicates if the value of the property ProcTdesc is null or not.
	 * The column for the database is 'PROC_TDESC'ProcTdesc.
	 */
	public boolean isNullProcTdesc() {
		return procTdescNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!procTipoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_TIPO");
			ls_values.append((lb_FirstTime?"":",")+procTipo);
			lb_FirstTime = false;
		}
		if (!procTcodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_TCOD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(procTcod)+"'");
			lb_FirstTime = false;
		}
		if (!procTdescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_TDESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(procTdesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_PROC_TIPOS ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (procTipoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_TIPO");
			if(procTipoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procTipo);
			lb_FirstTime = false;
		}
		if (procTcodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_TCOD");
			if(procTcodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(procTcod)+"'");
			lb_FirstTime = false;
		}
		if (procTdescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_TDESC");
			if(procTdescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(procTdesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("PROC_TIPO");
			ls_where.append("="+procTipo);
		} else {
			ls_where.append(" AND "+"PROC_TIPO");
			ls_where.append("="+procTipo);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_PROC_TIPOS SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("PROC_TIPO");
			ls_where.append("="+procTipo);
		} else {
			ls_where.append(" AND "+"PROC_TIPO");
			ls_where.append("="+procTipo);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_PROC_TIPOS WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _procTipo
	 * @return SesProcTipos - Retrieved object
	 */
	static public SesProcTipos read(Connection _connection,long _procTipo) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesProcTipos ls_SesProcTipos=new SesProcTipos();
		ls_where="PROC_TIPO = " + _procTipo;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_PROC_TIPOS WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesProcTipos.loadResultSet(ls_rs);
		} else {
			ls_SesProcTipos=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesProcTipos;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		procTipo=_rs.getLong("PROC_TIPO");
		procTipoNull=_rs.wasNull();
		procTcod=_rs.getString("PROC_TCOD");
		procTcodNull=_rs.wasNull();
		procTdesc=_rs.getString("PROC_TDESC");
		procTdescNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_PROC_TIPOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesProcTipos mySesProcTipos;
		Vector mySesProcTiposes = new Vector();
		while (ls_rs.next()) {
			mySesProcTipos = new SesProcTipos();
			mySesProcTipos.loadResultSet (ls_rs);
			mySesProcTiposes.addElement(mySesProcTipos);
		}
		ls_rs.close();
		myStatement.close();
		return mySesProcTiposes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_PROC_TIPOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_PROC_TIPOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_PROC_TIPOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setProcTipo(myPool.getSequence(this.getClass().getName(),profile));
	}
}
