//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesServicios
// Table: SES_SERVICIOS
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_SERVICIOS
 * Columns for the table:
 * 	(sessrv_cod - STRING), 
 * 	(sesesp_cod - STRING), 
 * 	(sessrv_desc - STRING), 
 * Primary columns for the table:
 * 	(sessrv_cod - STRING) 
 */
public class SesServicios
{
	// Properties
	private String sessrvCod;
	private boolean sessrvCodNull=true;
	private boolean sessrvCodModified=false;
	private String sesespCod;
	private boolean sesespCodNull=true;
	private boolean sesespCodModified=false;
	private String sessrvDesc;
	private boolean sessrvDescNull=true;
	private boolean sessrvDescModified=false;
	// Access Method
	/** SessrvCod
	 * Get the value of the property SessrvCod.
	 * The column for the database is 'SESSRV_COD'SessrvCod.
	 */
	public String getSessrvCod() {
		if (sessrvCod==null) return "";
		if (sessrvCod.compareTo("null")==0) return "";
		return sessrvCod;
	}
	/** SessrvCod
	 * Set the value of the property SessrvCod.
	 * The column for the database is 'SESSRV_COD'SessrvCod.
	 */
	public void setSessrvCod(String _sessrvCod) {
		this.sessrvCod=_sessrvCod;
		this.sessrvCodModified=true;
		this.sessrvCodNull=false;
	}
	/** SessrvCod
	 * Set Null the value of the property SessrvCod.
	 * The column for the database is 'SESSRV_COD'SessrvCod.
	 */
	public void setNullSessrvCod() {
		this.sessrvCod=null;
		this.sessrvCodModified=true;
		this.sessrvCodNull=true;
	}
	/** SessrvCod
	 * Indicates if the value of the property SessrvCod is null or not.
	 * The column for the database is 'SESSRV_COD'SessrvCod.
	 */
	public boolean isNullSessrvCod() {
		return sessrvCodNull;
	}
	/** SesespCod
	 * Get the value of the property SesespCod.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public String getSesespCod() {
		if (sesespCod==null) return "";
		if (sesespCod.compareTo("null")==0) return "";
		return sesespCod;
	}
	/** SesespCod
	 * Set the value of the property SesespCod.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public void setSesespCod(String _sesespCod) {
		this.sesespCod=_sesespCod;
		this.sesespCodModified=true;
		this.sesespCodNull=false;
	}
	/** SesespCod
	 * Set Null the value of the property SesespCod.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public void setNullSesespCod() {
		this.sesespCod=null;
		this.sesespCodModified=true;
		this.sesespCodNull=true;
	}
	/** SesespCod
	 * Indicates if the value of the property SesespCod is null or not.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public boolean isNullSesespCod() {
		return sesespCodNull;
	}
	/** SessrvDesc
	 * Get the value of the property SessrvDesc.
	 * The column for the database is 'SESSRV_DESC'SessrvDesc.
	 */
	public String getSessrvDesc() {
		if (sessrvDesc==null) return "";
		if (sessrvDesc.compareTo("null")==0) return "";
		return sessrvDesc;
	}
	/** SessrvDesc
	 * Set the value of the property SessrvDesc.
	 * The column for the database is 'SESSRV_DESC'SessrvDesc.
	 */
	public void setSessrvDesc(String _sessrvDesc) {
		this.sessrvDesc=_sessrvDesc;
		this.sessrvDescModified=true;
		this.sessrvDescNull=false;
	}
	/** SessrvDesc
	 * Set Null the value of the property SessrvDesc.
	 * The column for the database is 'SESSRV_DESC'SessrvDesc.
	 */
	public void setNullSessrvDesc() {
		this.sessrvDesc=null;
		this.sessrvDescModified=true;
		this.sessrvDescNull=true;
	}
	/** SessrvDesc
	 * Indicates if the value of the property SessrvDesc is null or not.
	 * The column for the database is 'SESSRV_DESC'SessrvDesc.
	 */
	public boolean isNullSessrvDesc() {
		return sessrvDescNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!sessrvCodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESSRV_COD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sessrvCod)+"'");
			lb_FirstTime = false;
		}
		if (!sesespCodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESESP_COD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sesespCod)+"'");
			lb_FirstTime = false;
		}
		if (!sessrvDescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESSRV_DESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sessrvDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_SERVICIOS ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (sessrvCodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESSRV_COD");
			if(sessrvCodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sessrvCod)+"'");
			lb_FirstTime = false;
		}
		if (sesespCodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESESP_COD");
			if(sesespCodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sesespCod)+"'");
			lb_FirstTime = false;
		}
		if (sessrvDescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESSRV_DESC");
			if(sessrvDescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sessrvDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("SESSRV_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(sessrvCod)+"'");
		} else {
			ls_where.append(" AND "+"SESSRV_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(sessrvCod)+"'");
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_SERVICIOS SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("SESSRV_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(sessrvCod)+"'");
		} else {
			ls_where.append(" AND "+"SESSRV_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(sessrvCod)+"'");
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_SERVICIOS WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,String _sessrvCod
	 * @return SesServicios - Retrieved object
	 */
	static public SesServicios read(Connection _connection,String _sessrvCod) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesServicios ls_SesServicios=new SesServicios();
		ls_where="SESSRV_COD  = '"+ SQLLanguageHelper.escapeQuote(  _sessrvCod) + "'";
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_SERVICIOS WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesServicios.loadResultSet(ls_rs);
		} else {
			ls_SesServicios=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesServicios;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		sessrvCod=_rs.getString("SESSRV_COD");
		sessrvCodNull=_rs.wasNull();
		sesespCod=_rs.getString("SESESP_COD");
		sesespCodNull=_rs.wasNull();
		sessrvDesc=_rs.getString("SESSRV_DESC");
		sessrvDescNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_SERVICIOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesServicios mySesServicios;
		Vector mySesServicioses = new Vector();
		while (ls_rs.next()) {
			mySesServicios = new SesServicios();
			mySesServicios.loadResultSet (ls_rs);
			mySesServicioses.addElement(mySesServicios);
		}
		ls_rs.close();
		myStatement.close();
		return mySesServicioses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_SERVICIOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_SERVICIOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_SERVICIOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
	}
}
