//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesProcGarantia
// Table: SES_PROC_GARANTIA
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_PROC_GARANTIA
 * Columns for the table:
 * 	(procgar_pk - LONG), 
 * 	(proc_pk - LONG), 
 * 	(sesesp_cod - STRING), 
 * 	(proc_garantia - DECIMAL), 
 * 	(proc_activo - DECIMAL), 
 * Primary columns for the table:
 * 	(procgar_pk - LONG) 
 */
public class SesProcGarantia
{
	// Properties
	private long procgarPk;
	private boolean procgarPkNull=true;
	private boolean procgarPkModified=false;
	private long procPk;
	private boolean procPkNull=true;
	private boolean procPkModified=false;
	private String sesespCod;
	private boolean sesespCodNull=true;
	private boolean sesespCodModified=false;
	private double procGarantia;
	private boolean procGarantiaNull=true;
	private boolean procGarantiaModified=false;
	private double procActivo;
	private boolean procActivoNull=true;
	private boolean procActivoModified=false;
	// Access Method
	/** ProcgarPk
	 * Get the value of the property ProcgarPk.
	 * The column for the database is 'PROCGAR_PK'ProcgarPk.
	 */
	public long getProcgarPk() {
		return procgarPk;
	}
	/** ProcgarPk
	 * Set the value of the property ProcgarPk.
	 * The column for the database is 'PROCGAR_PK'ProcgarPk.
	 */
	public void setProcgarPk(long _procgarPk) {
		this.procgarPk=_procgarPk;
		this.procgarPkModified=true;
		this.procgarPkNull=false;
	}
	/** ProcgarPk
	 * Set Null the value of the property ProcgarPk.
	 * The column for the database is 'PROCGAR_PK'ProcgarPk.
	 */
	public void setNullProcgarPk() {
		this.procgarPk=0;
		this.procgarPkModified=true;
		this.procgarPkNull=true;
	}
	/** ProcgarPk
	 * Sumatory of value of the property ProcgarPk.
	 * The column for the database is 'PROCGAR_PK'ProcgarPk.
	 */
	static public double sumProcgarPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROCGAR_PK");
	}
	/** ProcgarPk
	 * Indicates if the value of the property ProcgarPk is null or not.
	 * The column for the database is 'PROCGAR_PK'ProcgarPk.
	 */
	public boolean isNullProcgarPk() {
		return procgarPkNull;
	}
	/** ProcPk
	 * Get the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public long getProcPk() {
		return procPk;
	}
	/** ProcPk
	 * Set the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setProcPk(long _procPk) {
		this.procPk=_procPk;
		this.procPkModified=true;
		this.procPkNull=false;
	}
	/** ProcPk
	 * Set Null the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setNullProcPk() {
		this.procPk=0;
		this.procPkModified=true;
		this.procPkNull=true;
	}
	/** ProcPk
	 * Sumatory of value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	static public double sumProcPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_PK");
	}
	/** ProcPk
	 * Indicates if the value of the property ProcPk is null or not.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public boolean isNullProcPk() {
		return procPkNull;
	}
	/** SesespCod
	 * Get the value of the property SesespCod.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public String getSesespCod() {
		if (sesespCod==null) return "";
		if (sesespCod.compareTo("null")==0) return "";
		return sesespCod;
	}
	/** SesespCod
	 * Set the value of the property SesespCod.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public void setSesespCod(String _sesespCod) {
		this.sesespCod=_sesespCod;
		this.sesespCodModified=true;
		this.sesespCodNull=false;
	}
	/** SesespCod
	 * Set Null the value of the property SesespCod.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public void setNullSesespCod() {
		this.sesespCod=null;
		this.sesespCodModified=true;
		this.sesespCodNull=true;
	}
	/** SesespCod
	 * Indicates if the value of the property SesespCod is null or not.
	 * The column for the database is 'SESESP_COD'SesespCod.
	 */
	public boolean isNullSesespCod() {
		return sesespCodNull;
	}
	/** ProcGarantia
	 * Get the value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public double getProcGarantia() {
		return procGarantia;
	}
	/** ProcGarantia
	 * Set the value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public void setProcGarantia(double _procGarantia) {
		this.procGarantia=_procGarantia;
		this.procGarantiaModified=true;
		this.procGarantiaNull=false;
	}
	/** ProcGarantia
	 * Set Null the value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public void setNullProcGarantia() {
		this.procGarantia=0;
		this.procGarantiaModified=true;
		this.procGarantiaNull=true;
	}
	/** ProcGarantia
	 * Sumatory of value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	static public double sumProcGarantia(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_GARANTIA");
	}
	/** ProcGarantia
	 * Indicates if the value of the property ProcGarantia is null or not.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public boolean isNullProcGarantia() {
		return procGarantiaNull;
	}
	/** ProcActivo
	 * Get the value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public double getProcActivo() {
		return procActivo;
	}
	/** ProcActivo
	 * Set the value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public void setProcActivo(double _procActivo) {
		this.procActivo=_procActivo;
		this.procActivoModified=true;
		this.procActivoNull=false;
	}
	/** ProcActivo
	 * Set Null the value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public void setNullProcActivo() {
		this.procActivo=0;
		this.procActivoModified=true;
		this.procActivoNull=true;
	}
	/** ProcActivo
	 * Sumatory of value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	static public double sumProcActivo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_ACTIVO");
	}
	/** ProcActivo
	 * Indicates if the value of the property ProcActivo is null or not.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public boolean isNullProcActivo() {
		return procActivoNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!procgarPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROCGAR_PK");
			ls_values.append((lb_FirstTime?"":",")+procgarPk);
			lb_FirstTime = false;
		}
		if (!procPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_PK");
			ls_values.append((lb_FirstTime?"":",")+procPk);
			lb_FirstTime = false;
		}
		if (!sesespCodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESESP_COD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sesespCod)+"'");
			lb_FirstTime = false;
		}
		if (!procGarantiaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_GARANTIA");
			ls_values.append((lb_FirstTime?"":",")+procGarantia);
			lb_FirstTime = false;
		}
		if (!procActivoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_ACTIVO");
			ls_values.append((lb_FirstTime?"":",")+procActivo);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_PROC_GARANTIA ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (procgarPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROCGAR_PK");
			if(procgarPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procgarPk);
			lb_FirstTime = false;
		}
		if (procPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_PK");
			if(procPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procPk);
			lb_FirstTime = false;
		}
		if (sesespCodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESESP_COD");
			if(sesespCodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sesespCod)+"'");
			lb_FirstTime = false;
		}
		if (procGarantiaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_GARANTIA");
			if(procGarantiaNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+procGarantia);
			lb_FirstTime = false;
		}
		if (procActivoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_ACTIVO");
			if(procActivoNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+procActivo);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("PROCGAR_PK");
			ls_where.append("="+procgarPk);
		} else {
			ls_where.append(" AND "+"PROCGAR_PK");
			ls_where.append("="+procgarPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_PROC_GARANTIA SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("PROCGAR_PK");
			ls_where.append("="+procgarPk);
		} else {
			ls_where.append(" AND "+"PROCGAR_PK");
			ls_where.append("="+procgarPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_PROC_GARANTIA WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _procgarPk
	 * @return SesProcGarantia - Retrieved object
	 */
	static public SesProcGarantia read(Connection _connection,long _procgarPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesProcGarantia ls_SesProcGarantia=new SesProcGarantia();
		ls_where="PROCGAR_PK = " + _procgarPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_PROC_GARANTIA WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesProcGarantia.loadResultSet(ls_rs);
		} else {
			ls_SesProcGarantia=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesProcGarantia;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		procgarPk=_rs.getLong("PROCGAR_PK");
		procgarPkNull=_rs.wasNull();
		procPk=_rs.getLong("PROC_PK");
		procPkNull=_rs.wasNull();
		sesespCod=_rs.getString("SESESP_COD");
		sesespCodNull=_rs.wasNull();
		procGarantia=_rs.getDouble("PROC_GARANTIA");
		procGarantiaNull=_rs.wasNull();
		procActivo=_rs.getDouble("PROC_ACTIVO");
		procActivoNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_PROC_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesProcGarantia mySesProcGarantia;
		Vector mySesProcGarantiaes = new Vector();
		while (ls_rs.next()) {
			mySesProcGarantia = new SesProcGarantia();
			mySesProcGarantia.loadResultSet (ls_rs);
			mySesProcGarantiaes.addElement(mySesProcGarantia);
		}
		ls_rs.close();
		myStatement.close();
		return mySesProcGarantiaes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_PROC_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_PROC_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_PROC_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setProcgarPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
