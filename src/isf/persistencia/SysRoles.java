//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SysRoles
// Table: SYS_ROLES
// Package: ssf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SYS_ROLES
 * Columns for the table:
 * 	(sys_rol_pk - LONG), 
 * 	(sys_rol_nombre - STRING), 
 * 	(sys_rol_desc - STRING), 
 * Primary columns for the table:
 * 	(sys_rol_pk - LONG) 
 */
public class SysRoles
{
	// Properties
	private long sysRolPk;
	private boolean sysRolPkNull=true;
	private boolean sysRolPkModified=false;
	private String sysRolNombre;
	private boolean sysRolNombreNull=true;
	private boolean sysRolNombreModified=false;
	private String sysRolDesc;
	private boolean sysRolDescNull=true;
	private boolean sysRolDescModified=false;
	// Access Method
	/** SysRolPk
	 * Get the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public long getSysRolPk() {
		return sysRolPk;
	}
	/** SysRolPk
	 * Set the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public void setSysRolPk(long _sysRolPk) {
		this.sysRolPk=_sysRolPk;
		this.sysRolPkModified=true;
		this.sysRolPkNull=false;
	}
	/** SysRolPk
	 * Set Null the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public void setNullSysRolPk() {
		this.sysRolPk=0;
		this.sysRolPkModified=true;
		this.sysRolPkNull=true;
	}
	/** SysRolPk
	 * Sumatory of value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	static public double sumSysRolPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SYS_ROL_PK");
	}
	/** SysRolPk
	 * Indicates if the value of the property SysRolPk is null or not.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public boolean isNullSysRolPk() {
		return sysRolPkNull;
	}
	/** SysRolNombre
	 * Get the value of the property SysRolNombre.
	 * The column for the database is 'SYS_ROL_NOMBRE'SysRolNombre.
	 */
	public String getSysRolNombre() {
		if (sysRolNombre==null) return "";
		if (sysRolNombre.compareTo("null")==0) return "";
		return sysRolNombre;
	}
	/** SysRolNombre
	 * Set the value of the property SysRolNombre.
	 * The column for the database is 'SYS_ROL_NOMBRE'SysRolNombre.
	 */
	public void setSysRolNombre(String _sysRolNombre) {
		this.sysRolNombre=_sysRolNombre;
		this.sysRolNombreModified=true;
		this.sysRolNombreNull=false;
	}
	/** SysRolNombre
	 * Set Null the value of the property SysRolNombre.
	 * The column for the database is 'SYS_ROL_NOMBRE'SysRolNombre.
	 */
	public void setNullSysRolNombre() {
		this.sysRolNombre=null;
		this.sysRolNombreModified=true;
		this.sysRolNombreNull=true;
	}
	/** SysRolNombre
	 * Indicates if the value of the property SysRolNombre is null or not.
	 * The column for the database is 'SYS_ROL_NOMBRE'SysRolNombre.
	 */
	public boolean isNullSysRolNombre() {
		return sysRolNombreNull;
	}
	/** SysRolDesc
	 * Get the value of the property SysRolDesc.
	 * The column for the database is 'SYS_ROL_DESC'SysRolDesc.
	 */
	public String getSysRolDesc() {
		if (sysRolDesc==null) return "";
		if (sysRolDesc.compareTo("null")==0) return "";
		return sysRolDesc;
	}
	/** SysRolDesc
	 * Set the value of the property SysRolDesc.
	 * The column for the database is 'SYS_ROL_DESC'SysRolDesc.
	 */
	public void setSysRolDesc(String _sysRolDesc) {
		this.sysRolDesc=_sysRolDesc;
		this.sysRolDescModified=true;
		this.sysRolDescNull=false;
	}
	/** SysRolDesc
	 * Set Null the value of the property SysRolDesc.
	 * The column for the database is 'SYS_ROL_DESC'SysRolDesc.
	 */
	public void setNullSysRolDesc() {
		this.sysRolDesc=null;
		this.sysRolDescModified=true;
		this.sysRolDescNull=true;
	}
	/** SysRolDesc
	 * Indicates if the value of the property SysRolDesc is null or not.
	 * The column for the database is 'SYS_ROL_DESC'SysRolDesc.
	 */
	public boolean isNullSysRolDesc() {
		return sysRolDescNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!sysRolPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_ROL_PK");
			ls_values.append((lb_FirstTime?"":",")+sysRolPk);
			lb_FirstTime = false;
		}
		if (!sysRolNombreNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_ROL_NOMBRE");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sysRolNombre)+"'");
			lb_FirstTime = false;
		}
		if (!sysRolDescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_ROL_DESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sysRolDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SYS_ROLES ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (sysRolPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_ROL_PK");
			if(sysRolPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sysRolPk);
			lb_FirstTime = false;
		}
		if (sysRolNombreModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_ROL_NOMBRE");
			if(sysRolNombreNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sysRolNombre)+"'");
			lb_FirstTime = false;
		}
		if (sysRolDescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_ROL_DESC");
			if(sysRolDescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sysRolDesc)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		} else {
			ls_where.append(" AND "+"SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SYS_ROLES SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		} else {
			ls_where.append(" AND "+"SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SYS_ROLES WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _sysRolPk
	 * @return SysRoles - Retrieved object
	 */
	static public SysRoles read(Connection _connection,long _sysRolPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SysRoles ls_SysRoles=new SysRoles();
		ls_where="SYS_ROL_PK = " + _sysRolPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SYS_ROLES WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SysRoles.loadResultSet(ls_rs);
		} else {
			ls_SysRoles=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SysRoles;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		sysRolPk=_rs.getLong("SYS_ROL_PK");
		sysRolPkNull=_rs.wasNull();
		sysRolNombre=_rs.getString("SYS_ROL_NOMBRE");
		sysRolNombreNull=_rs.wasNull();
		sysRolDesc=_rs.getString("SYS_ROL_DESC");
		sysRolDescNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SYS_ROLES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SysRoles mySysRoles;
		Vector mySysRoleses = new Vector();
		while (ls_rs.next()) {
			mySysRoles = new SysRoles();
			mySysRoles.loadResultSet (ls_rs);
			mySysRoleses.addElement(mySysRoles);
		}
		ls_rs.close();
		myStatement.close();
		return mySysRoleses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SYS_ROLES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SYS_ROLES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SYS_ROLES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setSysRolPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
