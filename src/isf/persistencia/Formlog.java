//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: Formlog
// Table: FORMLOG
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:FORMLOG
 * Columns for the table:
 * 	(formlog_pk - LONG), 
 * 	(syslogin - STRING), 
 * 	(form_id - STRING), 
 * 	(log_fecha - DATE), 
 * 	(centro - LONG), 
 * 	(norden - LONG), 
 * Primary columns for the table:
 * 	(formlog_pk - LONG) 
 */
public class Formlog
{
	// Properties
	private long formlogPk;
	private boolean formlogPkNull=false;
	private boolean formlogPkModified=false;
	private String syslogin;
	private boolean sysloginNull=false;
	private boolean sysloginModified=false;
	private String formId;
	private boolean formIdNull=false;
	private boolean formIdModified=false;
	private java.sql.Date logFecha;
	private boolean logFechaNull=false;
	private boolean logFechaModified=false;
	private long centro;
	private boolean centroNull=false;
	private boolean centroModified=false;
	private long norden;
	private boolean nordenNull=false;
	private boolean nordenModified=false;
	// Access Method
	/** FormlogPk
	 * Get the value of the property FormlogPk.
	 * The column for the database is 'FORMLOG_PK'FormlogPk.
	 */
	public long getFormlogPk() {
		return formlogPk;
	}
	/** FormlogPk
	 * Set the value of the property FormlogPk.
	 * The column for the database is 'FORMLOG_PK'FormlogPk.
	 */
	public void setFormlogPk(long _formlogPk) {
		this.formlogPk=_formlogPk;
		this.formlogPkModified=true;
		this.formlogPkNull=false;
	}
	/** FormlogPk
	 * Set Null the value of the property FormlogPk.
	 * The column for the database is 'FORMLOG_PK'FormlogPk.
	 */
	public void setNullFormlogPk() {
		this.formlogPk=0;
		this.formlogPkModified=true;
		this.formlogPkNull=true;
	}
	/** FormlogPk
	 * Sumatory of value of the property FormlogPk.
	 * The column for the database is 'FORMLOG_PK'FormlogPk.
	 */
	static public double sumFormlogPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"FORMLOG_PK");
	}
	/** Syslogin
	 * Get the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public String getSyslogin() {
		if (syslogin==null) return "";
		if (syslogin.compareTo("null")==0) return "";
		return syslogin;
	}
	/** Syslogin
	 * Set the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setSyslogin(String _syslogin) {
		this.syslogin=_syslogin;
		this.sysloginModified=true;
		this.sysloginNull=false;
	}
	/** Syslogin
	 * Set Null the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setNullSyslogin() {
		this.syslogin=null;
		this.sysloginModified=true;
		this.sysloginNull=true;
	}
	/** FormId
	 * Get the value of the property FormId.
	 * The column for the database is 'FORM_ID'FormId.
	 */
	public String getFormId() {
		if (formId==null) return "";
		if (formId.compareTo("null")==0) return "";
		return formId;
	}
	/** FormId
	 * Set the value of the property FormId.
	 * The column for the database is 'FORM_ID'FormId.
	 */
	public void setFormId(String _formId) {
		this.formId=_formId;
		this.formIdModified=true;
		this.formIdNull=false;
	}
	/** FormId
	 * Set Null the value of the property FormId.
	 * The column for the database is 'FORM_ID'FormId.
	 */
	public void setNullFormId() {
		this.formId=null;
		this.formIdModified=true;
		this.formIdNull=true;
	}
	/** LogFecha
	 * Get the value of the property LogFecha.
	 * The column for the database is 'LOG_FECHA'LogFecha.
	 */
	public java.sql.Date getLogFecha() {
		return logFecha;
	}
	/** LogFecha
	 * Set the value of the property LogFecha.
	 * The column for the database is 'LOG_FECHA'LogFecha.
	 */
	public void setLogFecha(java.sql.Date _logFecha) {
		this.logFecha=_logFecha;
		this.logFechaModified=true;
		this.logFechaNull=false;
	}
	/** LogFecha
	 * Set Null the value of the property LogFecha.
	 * The column for the database is 'LOG_FECHA'LogFecha.
	 */
	public void setNullLogFecha() {
		this.logFecha=null;
		this.logFechaModified=true;
		this.logFechaNull=true;
	}
	/** Centro
	 * Get the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public long getCentro() {
		return centro;
	}
	/** Centro
	 * Set the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setCentro(long _centro) {
		this.centro=_centro;
		this.centroModified=true;
		this.centroNull=false;
	}
	/** Centro
	 * Set Null the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setNullCentro() {
		this.centro=0;
		this.centroModified=true;
		this.centroNull=true;
	}
	/** Centro
	 * Sumatory of value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	static public double sumCentro(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CENTRO");
	}
	/** Norden
	 * Get the value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	public long getNorden() {
		return norden;
	}
	/** Norden
	 * Set the value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	public void setNorden(long _norden) {
		this.norden=_norden;
		this.nordenModified=true;
		this.nordenNull=false;
	}
	/** Norden
	 * Set Null the value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	public void setNullNorden() {
		this.norden=0;
		this.nordenModified=true;
		this.nordenNull=true;
	}
	/** Norden
	 * Sumatory of value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	static public double sumNorden(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"NORDEN");
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		String ls_columns = new String();
		String ls_values  = new String();
		boolean lb_FirstTime = true;
		if (formlogPkModified) {
			if (lb_FirstTime ) { 
				ls_columns+="FORMLOG_PK";
				if(formlogPkNull) ls_values+="null";
				else ls_values+=formlogPk;
			} else {
				ls_columns+=","+"FORMLOG_PK";
				if(formlogPkNull) ls_values+=",null";
				else ls_values+=","+formlogPk;
			}
			lb_FirstTime = false;
		}
		if (sysloginModified) {
			if (lb_FirstTime ) { 
				ls_columns+="SYSLOGIN";
				if(sysloginNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			} else {
				ls_columns+=","+"SYSLOGIN";
				if(sysloginNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			}
			lb_FirstTime = false;
		}
		if (formIdModified) {
			if (lb_FirstTime ) { 
				ls_columns+="FORM_ID";
				if(formIdNull) ls_values+="null";
				else ls_values+="'"+SQLLanguageHelper.escapeQuote(formId)+"'";
			} else {
				ls_columns+=","+"FORM_ID";
				if(formIdNull) ls_values+=",null";
				else ls_values+=",'"+SQLLanguageHelper.escapeQuote(formId)+"'";
			}
			lb_FirstTime = false;
		}
		if (logFechaModified) {
			if (lb_FirstTime ) { 
				ls_columns+="LOG_FECHA";
				if(logFechaNull) ls_values+="null";
				else ls_values+= "'" + logFecha.toString() + "'" ; 
			} else {
				ls_columns+=","+"LOG_FECHA";
				if(logFechaNull) ls_values+=",null";
				else ls_values+= ",'"+logFecha.toString() + "'" ; 
			}
			lb_FirstTime = false;
		}
		if (centroModified) {
			if (lb_FirstTime ) { 
				ls_columns+="CENTRO";
				if(centroNull) ls_values+="null";
				else ls_values+=centro;
			} else {
				ls_columns+=","+"CENTRO";
				if(centroNull) ls_values+=",null";
				else ls_values+=","+centro;
			}
			lb_FirstTime = false;
		}
		if (nordenModified) {
			if (lb_FirstTime ) { 
				ls_columns+="NORDEN";
				if(nordenNull) ls_values+="null";
				else ls_values+=norden;
			} else {
				ls_columns+=","+"NORDEN";
				if(nordenNull) ls_values+=",null";
				else ls_values+=","+norden;
			}
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO FORMLOG ("+ls_columns+") VALUES ("+ls_values+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		String ls_setValues = new String();
		String ls_where  = new String();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (formlogPkModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="FORMLOG_PK";
				if(formlogPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+formlogPk;
			} else {
				ls_setValues+=","+"FORMLOG_PK";
				if(formlogPkNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+formlogPk;
			}
			lb_FirstTime = false;
		}
		if (sysloginModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="SYSLOGIN";
				if(sysloginNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			} else {
				ls_setValues+=","+"SYSLOGIN";
				if(sysloginNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(syslogin)+"'";
			}
			lb_FirstTime = false;
		}
		if (formIdModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="FORM_ID";
				if(formIdNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(formId)+"'";
			} else {
				ls_setValues+=","+"FORM_ID";
				if(formIdNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+SQLLanguageHelper.escapeQuote(formId)+"'";
			}
			lb_FirstTime = false;
		}
		if (logFechaModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="LOG_FECHA";
				if(logFechaNull) {
					ls_setValues+="=null";
				} else ls_setValues+= "='" + logFecha.toString() + "'"; 
			} else {
				ls_setValues+=","+"LOG_FECHA";
				if(logFechaNull) {
					ls_setValues+="=null";
				} else ls_setValues+="='"+ logFecha.toString() + "'"; 
			}
			lb_FirstTime = false;
		}
		if (centroModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="CENTRO";
				if(centroNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+centro;
			} else {
				ls_setValues+=","+"CENTRO";
				if(centroNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+centro;
			}
			lb_FirstTime = false;
		}
		if (nordenModified) {
			if (lb_FirstTime ) { 
				ls_setValues+="NORDEN";
				if(nordenNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+norden;
			} else {
				ls_setValues+=","+"NORDEN";
				if(nordenNull) {
					ls_setValues+="=null";
				} else ls_setValues+="="+norden;
			}
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where+="FORMLOG_PK";
			ls_where+="="+formlogPk;
		} else {
			ls_where+=" AND "+"FORMLOG_PK";
			ls_where+="="+formlogPk;
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE FORMLOG SET "+ls_setValues+" WHERE "+ls_where);
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		String ls_where  = new String();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where+="FORMLOG_PK";
			ls_where+="="+formlogPk;
		} else {
			ls_where+=" AND "+"FORMLOG_PK";
			ls_where+="="+formlogPk;
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE FORMLOG WHERE "+ls_where);
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _formlogPk
	 * @return Formlog - Retrieved object
	 */
	static public Formlog read(Connection _connection,long _formlogPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		Formlog ls_Formlog=new Formlog();
		ls_where="FORMLOG_PK = " + _formlogPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM FORMLOG WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_Formlog.loadResultSet(ls_rs);
		} else {
			ls_Formlog=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_Formlog;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		formlogPk=_rs.getLong("FORMLOG_PK");
		syslogin=_rs.getString("SYSLOGIN");
		formId=_rs.getString("FORM_ID");
		if (_rs.getDate("LOG_FECHA")!=null) {
			logFecha=new java.sql.Date(_rs.getDate("LOG_FECHA").getTime());
		} else {
				logFecha=null;
		}
		centro=_rs.getLong("CENTRO");
		norden=_rs.getLong("NORDEN");
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM FORMLOG ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		Formlog myFormlog;
		Vector myFormloges = new Vector();
		while (ls_rs.next()) {
			myFormlog = new Formlog();
			myFormlog.loadResultSet (ls_rs);
			myFormloges.addElement(myFormlog);
		}
		ls_rs.close();
		myStatement.close();
		return myFormloges;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM FORMLOG ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM FORMLOG ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM FORMLOG ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setFormlogPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
