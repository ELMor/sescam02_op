/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: Especialidades </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesEspecialidades </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 29/01/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class Especialidades {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio Especialidades
            */
        public static final String ID_TABLA="SES_ESPECIALIDADES";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesEspecialidades(la clase de persistencia) con las Especialidades que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesEspecialidades
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesEspecialidades (la clase de persistencia) con las Especialidades que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaEsp(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesEspecialidades.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Especialidad:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion de la Especialidad.
          * @param _sesEsp Objeto de clase SesEspecialidades (clase de persistencia asociada a tabla Ses_Especialidades)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Especialidades
          * @throws Exception
         */

        public void insertarEsp(SesEspecialidades _sesEsp) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                v = _sesEsp.search(conexion,"SESESP_COD='"+_sesEsp.getSesespCod()+"'","");
                conexion.setAutoCommit(false);
                try{
                   _sesEsp.insert(conexion);
                   try {
                     if (negevent != null) {
                      negevent.addVar(95,_sesEsp.getSesespCod());
                      negevent.addVar(96,_sesEsp.getSesespDesc());
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n de la especialidad con codigo = " + _sesEsp.getSesespCod() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(SQLException e){
//			System.out.println("Error al Insertar el Especialidad:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion de la Especialidad con Codigo = " + _sesEsp.getSesespCod()+ " Error -> " + e.toString());
                        conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
                   }finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                   }
                }

        /**
          * Realiza la modificacion del Servicio.
          * @param _sesEsp Objeto de clase SesEspecialidades (clase de persistencia asociada a tabla Ses_Especialidades)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Especialidades
         */
        public void modificarEsp(SesEspecialidades _sesEsp) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesEsp.update(conexion);
                   try {
                    if (negevent != null) {
                      negevent.addVar(97,_sesEsp.getSesespCod());
                      negevent.addVar(98,_sesEsp.getSesespDesc());
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion de la Especialidad  con C�digo = " + _sesEsp.getSesespCod() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(Exception e){
                        conexion.rollback();
//                        System.out.println("Error al Modificar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion de la Especialidad con Codigo = " + _sesEsp.getSesespCod() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                 }

                 finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                 }

        }

        /**
         * Realiza la eliminacion de la Especialidad.
         * @param _sesEsp Objeto de clase SesEspecialidades (clase de persistencia asociada a tabla Ses_Especialidades)
         * @throws Exception
         */

        public void eliminarEsp(SesEspecialidades _sesEsp) throws Exception,ExceptionSESCAM {
          Log log_fichero = null;
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesEsp.delete(conexion);
            try{
              if (negevent != null) {
                       negevent.addVar(99,_sesEsp.getSesespCod());
                       negevent.guardar(conexion);
                    }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de modificacion de la Especialidad  con C�digo = " + _sesEsp.getSesespCod() + " Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
                  String mensaje = new String(e.getMessage());
                  if (mensaje.indexOf("FK_SES_ESPECIALIDADES_RELATION__") > 0) {
                   conexion.rollback();
                   fichero_log = Log.getInstance();
                   fichero_log.error(" No se ha realizado la eliminaci�n de la Especialidad de Codigo  = " + _sesEsp.getSesespCod() + " Error -> " + e.toString());
                   ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ESPECIALIDAD_USADA);
                   throw es;
                  }else {
                      conexion.rollback();
                      fichero_log = Log.getInstance();
                      fichero_log.error(" No se ha realizado la eliminaci�n de la Especialidad de C�digo  = " + _sesEsp.getSesespCod()+ " Error -> " + e.toString());
                      ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                      throw es;
//                    System.out.println(" Error al borrar una Especialidad: "+e);
                    }
           }

           finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
           }
        }

} //fin de la clase

