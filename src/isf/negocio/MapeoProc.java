/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: MapeoProc </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesProcMap </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 05/02/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class MapeoProc {


        public static final String ID_TABLA="Ses_Proc_Map";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesProcMap(la clase de persistencia) con los Mapeos de Procedimientos que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesProcMap
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesProcMap (la clase de persistencia) con los Mapeos de Procedimientos que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaProcMap(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesProcMap.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar el Mapeo de Procedimiento:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion del Mapeo de Procedimiento.
          * @param _SesProcMap Objeto de clase SesProcMap (clase de persistencia asociada a tabla Ses_Proc_Map)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Proc_Map
          * @throws Exception
         */

        public void insertarProcMap(SesProcMap _sesProcMap) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                v = busquedaProcMap("PROC_CEN_COD='"+_sesProcMap.getProcCenCod()+"' AND CENTRO="+_sesProcMap.getCentro(),"");
                if (v.size()>0){
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_MAPEO_EXISTENTE);
                  throw es;
                }else{
                   try{
                    _sesProcMap.insert(conexion);
                    try {
                      if (negevent != null) {
                        String procpk = new Long(_sesProcMap.getProcPk()).toString();
                        negevent.addVar(109,procpk);
                        negevent.addVar(110,_sesProcMap.getProcCenCod());
                        String centro = new Double(_sesProcMap.getCentro()).toString();
                        negevent.addVar(111,centro);
                        negevent.guardar(conexion);
                      }
                    }catch (Exception e) {
                      fichero_log = Log.getInstance();
                      fichero_log.warning(" No se ha insertado el evento de inserci�n del Mapeo de Procedimiento con C�digo = " + _sesProcMap.getProcPk() + " Error -> " + e.toString());
                    }
                    conexion.commit();
                  }catch(SQLException e){
                    fichero_log = Log.getInstance();
                    fichero_log.error(" No se ha realizado la insercion del Procedimiento con Codigo = " + _sesProcMap.getProcPk() + " Error -> " + e.toString());
                    conexion.rollback();
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                    throw es;
                  }finally{
                    pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                  }
                }
        }

        /**
          * Realiza la modificacion del Mapeo del Procedimiento.
          * @param _SesProcMap Objeto de clase SesProcMap (clase de persistencia asociada a tabla Ses_Proc_Map)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Proc_Map
         */
        public void modificarProcMap(SesProcMap _sesProcMap) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesProcMap.update(conexion);
                  try {
                    if (negevent != null) {
                      String procpk = new Long(_sesProcMap.getProcPk()).toString();
                      negevent.addVar(109,procpk);
                      negevent.addVar(110,_sesProcMap.getProcCenCod());
                      String centro = new Double(_sesProcMap.getCentro()).toString();
                      negevent.addVar(111,centro);
                      negevent.guardar(conexion);
                    }
                  }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion del Procedimiento con C�digo = " + _sesProcMap.getProcPk() + " Error -> " + e.toString());
                  }
                 conexion.commit();
                    }catch(Exception e){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del Procedimiento con Codigo = " + _sesProcMap.getProcPk() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                 }

                 finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                 }

        }

        /**
         * Realiza la eliminacion del Mapeo del Procedimiento.
         * @param _SesProcMap Objeto de clase SesProcMap (clase de persistencia asociada a tabla Ses_Proc_Map)
         * @throws Exception
         */

        public void eliminarProcMap(SesProcMap _sesProcMap) throws Exception {
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesProcMap.delete(conexion);
            try{
              if (negevent != null) {

                      negevent.addVar(115,_sesProcMap.getProcCenCod());
                      String centro = new Double(_sesProcMap.getCentro()).toString();
                      negevent.addVar(116,centro);
                      negevent.guardar(conexion);
              }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de eliminacion del Procedimiento con C�digo = " + _sesProcMap.getProcPk() + " Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
           }
           finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
           }
        }

} //fin de la clase

