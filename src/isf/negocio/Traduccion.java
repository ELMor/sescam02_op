/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

import  java.sql.*;
import isf.db.*;
import beans.ConsultaMapeosProc;
import conf.*;

/**
 * <p>Clase: Traduccion </p>
 * <p>Descripcion: Clase de negocio que se encarga de realizar las traducciones de los mapeos. </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 03/03/2003
 * @author F�lix Alberto Bravo Hermoso.
 */

public class Traduccion {

  private long centro = 0;
  private String codigo_proc_mapeado = "SD";
  private String codigo_tipo_mapeado = "SD";
  private String codigo_serv_mapeado = "SD";
  private String codigo_diag_mapeado = "SD";

  private String gar_cq="";
  private long norden=0;

  private int resultCount;
  Pool ref_pool=null;
  public final int MAX_RESULT_SET=2000;

  /**
   * Metodo que se encarga de la Traduccion de los Procedimientos Mapeados a su correspondiente procedimiento SESCAM.
   * @return Objeto traduccion de procedimiento TraduccionProc.
   * @throws Exception
   */
public TraduccionProc TraducirProc() throws Exception {

  String sentWhere = "";
  String sentBusqueda = new String("");
  sentBusqueda= "select s.PROC_COD codigoSESCAM, s.PROC_DESC descSESCAM,s.PROC_TIPO tipoSESCAM,s.PROC_MONTO_MAX_EST monto from ses_procedimientos s, ses_proc_map smp ";
  String sentClausula = " WHERE s.PROC_PK=smp.PROC_PK";

  if (centro > 0){
    sentBusqueda = sentBusqueda + sentClausula + " AND smp.CENTRO="+centro;
  }
  if ((codigo_proc_mapeado!=null) && (!codigo_proc_mapeado.equals(""))) {
    codigo_proc_mapeado = codigo_proc_mapeado + "%";
    sentBusqueda = sentBusqueda + " AND smp.PROC_CEN_COD like '"+codigo_proc_mapeado+"'";
  }

  if ((codigo_tipo_mapeado!=null) && (!codigo_tipo_mapeado.equals(""))) {
    sentBusqueda = sentBusqueda + " AND smp.PROC_CEN_TIPO = '"+codigo_tipo_mapeado+"'";
  }

  Connection conn = null;
  resultCount=0;
  SingleConn sc=new SingleConn();
  TraduccionProc tp = new TraduccionProc();

  ref_pool = Pool.getInstance();
  conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
  try{
    conn.setAutoCommit(false);
    PreparedStatement st=conn.prepareStatement(sentBusqueda);
    ResultSet rs=st.executeQuery();
    while(rs.next()) {
      resultCount++;
      tp.codigoSESCAM=rs.getString(1);
      tp.descSESCAM=rs.getString(2);
      tp.tipoSESCAM=rs.getString(3);
      tp.monto=rs.getDouble(4);
    }
    conn.commit();
    try {
        rs.close();
        st.close();
      }
    catch (Exception ex){}
    }catch(Exception e)
    {
      try{conn.rollback();} catch(Exception esql){}
      e.printStackTrace();
      throw e;
    }
    finally{
      ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
    }
    return tp;
  }

  /**
   * Metodo que se encarga de la Traduccion de los Servicios Mapeados a su correspondiente servicio SESCAM.
   * @return Objeto traduccion de servicio TraduccionServ.
   * @throws Exception
   */
public TraduccionServ TraducirServ() throws Exception {

  String sentWhere = "";
  String sentBusqueda = new String("");
  sentBusqueda= "select s.SESSRV_COD, s.SESSRV_DESC, s.SESESP_COD from ses_srv_map ssm, ses_servicios s ";
  String sentClausula = " where s.SESSRV_COD=ssm.SESSRV_COD";
  if (centro > 0){
    sentBusqueda = sentBusqueda + sentClausula + " AND ssm.centro="+centro;
  }
  if ((codigo_serv_mapeado!=null) && (!codigo_serv_mapeado.equals(""))) {
//  codigo_proc_mapeado = codigo_proc_mapeado + "%";
    sentBusqueda = sentBusqueda + " AND ssm.SRV_CEN_COD = '"+codigo_serv_mapeado+"'";
  }

  Connection conn = null;
  resultCount=0;
  SingleConn sc=new SingleConn();
  TraduccionServ tS = new TraduccionServ();

  ref_pool = Pool.getInstance();
  conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
  try{
    conn.setAutoCommit(false);
    PreparedStatement st=conn.prepareStatement(sentBusqueda);
    ResultSet rs=st.executeQuery();
    while(rs.next()) {
      resultCount++;
      tS.codigoSESCAM=rs.getString(1);
      tS.descSESCAM=rs.getString(2);
      tS.especialidadSESCAM=rs.getString(3);
    }
    conn.commit();
    try {
        rs.close();
        st.close();
      }
    catch (Exception ex){}
    }catch(Exception e)
    {
      try{conn.rollback();} catch(Exception esql){}
      e.printStackTrace();
      throw e;
    }
    finally{
      ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
    }
    return tS;
  }

  /**
   * Metodo que se encarga de la Traduccion de los Diagnosticos Mapeados a su correspondiente diagnostico SESCAM.
   * @return Objeto traduccion de diagnostico TraduccionDiag.
   * @throws Exception
   */
public TraduccionDiag TraducirDiag() throws Exception {

  String sentWhere = "";
  String sentBusqueda = new String("");
  sentBusqueda= "select d.DIAG_COD, d.DIAG_DESC from ses_diagnosticos d, ses_diag_map sdm";
  String sentClausula = " where d.DIAG_COD=sdm.DIAG_CEN_COD";
  if (centro > 0){
    sentBusqueda = sentBusqueda + sentClausula + " AND sdm.CENTRO="+centro;
  }
  if ((codigo_diag_mapeado!=null) && (!codigo_diag_mapeado.equals(""))) {
//    codigo_diag_mapeado = codigo_diag_mapeado + "%";
    sentBusqueda = sentBusqueda + " AND sdm.DIAG_CEN_COD = '"+codigo_diag_mapeado+"'";
  }

  Connection conn = null;
  resultCount=0;
  SingleConn sc=new SingleConn();
  TraduccionDiag tD = new TraduccionDiag();

  ref_pool = Pool.getInstance();
  conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
  try{
    conn.setAutoCommit(false);
    PreparedStatement st=conn.prepareStatement(sentBusqueda);
    ResultSet rs=st.executeQuery();
    while(rs.next()) {
      resultCount++;
      tD.codigoSESCAM=rs.getString(1);
      tD.descSESCAM=rs.getString(2);
    }
    conn.commit();
    try {
        rs.close();
        st.close();
      }
    catch (Exception ex){}
    }catch(Exception e)
    {
      try{conn.rollback();} catch(Exception esql){}
      e.printStackTrace();
      throw e;
    }
    finally{
      ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
    }
    return tD;
  }


  /**
   * Funcion que extrae el monto de un procedimiento mapeado en LESP o LESPCEX.
   * @param long centro Centro asociado a LESP � LESPCEX  para obtener la entrada en Lista de Espera asociada
   * @param long orden Orden (en el caso de trabajar con LESP) o NCita (en el caso de LESPCEX) para obtener la entrada en Lista de Espera asociada
   * @param String gar_cq 'C' identifica que estamos trabajando con LESPCEX (Lista de Espera de Consultas Externas) u 'Q' para identificar que trabajamos con LESP (Lista de Espera Quir�rgica)
   * @return Devuelve el objeto TraduccionProc donde se encuentran los datos SESCAM del procedimiento a traducir.
   */
  public TraduccionProc ConsultaMontoLE() {
    Pool pool=null;
    ConsultaMapeosProc cmp = new ConsultaMapeosProc();
    TraduccionProc trad = new TraduccionProc();
    Connection conexion = null;
    String query = new String();
    String campo = "";
    String campo2 = "";
    try{
      pool = Pool.getInstance();
      conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
      if (gar_cq.equals("Q") ) {
        query = "SELECT * FROM LESP WHERE NORDEN = " +norden+ " AND CENTRO = " +centro;
        campo = "CPR1";
        campo2 = "Q";
      } else {
        query = "SELECT * FROM LESPCEX WHERE NCITA = " +norden+ " AND CEGA = " +centro;
        campo = "PRESTACI";
        campo2 = "IDENTPRES";
      }
      // Creamos el Statement y el ResultSet.
      Statement myStatement = conexion.createStatement();
      ResultSet ls_rs = myStatement.executeQuery (query);
      String cod_proc="";
      String cod_tipo="";
      if (ls_rs.next()) {
        cod_proc = ls_rs.getString(campo);
        if (gar_cq.equals("Q")){
          cod_tipo = campo2;
        }else{
          cod_tipo = ls_rs.getString(campo2);
        }
      }
      this.setCodigo_proc_mapeado(cod_proc);
      this.setCodigo_tipo_mapeado(cod_tipo);
      trad = this.TraducirProc();
      myStatement.close();
    }
    catch(Exception e){
        System.out.println("Error al Buscar monto procedimiento:"+ e);
    }
    finally{
        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
    }
    return trad;
 }


  public void setCentro(long centro) {
    this.centro = centro;
  }

  public void setCodigo_diag_mapeado(String codigo_diag_mapeado) {
    this.codigo_diag_mapeado = codigo_diag_mapeado;
  }

  public void setCodigo_proc_mapeado(String codigo_proc_mapeado) {
    this.codigo_proc_mapeado = codigo_proc_mapeado;
  }

  public void setCodigo_serv_mapeado(String codigo_serv_mapeado) {
    this.codigo_serv_mapeado = codigo_serv_mapeado;
  }

  public void setCodigo_tipo_mapeado(String codigo_tipo_mapeado) {
    this.codigo_tipo_mapeado = codigo_tipo_mapeado;
  }

  public void setGar_cq(String gar_cq) {
    this.gar_cq = gar_cq;
  }

  public void setNorden(long norden) {
    this.norden = norden;
  }

}
