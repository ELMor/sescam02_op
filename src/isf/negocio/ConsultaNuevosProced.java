/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

import  java.sql.*;
import java.util.Vector;
import isf.db.*;
import conf.*;
import isf.persistencia.*;

/**
 * <p>Clase: ConsultaNuevosProced </p>
 * <p>Descripcion: Clase de negocio para obtener Procedimientos no definidos en SesProcedimietnos pero que tengan informaci�n en cualquier Lista de Espera</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 21/11/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */


public class ConsultaNuevosProced {

  public int MAX_FILAS_PERMITIDAS = 1000;
  public boolean max_filas_superadas = false;

  public ConsultaNuevosProced(int maxfilas) {
    MAX_FILAS_PERMITIDAS = maxfilas;
    max_filas_superadas = false;
  }
  /**
   * Construye la consulta para obtener informaci�n de procedimientos que no esten definidos en SesProcedimientos pero que se tenga informaci�n de ellos en Lista de Espera
   * @return Devuelve un vector de objetos de clase SesProcedimientos que deben de ser insertados en tabla SesProcedimientos porque se tiene informaci�n en Lista de Espera de Cex � Quir�rgica
   */
  public Vector ConstruyeConsulta() {
    Pool pool=null;
    Connection conexion = null;
    String query = new String();
    String campo = new String();
    Vector resultado = new Vector();
    try{
      pool = Pool.getInstance();
      conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
      query = " SELECT COD,DESCRIP from lesp_uni_procs ";
   // Creamos el Statement y el ResultSet.
      Statement myStatement = conexion.createStatement();
      ResultSet ls_rs = myStatement.executeQuery (query);
      int nfilas = 0;
      while (ls_rs.next()) {
        nfilas = nfilas + 1;
        SesProcedimientos proced = new SesProcedimientos();
        proced.setProcCod(ls_rs.getString("cod"));
        proced.setProcDescCorta(ls_rs.getString("descrip"));
        proced.setProcGarantia(0);
        proced.setProcMontoMaxEst(0);
        resultado.addElement(proced);
        if (nfilas == MAX_FILAS_PERMITIDAS) {
          max_filas_superadas = true;
          break;
        }
      }
      ls_rs.close();
      myStatement.close();

      return resultado;
    }
    catch(SQLException e){
        System.out.println("Error al Obtener nuevos procedimientos " + e);
        return null;
    }

    finally{
        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
    }
 }

 //public static void main(String args[]) {
 //  ConsultaNuevosProced nuevo = new ConsultaNuevosProced();
 //  Vector v = new Vector();
 //  v = nuevo.ConstruyeConsulta();
 //  if (v!= null) {
 //    System.out.println("Encontrados " +v.size() +"registros ");
 //  }
 //}
}
