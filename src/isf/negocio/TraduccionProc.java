/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: TraduccionProc </p>
 * <p>Descripcion: Clase que guarda la los datos de la traduccion de los Servicios mapeados</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 03/03/2003
 * @author Felix Alberto Bravo Hermoso.
 */

public class TraduccionProc {

    /**
      * Codigo del Procedimiento SESCAM obtenido de la traduccion
      */
    public String 	codigoSESCAM="SD";

    /**
      * Descripci�n del Procedimiento SESCAM obtenido de la traduccion
      */
    public String 	descSESCAM="SD";

    /**
      * Monto del Procedimiento SESCAM obtenido de la traduccion
      */
    public String       tipoSESCAM="SD";

    /**
      *  Monto del Procedimiento SESCAM obtenido de la traduccion
      */
    public double        monto=0;

}