/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;

/**
 * <p>Clase : PermRol </p>
 * <p>Descripcion: Clase de negocio que maneja la clase de persistencia SysPermRol</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 22/10/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class PermRol {

        /**
          * Nombre de la clase de persistencia asociada a la clase de Negocio PermRol
          */

	public static final String ID_TABLA="SYS_PERM_ROL";

	private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


	// Devuelve un vector DE OBJETOS SysPermRol(la clase de persistencia)
	// con los permisos-roles que cumplan la clausula del where.

        /**
          * Devuelve un vector DE OBJETOS SysPermRol(la clase de persistencia) con los permisos-roles que cumplan la clausula del where.
          * @param String _where Clausula where para la consulta de objetos de clase SysPermRol
          * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SysPermRol (la clase de persistencia) con los permisos-roles que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */
	public Vector busquedaPermRol(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		v = SysPermRol.search(conexion,_where,_sort);
		}catch(SQLException e){
			System.out.println("Error al Insertar el Permiso-Rol:"+ e);
		}catch(Exception ex){
			System.out.println("Error al Insertar el Permiso-Rol:"+ ex);
		}
		finally{
			pool.freeConnection(DB,conexion);
		}
		return v;
	}

	//Realiza la insercion del Permiso-Rol.
        /**
          * Realiza la insercion del Permiso-Rol.
          * @param SysPermRol _sysPermRol Objeto de clase SysPermRol (clase de persistencia asociada a tabla Sys_Perm_Rol)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Sys_Perm_Rol
         */
	public void insertarPermRol(SysPermRol _sysPermRol) throws SQLException {
		Pool pool=null;
		Connection conexion = null;
		try{
		//System.out.println("Entra en insertarPerm");
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		_sysPermRol.insert(conexion);
		conexion.commit();
		}catch(SQLException e){
			conexion.rollback();
			System.out.println("Error al Insertar el Permiso-Rol:"+ e);
		}catch(Exception ex){
			conexion.rollback();
			System.out.println("Error al Insertar el Permiso-Rol:"+ ex);
		}
		finally{
			pool.freeConnection(DB,conexion);
		}
	}

	//Realiza la eliminacion del Permiso-Rol.
        /**
         * Realiza la eliminacion del Permiso_Rol.
         * @param SysPermRol _sysPermRol Objeto de clase SysPermRol(clase de persistencia asociada a tabla Sys_Perm_Rol)
         * @autor Felix Alberto Bravo Hermoso
         * @throws SQLException Exception producida por la operacion DELETE sobre tabla Sys_Perm_Rol
         */
	public void eliminarPerm(SysPermRol _sysPermRol) throws ExceptionSESCAM,Exception {
		Pool pool=null;
		Connection conexion = null;
		try{
		//System.out.println("Entra en metodo eliminarPerm");
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		_sysPermRol.delete(conexion);
		conexion.commit();
		//System.out.println("Delete hecho");
		}catch(SQLException e){
			System.out.println("Error al Modificar el Permiso-Rol:"+ e);
			conexion.rollback();
		}
		finally{
			pool.freeConnection(DB,conexion);
		}
	}


/*		public static void main(String args[]) {
	    Vector v = new Vector();
		Usuarios user = new Usuarios();
		try
 		{
		v = user.busquedaUsu("ACTIVO_SN=1 AND SYSLOGIN='adm'","APELLIDO1 ASC");
		System.out.println("4") ;
		for(int int_pos = 0;int_pos < v.size(); int_pos++){
						SysUsu x=(SysUsu)v.elementAt(int_pos);
						System.out.print(x.getApellido1() +"-");
						System.out.print(x.getApellido2() +"-");
						System.out.println(x.getNombre() +"");
			}
 		}catch (Exception e){
			System.out.println("Error Excepcion Main:"+ e) ;
 		}

  		}		*/


} //fin de la clase

