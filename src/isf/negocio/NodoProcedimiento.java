/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: NodoProcedimiento </p>
 * <p>Descripcion: Clase que guarda informaci�n de los procedimientos que pueden ser asociados a una clasificacion</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 06/02/2003
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */

public class NodoProcedimiento {
    /**
      * Codigo del Procedimiento obtenido en la consulta
      */
     public String 	NodoCodigo="SD";
     /**
       * Descripci�n Corta del Procedimiento obtenido en la consulta
       */
     public String 	NodoDesc="SD";
     /**
      *  pk del Procedimiento obtenido en la consulta
      */
     public long        NodoProcPk = 0;
     /**
       * Garantia del procediminto obtenido
       */
     public long 	NodoGarantia = 0;
     /**
       * Garantia del procediminto obtenido
       */
     public double      NodoMonto = 0;
     /**
      * Tipo del Procedimiento seleccionado
     */
     public long      NodoProcTipo = 0;

}