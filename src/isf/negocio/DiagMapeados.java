/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: DiagMapeados </p>
 * <p>Descripcion: Clase que guarda informaci�n de los Diagnosticos que pueden ser mapeados automaticamente</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 10/02/2003
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */

public class DiagMapeados {
    /**
    * pk del Diagnostico  obtenido en la consulta
    */
    public long 	DiagPk=0;

    /**
      * Codigo del Diagnostico  obtenido en la consulta
      */
     public String 	CodDiagSESCAM="SD";
     /**
       * Descripci�n del Diagnostico obtenido en la consulta
       */
     public String 	DiagDesc="SD";
     /**
      *  Diagnostico de LESP
      */
     public String        CodDiagLESP="SD";

}