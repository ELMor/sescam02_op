/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import conf.*;

/**
 * <p>Clase: Oficina </p>
 * <p>Descripcion: Clase de negocio que maneja la clase de persistencia SysPermisos </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 21/10/2002
 * @author Felix Alberto Bravo Hermoso
 */
public class Permisos {

     /**
       * Nombre de la clase de persistencia asociada a la clase de Negocio Permisos
       */
	public static final String ID_TABLA="SYS_PERMISOS";

	private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


	// Devuelve un vector DE OBJETOS SysPermisos(la clase de persistencia)
	// con los permisos que cumplan la clausula del where.

        /**
         * Devuelve un vector DE OBJETOS SysPermisos(la clase de persistencia) con los permisos que cumplan la clausula del where.
         * @param String _where Clausula where para la consulta de objetos de clase SysPermisos
         * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
         * @return Devuelve un vector DE OBJETOS SysPermisos (la clase de persistencia) con los permisos que cumplan la clausula del where.
         * @autor Felix Alberto Bravo Hermoso
         */
	public Vector busquedaPerm(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

	  	v = SysPermisos.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Permisos:"+ e) ;
		}

		finally{
			pool.freeConnection(DB,conexion);
		}
		return v;
	}

	//Realiza la insercion del Permiso.
	public void insertarPerm(SysPermisos _sysPerm) throws SQLException {
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		//System.out.println("Entra en insertarPerm");
		_sysPerm.newId(DB);
	  	_sysPerm.insert(conexion);
		conexion.commit();
		}catch(SQLException e){
			conexion.rollback();
			System.out.println("Error al Insertar el Permiso:"+ e);
		}catch(Exception ex){
			conexion.rollback();
			System.out.println("Error al Insertar el Permiso:"+ ex);
		}
		finally{
			pool.freeConnection(DB,conexion);
		}
	}

	//Realiza la modificacion del Permiso.
	public void modificarPerm(SysPermisos _sysPerm) throws SQLException  {
		Pool pool=null;
		Connection conexion = null;
		try{
		//System.out.println("Entra en modificarPerm");
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		_sysPerm.update(conexion);
		conexion.commit();
		}catch(SQLException e){
			System.out.println("Error al Modificar el Permiso:"+ e);
			conexion.rollback();
		}catch(Exception ex){
			conexion.rollback();
			System.out.println("Error al Modificar el Permiso:"+ ex);
		}
		finally{
			pool.freeConnection(DB,conexion);
		}
	}

	//Realiza la eliminacion del Permiso.
	public void eliminarPerm(SysPermisos _sysPerm) throws ExceptionSESCAM,Exception {
		Pool pool=null;
		Connection conexion = null;
		Vector v = new Vector();
		SysPermRol spr = new SysPermRol();
		try{
		//System.out.println("Entra en metodo eliminarPerm");
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		v = spr.search(conexion,"SYS_PERM_PK="+_sysPerm.getSysPermPk(),"");
		//System.out.println("Tama�o v:"+v.size());
		if (v.size()>0){
			ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.PERFIL_USADO);
			throw es;
		}else{
		 _sysPerm.delete(conexion);
		 conexion.commit();
		 //System.out.println("Delete hecho");
		}

		}catch(SQLException e){
			System.out.println("Error al Modificar el Permiso:"+ e);
			conexion.rollback();
		}
		finally{
			pool.freeConnection(DB,conexion);
		}
	}


/*		public static void main(String args[]) {
	    Vector v = new Vector();
		Usuarios user = new Usuarios();
		try
 		{
		v = user.busquedaUsu("ACTIVO_SN=1 AND SYSLOGIN='adm'","APELLIDO1 ASC");
		System.out.println("4") ;
		for(int int_pos = 0;int_pos < v.size(); int_pos++){
						SysUsu x=(SysUsu)v.elementAt(int_pos);
						System.out.print(x.getApellido1() +"-");
						System.out.print(x.getApellido2() +"-");
						System.out.println(x.getNombre() +"");
			}
 		}catch (Exception e){
			System.out.println("Error Excepcion Main:"+ e) ;
 		}

  		}		*/


} //fin de la clase

