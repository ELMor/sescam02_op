/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.Nolesp;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: NoRPLE </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia Noesp</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 26/05/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class NoRPLE{

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio NoRPLE
            */
	public static final String ID_TABLA="NOLESP";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS Nolesp(la clase de persistencia) con los servicios que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase Nolesp
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesNolesp (la clase de persistencia) con los servicios que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

	public Vector busquedaNoLesp(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
		try{

	  		v = Nolesp.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Paciente sin RLESP:"+ e) ;
		}

		finally{
 			pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
		}
		return v;
	}

        /**
         * Toma el valor de la clave gar_pk como pk para la tabla NOLESP
         * @return el valor de la clave autoincrement de la tabla SES_GARANTIAS
         * @throws SQLException
         */
        public long getGARPK () throws SQLException {
                        long sequence;
                        String name = "isf.persistencia.SesGarantia";
                        Pool pool= Pool.getInstance();
                        Connection myConnection;
                        Statement st;
                        ResultSet rs;

                        sequence = 0;
                        myConnection = pool.getConnection();
                        myConnection.setAutoCommit(false);
                        st = myConnection.createStatement();
                        rs = st.executeQuery ("SELECT * FROM CLAVE " +
                                                "WHERE CLAVE_NOMBRE = '" + name + "' " +
                                                  "FOR UPDATE");
                        if (rs.next()) {
                                sequence = rs.getLong ("CLAVE_NUMERO_ACTUAL") + 1;
                                st.executeUpdate ("UPDATE CLAVE " +
                                                  "SET CLAVE_NUMERO_ACTUAL = " + (sequence) + " " +
                                                  "WHERE CLAVE_NOMBRE = '" + name + "'");
                        } else {
                                sequence = 1;
                                st.executeUpdate ("INSERT INTO CLAVE " +
                                                                  "(CLAVE_NOMBRE, CLAVE_NUMERO_ACTUAL) " +
                                                                  "VALUES ('" + name + "', " + (sequence) + ")");
                        }

                        myConnection.commit();
                        rs.close();
                        st.close();
                        pool.freeConnection (myConnection);

                        return sequence;
                 }




        /**
          * Realiza la insercion del Paciente sin RLESP.
          * @param pac_NoLESP Objeto de clase Nolesp (clase de persistencia asociada a tabla NOLESP)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla NOLESP
          * @throws Exception
         */

	public void insertarNoLesp(Nolesp pac_NoLESP) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
		Pool pool=null;
		Connection conexion = null;
		Vector v = new Vector();
 		pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
		conexion.setAutoCommit(false);
		try{
                   pac_NoLESP.setNnolesp(this.getGARPK());
                   pac_NoLESP.insert(conexion);
                   try {
                     if (negevent != null) {
                      negevent.addVar(69,"DOC03");
                      negevent.addVar(70,pac_NoLESP.getSysUsu());
                      negevent.addVar(71,new Double(pac_NoLESP.getCentro()).toString());
                      negevent.addVar(72,new Double(pac_NoLESP.getNnolesp()).toString());
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n del Paciente sin RLESP con CIP = " + pac_NoLESP.getCip() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(SQLException e){
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion del del Paciente sin RLESP con CIP = " + pac_NoLESP.getCip() + " Error -> " + e.toString());
			conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
		   }finally{
			pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
		   }
		}

        /**
          * Realiza la modificacion del Paciente sin RLESP.
          * @param pac_NoLESP Objeto de clase Nolesp (clase de persistencia asociada a tabla NOLESP)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla NOLESP
         */
	public void modificarNoLesp(Nolesp pac_NoLESP) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
		Pool pool=null;
		Connection conexion = null;
                pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
		try{
	  	 pac_NoLESP.update(conexion);
/*                  try {
                    if (negevent != null) {
                      negevent.addVar(91,pac_NoLESP.getCip());
                      negevent.addVar(92,new Double (pac_NoLESP.getCentro()).toString());
                      negevent.addVar(93,pac_NoLESP.getNombre());
                      negevent.addVar(93,pac_NoLESP.getApellido1());
                      negevent.addVar(93,pac_NoLESP.getApellido2());
                      negevent.guardar(conexion);
                    }
                  }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion del Paciente sin RLESP con CIP = " + pac_NoLESP.getCip() + " Error -> " + e.toString());
                  }
*/                 conexion.commit();
   		 }catch(Exception e){
			conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del Paciente sin RLESP con CIP = " + pac_NoLESP.getCip() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
			throw es;
                 }

		 finally{
			pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
		 }

	}
} //fin de la clase

