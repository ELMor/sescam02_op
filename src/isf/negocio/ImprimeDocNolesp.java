/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;

import beans.AccesoBean;
import beans.ConsultaEntradasNoLE;
import isf.negocio.EntradaNoLE;
import isf.util.FrameTokenizer;
import isf.util.Impresion;
import isf.util.Utilidades;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 * <p>Clase: ImprimeDoc </p>
 * <p>Descripcion: Clase de negocio que se encarga de la impresion de los documentos de Nolesp.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 27/05/2003
 * @author Felix Alberto Bravo Hermoso
 */
public class ImprimeDocNolesp extends HttpServlet {
    private final int iMaxLengthMot = 6;
    private int Var_Evento1;

    /**
     * Metodo que convierte el parametro si es nulo a cadena vacia.
     * @param str ,String, cadena para forzar no nulo.
     * @return String, "" si es nulo y el mismo paramtro si no los es.
     * @since 25/11/2002
     * @author Felix Alberto Bravo Hermoso
     */
    private String ForzarNoNulo(String str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    }

    /**
     * Traduccion de los campos en los tag del documento.
     * @param translate Hashtable,
     * @param pStrCentro
     * @param pStrNOrden
     * @param pStrCQ
     * @param pStrDocumento
     * @param piGarpk
     * @param piOperacion
     * @return si la carga de campos se ha efectuado correctamente o no
     * @since 28/05/2003
     * @author felix Alberto Bravo Hermoso
     */
    private boolean cargaCampos(Hashtable translate, String CIP,
        String pStrDocumento, long plGarpk) {
        ConsultaEntradasNoLE cpb = new ConsultaEntradasNoLE();
        EntradaNoLE entnoLE;
        Vector vp;
        String strNom = "";
        int lenght_maxima;
        int iCnt = 0;

        /*************** Entradas NoLE ****************************/
        cpb.setCip(CIP);
        cpb.setGarpk(plGarpk);

        java.util.Date oFecha = new java.util.Date();

        try {
            vp = cpb.resultado();
        } catch (Exception ex) {
            return false;
        }

        if (vp != null) {
            Enumeration e = vp.elements();

            if (e.hasMoreElements()) {
                entnoLE = (EntradaNoLE) e.nextElement();
                translate.put("norden", "");
                translate.put("centro_cod",
                    ForzarNoNulo(String.valueOf(entnoLE.fCentro)));
                translate.put("orden", "");
                translate.put("fecha_demora", "");
                translate.put("tiempo_demora", "");
                translate.put("fecha_inclusion", "");
                translate.put("fecha_salida", "");
                translate.put("motivo_salida", "");
                translate.put("pac_nss", ForzarNoNulo(entnoLE.getFNSS()));
                translate.put("hicl",
                    ForzarNoNulo(String.valueOf(entnoLE.fHicl)));
                translate.put("cip", ForzarNoNulo(entnoLE.getFCip()));
                translate.put("apellido1", ForzarNoNulo(entnoLE.getFApellido1()));
                translate.put("apellido2", ForzarNoNulo(entnoLE.getFApellido2()));
                translate.put("nombre", ForzarNoNulo(entnoLE.getFNombre()));

                String nomape1ape2 = entnoLE.getFNombre() + ' ' +
                    entnoLE.getFApellido1() + ' ' + entnoLE.getFApellido2();
                translate.put("nombreape1ape2", ForzarNoNulo(nomape1ape2));
                translate.put("fec_nac", ForzarNoNulo(entnoLE.getFFechaNac()));
                translate.put("sexo", "");
                translate.put("domicilio", ForzarNoNulo(entnoLE.getFDomicilio()));
                translate.put("poblacion", ForzarNoNulo(entnoLE.getFPoblacion()));
                translate.put("cod_pos", "");
                translate.put("provincia", ForzarNoNulo(entnoLE.getFProvincia()));
                translate.put("telefono1", ForzarNoNulo(entnoLE.getFTelefono()));
                translate.put("telefono2", "");
                translate.put("telefono3", "");
                translate.put("financiador", "");
                translate.put("garante", "");

                String cod_servicio = "";
                translate.put("servicio", ForzarNoNulo(cod_servicio));

                String cod_seccion = "";
                translate.put("seccion", ForzarNoNulo(cod_seccion));
                translate.put("area_clinica", "SD");
                translate.put("inscripcion_nro",
                    String.valueOf(entnoLE.getFNnolesp()));
                translate.put("medico_codigo", "SD");
                translate.put("medico_nombre", "NO DISPONIBLE");
                translate.put("diag1_codigo", "");
                translate.put("diag1_descr", "NO DISPONIBLE");
                translate.put("diag2_codigo", "NO DISPONIBLE");
                translate.put("diag2_descr", "NO DISPONIBLE");
                translate.put("desc_serv_map", "NO DISPONIBLE");
                translate.put("proc1_codigo", "SD");
                translate.put("cod_proc_map", "SD");

                String cod_proc = entnoLE.getFPrestacion();

                if (ForzarNoNulo(entnoLE.getFPrestacion()).length() < 40) {
                    lenght_maxima = entnoLE.getFPrestacion().length();
                } else {
                    lenght_maxima = 40;
                }

                translate.put("proc1_descr",
                    ForzarNoNulo(entnoLE.getFPrestacion()).substring(0,
                        lenght_maxima));
                translate.put("desc_proc_map",
                    ForzarNoNulo(entnoLE.getFPrestacion()).substring(0,
                        lenght_maxima));
                translate.put("proc2_codigo", "SD");
                translate.put("proc2_descr", "NO DISPONIBLE");
                translate.put("tipo_cirugia", "SD");
                translate.put("tipo_anestesia", "SD");
                translate.put("prioridad", "SD");
                translate.put("ofi_prov", "");
                translate.put("hoy_DD_nro", "");
                translate.put("hoy_MM_txt", "");
                translate.put("hoy_YYYY_nro", "");
                translate.put("centro_nombre", ForzarNoNulo(entnoLE.fDesCentro));
                translate.put("centro_nombre_largo",
                    ForzarNoNulo(entnoLE.getFDesCentro()));
                translate.put("situacion_laboral", "");
                translate.put("pac_sit_lab_it", "");
                translate.put("pac_sit_lab_act", "");
                translate.put("monto_aprobacion", "0");

                /*if ((entnoLE.getfSlab()).equals("N")){
                  translate.put("pac_sit_lab_it","X");
                  translate.put("pac_sit_lab_act","");
                }else{
                  translate.put("pac_sit_lab_it","");
                  translate.put("pac_sit_lab_act","X");
                }*/
                translate.put("fecha_acred", ForzarNoNulo(entnoLE.getFFecha()));
                translate.put("resp_legal_dni",
                    ForzarNoNulo(entnoLE.getFResp_dni()));

                if (entnoLE.getFResp_apenom() != null) {
                    if (entnoLE.getFResp_apenom().length() < 65) {
                        lenght_maxima = entnoLE.getFResp_apenom().length();
                    } else {
                        lenght_maxima = 65;
                    }

                    translate.put("resp_legal_nombre",
                        entnoLE.getFResp_apenom().substring(0, lenght_maxima));
                } else {
                    translate.put("resp_legal_nombre", "");
                }

                translate.put("pac_nss", "");
                translate.put("ofi_coordinador",
                    ForzarNoNulo(entnoLE.getFOfi_coordinador()));
                translate.put("ofi_prov",
                    ForzarNoNulo(entnoLE.getFOfi_provincia()));
                translate.put("fecha_de_hoy", entnoLE.getFFecha());

                try {
                    translate.put("hoy_DD_nro",
                        entnoLE.getFFecha().substring(0, 2));
                    translate.put("hoy_MM_txt",
                        Utilidades.nombreMes(Integer.parseInt(
                                entnoLE.getFFecha().substring(3, 5))));
                    translate.put("hoy_YYYY_nro",
                        entnoLE.getFFecha().substring(6));
                } catch (NumberFormatException ex) {
                    translate.put("hoy_DD_nro", "");
                    translate.put("hoy_MM_txt", "");
                    translate.put("hoy_YYYY_nro", "");
                }

                if (entnoLE.getFMotivo_rechazo() != null) {
                    StringTokenizer st = new StringTokenizer(entnoLE.getFMotivo_rechazo(),
                            "\n", false);

                    while (st.hasMoreElements()) {
                        FrameTokenizer ft = new FrameTokenizer((String) st.nextElement(),
                                120);

                        while (ft.hasMoreElements()) {
                            iCnt++;
                            strNom = "motivo_denegacion" +
                                Integer.toString(iCnt);
                            translate.put(strNom, (String) ft.nextElement());
                        }
                    }
                }

                while (iCnt < iMaxLengthMot) {
                    iCnt++;
                    strNom = "motivo_denegacion" + Integer.toString(iCnt);
                    translate.put(strNom, " ");
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     *
     * @throws ServletException
     */
    public void init() throws ServletException {
    }

    //Process the HTTP Get request

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void service(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        ServletOutputStream outStr = response.getOutputStream();
        String strCIP;
        String strDocumento;
        String strDocName;
        String strTipo;
        String strReimpre;
        boolean lb_lanzar_evento = false;
        int iOperacion;
        long lGarpk;
        strDocumento = request.getParameter("documento");
        strReimpre = request.getParameter("impresion");
        strCIP = request.getParameter("cip");
        request.getSession().getServletContext();

        if (strReimpre.equals("1")) {
            lb_lanzar_evento = true;
        }

        try {
            lGarpk = new Double(request.getParameter("garpk")).longValue();
        } catch (NumberFormatException ex) {
            System.out.println(ex);
            lGarpk = 0;
            iOperacion = 0;
        }

        String strUsuario = request.getParameter("usuario");
        Impresion impDoc = new Impresion();

        //Control de acceso directo
        HttpSession session = request.getSession();
        AccesoBean accesoBeanId = (AccesoBean) session.getAttribute(
                "accesoBeanId");

        if ((accesoBeanId == null) || (!accesoBeanId.accesoA("acceso_LEQ"))) {
            outStr.println("<html>");
            outStr.println("<head>");
            outStr.println("<title>Acceso denegado</title>");
            outStr.println("</head>");
            outStr.println("<body>");
            outStr.println("<script language='javascript'>");
            outStr.println("	document.location = 'cierra.html'");
            outStr.println("</script>");
            outStr.println("<body>");
            outStr.println("</html>");

            return;
        }

        // Carga la Hashtable con el valor de los campos
        Hashtable translate = new Hashtable();
        cargaCampos(translate, strCIP, strDocumento, lGarpk);

        Properties docProps = new Properties();
        InputStream is = getClass().getResourceAsStream("/Impresion.properties");
        docProps.load(is);
        strTipo = docProps.getProperty(strDocumento + "_TIPO");
        strDocName = docProps.getProperty(strDocumento + "_DOC");

        if (strTipo.equals("RTF")) {
            response.setHeader("Content-Disposition",
                "inline;filename=" + strDocName);
            response.setContentType("application/rtf");
        } else if (strTipo.equals("XLS")) {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition",
                "inline;filename=" + strDocName);
        } else if (strTipo.equals("PDF")) {
            response.setHeader("Content-Disposition",
                "inline;charset='PDF';filename=" + strDocName);
            response.setContentType("application/pdf");
        } else {
            response.setHeader("Content-Disposition",
                "inline;filename=" + strDocName);
            response.setContentType("application/msword");
        }

        try {
            impDoc.imprimir(strDocumento, translate, outStr);

            //Registro de impresi�n de documentos
            if (lb_lanzar_evento == true) {
                NegEventoTram evento = accesoBeanId.getNegEventoTramInstance(14);

                if (evento != null) {
                    evento.setGarPk(lGarpk);
                    evento.setGarOperacion(3);
                    evento.addVar(57, strDocumento);
                    evento.addVar(58, strUsuario);
                    evento.guardar();
                }
            }
        } catch (IOException ioe) {
            throw ioe;
        } catch (Exception sqle) {
            System.out.println(
                "Error al grabar el registro de impresion de documentos" +
                "Documento:" + strDocumento + " Usuario:" + strUsuario +
                " CIP:" + strCIP + " NNolesp:" + lGarpk);
        }
    }

    //Clean up resources
    public void destroy() {
    }
}
