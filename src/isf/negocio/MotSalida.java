/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import conf.*;

/**
 * <p>Clase: Oficina </p>
 * <p>Descripcion: Clase de negocio que maneja la clase de persistencia SesMotSalida </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 27/01/2003
 * @author Jos� Vicente Nieto
 */
public class MotSalida {

     /**
       * Nombre de la clase de persistencia asociada a la clase de Negocio MotSalida
       */
	public static final String ID_TABLA="SES_MOT_SALIDA";

	private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


	// Devuelve un vector DE OBJETOS SesMotSalida(la clase de persistencia)
	// con los motivos de salida que cumplan la clausula del where.

        /**
         * Devuelve un vector DE OBJETOS SesMotSalida(la clase de persistencia) con los motivos que cumplan la clausula del where.
         * @param String _where Clausula where para la consulta de objetos de clase SesMotSalida
         * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
         * @return Devuelve un vector DE OBJETOS SesMotSalida (la clase de persistencia) con los motivos de salida que cumplan la clausula del where.
         * @autor Jos� Vicente Nieto
         */
	public Vector busquedaMotSalida(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

	  	v = SesMotSalida.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Motivos de Salida:"+ e) ;
		}

		finally{
			pool.freeConnection(DB,conexion);
		}
		return v;
	}

} //fin de la clase

