<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanEventoTipo" class="java.util.HashMap" scope="session"/>

<script language="javascript">
function cambia(fila_modificada) {
  this.document.seleccion.modificado.value = "true"
  this.document.seleccion.fila_destino.value = fila_modificada;
}
var FilaSelected=-1;

function seleccionaFila(id,pk,inicial){
  var nomCheck="",estado="";
  var checkeado = false;
  if (FilaSelected!=-1){
    nomCheck="chequeo"+FilaSelected;
    document.all(nomCheck).style.visibility ="hidden";
  }
  nomCheck="chequeo"+id;


  document.all(nomCheck).style.visibility ="visible";
  if (document.all("estado"+id).checked == false) {
    estado = "disabled";
  }

  if ((inicial == true) && (this.document.seleccion.fila_destino.value != id)) {
    modificado = parent.var_eventos.document.seleccion_var.modificado.value;

    if (modificado == "false") {
      modificado = this.document.seleccion.modificado.value;
    }
  }else {
    modificado = "false";
  }

  FilaSelected=id;
  if (modificado == "true") {
    resp = confirm("Esta seguro que desea guardar cambios de Evento seleccionado ");
    this.document.seleccion.modificado.value = "false";
    if (resp == true ) {
      this.document.seleccion.filas.value = "-1";
      this.seleccion.submit();
      parent.var_eventos.document.seleccion_var.todas.value = this.document.seleccion.marcar_todas.value;
      parent.var_eventos.document.seleccion_var.modificado.value = "false"
      parent.var_eventos.document.seleccion_var.submit();

    }else {
      this.document.seleccion.filas.value = pk;
      parent.eventos.location = "mant_aud_eventos.jsp"
    }
  }else {
    this.document.seleccion.filas.value = pk;
    this.document.seleccion.fila_destino.value = id;
    parent.var_eventos.location = "mant_aud_var_eventos.jsp?evtopk="+pk+"&estado="+estado;
   }
}

function selec(pk){
  parent.var_eventos.location = "mant_aud_var_eventos.jsp?evtopk="+pk;
}

</script>

</head>

<BODY  id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" >
<%

		Long fila;
                long fila_selec = 1;
                boolean lb_modificar = false;
                TipoEvento tipo_evento= new TipoEvento();
                long TpoEvto = 0;
		Vector vec = new Vector();
                String marcar_todas = "";
                vec = tipo_evento.busquedaTiposEventos("","");
                if (beanEventoTipo.size()>0 ) {
                  fila = (Long)beanEventoTipo.get("FILA");
                  fila_selec = fila.longValue();
                  beanEventoTipo.clear();
                }
                String marcar = "";
                boolean marcar_bd = true;
        //        System.out.println("Marcar todas" +request.getParameter("marcar_todas"));
                if (request.getParameter("marcar_todas") != null) {
                    marcar = request.getParameter("marcar_todas");
                    marcar_todas = "CHECKED";
                    if (marcar.equals("NOCHECKED")) {
                      marcar = "";
                      marcar_todas = "NOCHECKED";
                    }
                    marcar_bd = false;
                    lb_modificar = true;
               }
%>
<form name="seleccion" action = "modificarEventos.jsp" target = "oculto2">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
        <table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
                <%
                int idCounter=0;
                boolean bFilaPar = true,bFilaImpar = false;
                if (vec!=null){
              //         System.out.println("MARCABD"+marcar_bd);
                        EventoTipo eventos = new EventoTipo();
                        for(int int_pos = 0;int_pos < vec.size(); int_pos++){
                                idCounter++;
                                eventos=(EventoTipo)vec.elementAt(int_pos);
                                long tpoevtopk = eventos.getTpoevtoPk();
                                if (int_pos == (fila_selec -1)) {
                                  TpoEvto = tpoevtopk;
                                }
            //                    System.out.println(tpoevtopk);
            //                    System.out.println(eventos.getTpoevtoAct());
                                if (marcar_bd == true) {
                                  if (eventos.getTpoevtoAct() == 1) {
                                    marcar = "CHECKED";
                                  }
                                  else {
                                    marcar = "";
                                  }
                                }
          //                      System.out.println("Fila"+idCounter);

          //                      System.out.println("MARCA"+marcar);
                                if (bFilaPar){
                                                 bFilaPar=false;
        %>
                                        <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(this.id,<%=tpoevtopk%>,<%=true%>);" style="cursor:hand;">
        <%				}else{
                                                bFilaPar=true;
        %>
                                                <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(this.id,<%=tpoevtopk%>,<%=true%>);" style="cursor:hand;">
        <%
                                }
        %>
                                <td width="5%"><DIV id="chequeo<%=idCounter%>" style="visibility: hidden;"><img src="../../../imagenes/verifica.gif" border=0></DIV></td>
                                <td width="15%" height="12px"><%=tpoevtopk%></td>
                                <td width="65%" height="12px"><%=eventos.getTpoevtoDesc()%></td>
                                <td width="15%" height="12px"><input type = "checkbox"  name="estado<%=idCounter%>" <%=marcar%> onclick="cambia(<%=idCounter%>)"></td>
                           </tr>
                           <td><input type="hidden" id="codigo<%=idCounter%>" name="codigo_<%=idCounter%>"  value = "<%=tpoevtopk%>"></td>

<%
                          }
                }
%>
                </table>
              </td>
            </tr>
          </table>
          <input type="hidden" name="filas" value = <%=TpoEvto%>>
          <input type="hidden" name="marcar_todas" value = <%=marcar_todas%> >
          <input type="hidden" name="modificado" value = <%=lb_modificar%>>
          <input type="hidden" name="fila_destino" value = "">
          <input type="hidden" name="numfilas" value = <%=idCounter%>>

<%
          if ((vec != null) && (TpoEvto > 0)) {

        %>
                  <script language="javascript">
                     this.seleccionaFila(<%=fila_selec%>,<%=TpoEvto%>,false);
                  </script>
        <%
         }else{
        %>        <script language="javascript">
                    parent.var_eventos.location = "mant_aud_var_eventos.jsp";
                  </script>
        <%
         }
        %>
        <script language="javascript">
             parent.pie_eventos.location="mant_aud_pie_eventos.jsp?estado=Listo&filas=<%=vec.size()%>&marcar=<%=marcar_todas%>";
        </script>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
