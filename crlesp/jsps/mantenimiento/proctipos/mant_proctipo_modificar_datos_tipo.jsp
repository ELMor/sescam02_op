<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/SelecUtil.js"></script>
</head>
<jsp:useBean id="beanProcTipo" class="java.util.HashMap" scope="session"/>
<script language="javascript">
  var FilaSelected=-1;
  function seleccionaFila(filas, id){
      if (FilaSelected!=-1){
          if((FilaSelected%2)==0)
            filas[FilaSelected].className = "filaPar";
          else
            filas[FilaSelected].className = "filaImpar";
         }
      filas[id].className = "filaSelecionada";
      FilaSelected=id;
    }

  function selec(codserv,dato_origen,destino){

    parent.centro.location = "../servicios/mant_serv_modificar_datos_servicio.jsp?codserv="+codserv;
    parent.botonera.location.href = "../servicios/mant_serv_botones_modificar2.jsp?origen="+dato_origen +"&pk_destino="+destino;

}
</script>
<BODY class="mainFondo">
	<%
		ProcTipos pT = new ProcTipos();
		Vector vec = new Vector();
		long pktipo = new Long(request.getParameter("pktipo")).longValue();
		vec = pT.busquedaProcTipo("PROC_TIPO="+pktipo,"");
                SesProcTipos sesPT=(SesProcTipos)vec.elementAt(0);
                beanProcTipo.put("PROCTIPO",sesPT);
	%>
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> Editar Tipo de Procedimientos</td>
  </tr>
</table>
<form name="formulario" action="modificarProctipo.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <br>
  <tr>
    <td class= "texto" width="20%">Codigo:</td>
    <td class= "normal" width="80%"><input type="text" style="width:10%" name="codproc" maxlength=10 class="cajatexto2" value="<%=sesPT.getProcTcod()%>"></td>
  </tr>
  <tr>
    <td class= "texto">Descripci&oacute;n: </td>
    <td class= "normal"><input type="text" style="width:50%" maxlength=30 name="descproc" class="cajatexto3" value="<%=sesPT.getProcTdesc()%>"></td>
  </tr>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
