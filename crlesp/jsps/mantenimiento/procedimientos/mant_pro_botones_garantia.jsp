<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">
     function insertar() {
         parent.tit.location.href  = "mant_pro_modificar_gar_tit_blanco.htm";
         parent.fichagar.location.href = "mant_pro_alta_garantia.jsp";
         parent.downgar.location.href = "mant_pro_botones_alta_garantia.jsp";
      }

    function eliminar() {
      var procpk;
  //    alert("hola");
//      alert(parent.fichagar.document.formulario.filaseleccionada.value); //.value);
      var fila;
      fila = parseInt(parent.fichagar.document.formulario.filaseleccionada.value)
      if (fila >= 0) {
        resp = confirm("Desea eliminar la excepcion de garantia?")
        if (resp == true){
          parent.fichagar.document.formulario.action = "EliminarGarantia.jsp"
//        parent.fichagar.document.formulario.target="oculto1"
          parent.fichagar.document.formulario.submit();
          parent.fichagar.document.formulario.target = "GuardarGarantia.jsp"
        }else{
          procpk = parent.fichagar.document.formulario.procpk.value;
          parent.parent.centro.location='mant_pro_main_ficha_procedimiento.jsp?procpk='+procpk;
          parent.parent.botonera.location.href = "mant_pro_botones_modificar.jsp"
        }
      }else {
       alert(" No se puede eliminar, no hay fila seleccionada");
      }
    }
    function guardar() {
      var fila;
      fila = parseInt(parent.fichagar.document.formulario.filaseleccionada.value)
      if (fila >= 0) {
        resp = confirm("Desea guardar los datos?")
        if (resp == true){
          parent.fichagar.document.formulario.submit();
        }else{
          procpk = parent.fichagar.document.formulario.procpk.value;
          parent.parent.centro.location='mant_pro_main_ficha_procedimiento.jsp?procpk='+procpk;
          parent.parent.botonera.location.href = "mant_pro_botones_modificar.jsp"
        }
      }else {
       alert(" No se puede guardar, no hay fila seleccionada");
      }
    }

</script>

</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="guardar();">Guardar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="insertar();">Insertar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="eliminar();">Eliminar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
