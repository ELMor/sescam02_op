<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimiento" class="java.util.HashMap" scope="session"/>
<script language="javascript" type="text/javascript" src="../../../javascript/validarDecimal.js"></script>

</head>

<BODY class="mainFondo">
<%
		beanProcedimiento.clear();
		Procedimientos proced = new Procedimientos();
                double d_garantia = 0;
		Vector vec = new Vector();
		long l_reqpk = new Long(request.getParameter("procpk")).longValue();
                vec = proced.busquedaProced("PROC_PK="+l_reqpk,"");
		SesProcedimientos sesproc=(SesProcedimientos)vec.elementAt(0);
                ProcGarantias procgar = new  ProcGarantias();
                Vector ve = new Vector();
                ve = procgar.busquedaProcGarantia("proc_pk = "+l_reqpk+" and sesesp_cod is null ","");
                if (ve.size() == 1) {
                  SesProcGarantia sesprocgar = (SesProcGarantia)ve.elementAt(0);
                  d_garantia = sesprocgar.getProcGarantia();
                }
		beanProcedimiento.put("PROCED",sesproc);
%>
<TABLE width="100%" class="textoTop" border = "0">
  <tr>
  	 <td bgcolor=#5C5CAD> Modificar Procedimiento</td>
  </tr>
</table>
<form name="formulario" action="modificarProcedimiento.jsp" target="oculto2">
<table width="97%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width = "20%"> C�digo:     </td>
    <td class= "normal" width = "20%"><input type="text" maxlength=10 name="codigo" class="cajatexto2" style="width:50%" value="<%= sesproc.getProcCod()%>" ></td>
    <td class= "texto" width = "10%">&nbsp;</td>
    <td class="texto" width="15%">Tipo Proc.:</td>
    <td class= "normal" width="35%"><div id="divTipo">
          <Select name="valor_tipo" class="cajatexto2" style="width:25%">
<%
           ProcTipos PT = new ProcTipos();
           Vector vp = new Vector();
           vp = PT.busquedaProcTipo("","PROC_TCOD ASC");
           for(int int_pos = 0;int_pos < vp.size(); int_pos++){
                SesProcTipos sPT=(SesProcTipos)vp.elementAt(int_pos);
                if (sPT.getProcTipo()==sesproc.getProcTipo()){
%>
                <option  value='<%=sPT.getProcTipo()%>' selected><%=sPT.getProcTcod()%></option>
<%
                }else{
%>
                <option  value='<%=sPT.getProcTipo()%>'><%=sPT.getProcTcod()%></option>
<%
                }
            }
%>
          </Select></div>
        </td>
  </tr>

</table>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto"> Desc.Corta: </td>
    <td class= "normal"><input type="text" maxlength=80 name="descrip" class="cajatexto3" value="<%=sesproc.getProcDescCorta()%>" ></td>
  </tr>
 <tr>
   <td class= "texto" width = "20%"> Descripcion: </td>
   <td class= "normal" width = "80%"><input type="text" maxlength=255 name="descrip_larga" class="cajatexto3" value="<%= sesproc.getProcDesc() %>" ></td>
 </tr>
</table>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
 <tr>
    <td class= "texto" width = "20%"> Estado:     </td>
    <td class= "normal"width = "30%"><div id="divActivo">
            <Select name="activo" class="cajatexto2">
                    <%if (sesproc.getProcActivo()==1) { %>
                      <option  value='1' selected>Alta</option>
                      <option  value='0'>Baja</option>
                    <%}else{ %>
                      <option  value='1'>Alta</option>
                      <option  value='0' selected>Baja</option>
                    <% } %>
    	    </Select></div>
    </td>
    <td class= "texto"  width = "10%">   Garantia: </td>
    <td class= "normal" width = "40%"> <input type="text"  class="cajatexto2" name="garantia" style="width:30%" maxlength=6 value="<%=d_garantia%>" onkeypress="if ((event.keyCode < 48) || (event.keyCode > 57))  event.returnValue = false;" > dias. </td>
  </tr>
  <tr>
    <td class= "texto"> Precio :</td>
    <td class= "normal"> <input type="text"  class="cajatexto4" name="monto" style="width:50%" maxlength=20 value="<%=sesproc.getProcMontoMaxEst()%>" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false" onBlur = "validaDatoDecimal(this.value,13,2);" >&nbsp;
    <img align="middle" src="../../../imagenes/euro2.gif">
    </td>
  </tr>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
