<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimiento" class="java.util.HashMap" scope="session"/>
<script language="javascript" type="text/javascript" src="../../../javascript/validarDecimal.js"></script>
<script language="javascript">

var idfila = -1;
function seleccionadas(contador) {
  if (idfila >= 0) {
    this.document.all("marca_"+idfila).checked = false;
  }
  this.document.all("marca_"+contador).checked = true;
//  alert(contador);
  this.document.all.filaseleccionada.value = contador;
  idfila = contador;
}
</script>
</head>

<BODY class="mainFondo">
<%
                int int_pos = 0;
                int idCounter = 0;
                boolean bFilaPar=true;
		ProcGarantias procedGar = new ProcGarantias();
		Vector vec = new Vector();
		long l_reqpk = new Long(request.getParameter("procpk")).longValue();
                Long pk = new Long(l_reqpk);
                vec = procedGar.busquedaProcGarantia("PROC_PK="+l_reqpk,"");
                beanProcedimiento.put("PROCEDIMIENTO",pk);
%>
<form name="formulario" action="GuardarGarantia.jsp" target="oculto1">
<table width="100%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
<%
   for (int_pos = 0;int_pos < vec.size();int_pos++) {
    SesProcGarantia sesprocGar=(SesProcGarantia)vec.elementAt(int_pos);
    if ((sesprocGar.getSesespCod() != null) && (!sesprocGar.getSesespCod().equals(""))) {

    if (bFilaPar){
         bFilaPar=false;
%>
        <tr class="filaPar">
<%	 }else{
        bFilaPar=true;
%>
        <tr class="filaImpar">
<%
	 }
%>
    <td width="5%" height="12px"><input type="checkbox" id="marca_<%=idCounter%>" name="marca_<%=idCounter%>"  onClick="seleccionadas(<%=idCounter%>);"></td>
    <td width = "70%"><div id="especialidad_<%=idCounter%>">
          <Select name="especialidad_<%=idCounter%>" class="cajatexto3" style="width:70%" onChange="seleccionadas(<%=idCounter%>);">
           <%
           Especialidades esp = new Especialidades();
           Vector v = new Vector();
           v = esp.busquedaEsp("","");
           for(int pos = 0;pos < v.size(); pos++){
                SesEspecialidades sesEsp=(SesEspecialidades)v.elementAt(pos);
                   if (sesprocGar.getSesespCod().equals(sesEsp.getSesespCod())) {
           %>
                <option  value='<%=sesprocGar.getSesespCod()%>' selected><%=sesEsp.getSesespDesc()%></option>
            <%
                 }else{
           %>
                    <option value='<%=sesEsp.getSesespCod()%>'><%= sesEsp.getSesespDesc()%></option>
            <%
              }
            }
%>
           </Select></div>
     </td>
     <td width = "30%"> <input type="text"  class="cajatexto2" id="garantia_<%=int_pos%>" name="garantia_<%=idCounter%>" style="width:30%" maxlength=6 value="<%=sesprocGar.getProcGarantia()%>" onkeypress="if ((event.keyCode < 48) || (event.keyCode > 57))  event.returnValue = false;"onChange="seleccionadas(<%=idCounter%>);" > dias. </td>
    </tr>
    <td><input type="hidden" id="pk_<%=idCounter%>" name="pk_<%=idCounter%>"  value ="<%=sesprocGar.getProcgarPk()%>"></td>

<%
    idCounter++;
   }
   }
%>
   <td><input type="hidden" id="procpk" name="procpk"  value = <%=l_reqpk%>></td>
   <td><input type="hidden" id="filas" name="filas"  value = <%=idCounter%>></td>
   <td><input type="hidden" id="filaseleccionada" name="filaseleccionada"  value = "-1"></td>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
