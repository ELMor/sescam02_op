<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.ConsultaNuevosProced,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">

<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/validarDecimal.js"></script>
<script language="javascript">
  function seleccionadas(contador) {
    var numero_filas = 0;
    parseInt(contador);
    if (this.document.all("marca_"+contador).checked == true) {
      numero_filas = this.document.seleccion2.filas_modificadas.value;
      this.document.seleccion2.filas_modificadas.value = parseInt(numero_filas)+1;
    }else{
      numero_filas = this.document.seleccion2.filas_modificadas.value;
      this.document.seleccion2.filas_modificadas.value = parseInt(numero_filas)-1;
    }
  }
</script>
</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" >
  <%
      String marcar = "";

      if (request.getParameter("marcar_todas") != null) {
        marcar = request.getParameter("marcar_todas");
      }
      ConsultaNuevosProced proced = new ConsultaNuevosProced(500);
      Vector v = new Vector();
      v =  proced.ConstruyeConsulta();
      if (proced.max_filas_superadas == true) {
        %>
         <script language="javascript">
          alert(" Debido a la gran cantidad de registros resultantes de la busqueda solo se mostraran los " + <%=proced.MAX_FILAS_PERMITIDAS%> + " primeros registros ");
         </script>
        <%
      }
  %>
<form name="seleccion" action="mant_pro_datos_nuevos_proced.jsp" target="_self" method = "post">
   <input type="hidden" name="marcar_todas" value = "">
</form>
<form name="seleccion2" action="mant_pro_datos_nuevos_proced.jsp" target="_self" method = "post">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="1">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" cellspacing="0" cellpadding="0" border="1">

          <script language="javascript">
           parent.pie.document.pieEtado.lEstado.value="Generando resultado...";
          </script>
<%
           boolean bFilaPar=true;
           int idCounter=0;
           int i;
           if (v.size() > 0) {
                for (int int_pos = 0;int_pos < v.size(); int_pos++) {
                  SesProcedimientos proc=(SesProcedimientos)v.elementAt(int_pos);

                  if (bFilaPar){
                    bFilaPar=false;
                %>
                    <tr class="filaPar" id=<%=idCounter%> style="cursor:hand;">
                      <td width="5%" height="12px"><input type="checkbox" id="marca_<%=idCounter%>" name="marca_<%=idCounter%>" <%=marcar%> onClick="seleccionadas(<%=idCounter%>);"></td>
                      <td width="20%" height="12px"><%=proc.getProcCod()%></td>
                      <td width="45%" height="12px"><%=proc.getProcDescCorta()%></td>
                      <td width="15%" height="12px"><input type="text" id="monto_<%=idCounter%>" style="width:100%" name="monto_<%=idCounter%>" class="filaPar" value = <%=proc.getProcMontoMaxEst()%> onkeypress="if ((event.keyCode < 48) || (event.keyCode > 57))  event.returnValue = false" onClick = "this.value = ''"></td>
                      <td width="15%" height="12px"><input type="text" id="garantia_<%=idCounter%>" style="width:100%"  name="garantia_<%=idCounter%>" class="filaPar" value = <%=proc.getProcGarantia()%> onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false" onBlur = "validaDatoDecimal(this.value,13,2) "onClick = "this.value = ''" ></td>
                <%
                  }else{
                    bFilaPar=true;
                %>
                    <tr class="filaImpar" id=<%=idCounter%> style="cursor:hand;">
                      <td width="5%" height="12px"><input type="checkbox" id="marca_<%=idCounter%>" name="marca_<%=idCounter%>" <%=marcar%> onClick="seleccionadas(<%=idCounter%>);"></td>
                      <td width="20%" height="16px"><%=proc.getProcCod()%></td>
                      <td width="45%" height="16px"><%=proc.getProcDescCorta()%></td>
                      <td width="15%" height="12px"><input type="text" id="monto_<%=idCounter%>" style="width:100%" name="monto_<%=idCounter%>" class="filaImpar" maxlength=6 value = <%=proc.getProcMontoMaxEst()%> onkeypress="if ((event.keyCode < 48) || (event.keyCode > 57))  event.returnValue = false" onClick = "this.value = ''" ></td>
                      <td width="15%" height="12px"><input type="text" id="garantia_<%=idCounter%>" style="width:100%" name="garantia_<%=idCounter%>" class="filaImpar"  maxlength=20 value = <%=proc.getProcGarantia()%> onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false" onBlur = "validaDatoDecimal(this.value,13,2)" onClick = "this.value = ''"></td>
                <%
                  }
        	%>
                  </tr>
                <td><input type="hidden" id="codigo_<%=idCounter%>" name="codigo_<%=idCounter%>"  value = "<%=proc.getProcCod()%>"></td>
                <td><input type="hidden" id="descrip_<%=idCounter%>" name="descrip_<%=idCounter%>"  value = "<%=proc.getProcDescCorta()%>"></td>

<%
                  idCounter++;
                }
                %>
                 <script language="javascript">
                    parent.pie.document.pieEtado.lEstado.value="Listo";
                    parent.pie.document.pieEtado.lFilas.value=<%=v.size()%>;
                    parent.parent.botonera.location = "mant_pro_botones_nuevos_proced.jsp"
                 </script>
                <%
           }
%>
     </table>
    </td>
   </tr>
   </table>
   <td><input type="hidden" name="filas" value = <%=idCounter%>></td>
   <td><input type="hidden" name="marcar_todas" <%=marcar%> value = "0"></td>
   <td><input type="hidden" name="filas_modificadas" value = "0"></td>

  </form>

  <form name="guardar" action="AnnadirProced.jsp" target="oculto1">
  <div id="datos">
  </div>
  </form>


</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

