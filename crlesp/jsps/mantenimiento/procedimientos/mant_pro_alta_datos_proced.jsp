<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Procedimientos,isf.negocio.ProcTipos,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>

<style>
input {background-color: transparent}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/validarDecimal.js"></script>
</head>

<BODY class="mainFondo">
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> A�adir Procedimiento</td>
  </tr>
</table>
<form name="alta" action="guardarProcedimiento.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
          <td></td>
          <td></td>
  </tr>
  <tr>
    <td class= "texto" width = "20%"> C�digo:     </td>
    <td class= "normal" width = "80%"><input type="text" maxlength=10 name="codigo" class="cajatexto2"></td>
  </tr>
  <tr>
    <td class= "texto"> Desc.Corta: </td>
    <td class= "normal"><input type="text" maxlength=80 name="descrip" class="cajatexto3"></td>
  </tr>
</table>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
 <tr>
   <td class= "texto" width = "20%"> Descripcion: </td>
   <td class= "normal" width = "80%"><input type="text" maxlength=255 name="descrip_larga" class="cajatexto3"></td>
 </tr>
</table>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
 <tr>
    <td class= "texto" width = "20%"> Estado:     </td>
    <td class= "normal"width = "30%"><div id="divActivo">
            <Select name="activo" class="cajatexto2">
               <option  value='1' selected>Alta</option>
               <option  value='0'>Baja</option>
            </Select></div>
    </td>
    <td class= "texto"  width = "10%" align="right">Garantia:</td>
    <td class= "normal" width = "40%"> <input type="text"  class="cajatexto2" name="garantia" style="width:30%" maxlength=6 value = "0" onkeypress="if ((event.keyCode < 48) || (event.keyCode > 57))  event.returnValue = false;" >&nbsp;dias.
    </td>
  </tr>
  <tr>
    <td class= "texto"> Precio :</td>
    <td class= "normal"> <input type="text" class="cajatexto4" name="monto" style="width:50%" maxlength=20 value = "0.0" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;" onBlur = "validaDatoDecimal(this.value,13,2);">&nbsp;
    <img align="middle" src="../../../imagenes/euro2.gif">
    </td>
  </tr>
  <tr>
        <td class="texto" width="10%">Tipo Proc.:</td>
        <td class= "normal" width="10%"><div id="divTipo">
          <Select name="valor_tipo" class="cajatexto2" style="width:30%">
<%
           ProcTipos PT = new ProcTipos();
           Vector vp = new Vector();
           vp = PT.busquedaProcTipo("","PROC_TCOD ASC");
           for(int int_pos = 0;int_pos < vp.size(); int_pos++){
                SesProcTipos sPT=(SesProcTipos)vp.elementAt(int_pos);
%>
                <option  value='<%=sPT.getProcTipo()%>'><%=sPT.getProcTcod()%></option>
<%
            }
%>
          </Select></div>
        </td>
       </tr>
  <tr>
          <td>&nbsp</td>
          <td>&nbsp</td>
  </tr>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
