<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../../javascript/SelecUtil.js"></script>
</head>
<jsp:useBean id="beanMapeoProcedimientos" class="java.util.HashMap" scope="session"/>
<script language="javascript">
  function abrir_buscador(){
    var retorno=0;
    llamado="buscador_proc_index.htm";
    retorno=window.showModalDialog(llamado,"","dialogWidth:45em;dialogHeight:35em;status=no");
    document.form.procpk.value=retorno;
    document.formulario.procpk.value=retorno;
    document.form.submit();
  }

</script>
<BODY class="mainFondo">
	<%
		MapeoProc mapproc= new MapeoProc();
		Vector vec = new Vector();
                ProcTipos pT= new ProcTipos();
                Vector vT = new Vector();
		String str_codproc = request.getParameter("codproc");
                long l_centro = new Long(request.getParameter("centro")).longValue();
                String str_tipo = request.getParameter("tipo");
                String descprestmapeo = request.getParameter("descproc");
                String str_codprocsescam = "S/D";
                String str_descprocsescam = "S/D";
                String tipo="SD";
		vec = mapproc.busquedaProcMap("PROC_CEN_COD='"+str_codproc+"' AND CENTRO="+l_centro+" AND PROC_CEN_TIPO='"+str_tipo+"'","");
                SesProcMap sPM=(SesProcMap)vec.elementAt(0);
                beanMapeoProcedimientos.put("MAPEOPROCEDIMIENTO",sPM);
                Procedimientos proc = new Procedimientos();
                Vector vd = new Vector();
                vd = proc.busquedaProced("PROC_PK="+sPM.getProcPk(),"");
                if (vd.size()>0){
                  SesProcedimientos sP = (SesProcedimientos)vd.elementAt(0);
                  str_codprocsescam = sP.getProcCod();
                  str_descprocsescam = sP.getProcDescCorta();
                  vT = pT.busquedaProcTipo("PROC_TIPO="+sP.getProcTipo(),"");
                  SesProcTipos sPT=(SesProcTipos)vT.elementAt(0);
                  tipo = sPT.getProcTcod();
                }
                /* Ulizacion de la clase de Persistencia Prestacion en lugar de pasar por
                   parametro la descripcion del procedimiento mapeado.
                vT.clear();
                PrestacionHosp pH = new PrestacionHosp();
                vT = pH.busquedaPrest("trim(codprest)='"+sPM.getProcCenCod().trim()+"' and centro="+l_centro,"");
                if (vT.size()>0){
                  Prestacion p = (Prestacion) vT.elementAt(0);
                  descprestmapeo = p.getDesprest();
                }*/


	%>
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> Editar Mapeo Procedimientos</td>
  </tr>
</table>
<form name="form" action="busquedaPkProcedimiento.jsp" target="oculto1">
<input type="hidden" name="procpk" value="">
</form>
<form name="formulario" action="modificarMapProcedimiento.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width="20%">Tipo/Codigo Sescam:</td>
    <td class= "normal" width="50%"><input type="text" style="width:20%" maxlength=4 name="codprocses" class="cajatexto2" value="<%=tipo%> / <%=str_codprocsescam%>" readonly></td>
    <td class= "normal" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td class= "texto">Procedimiento Sescam: </td>
    <td class= "normal"><input type="text" name="descprocses" class="cajatexto3" value="<%=str_descprocsescam%>" readonly></td>
    <td class= "normal"><a href="#"><img src="../../../../imagenes/lupa_2.gif" border=0 onClick="return abrir_buscador();" alt="Buscador"></a></td>
  </tr>
  <tr>
    <td class= "texto">Centro: </td>
    <td  width="65%"  class= "normal"><div id="divCentro">
      <Select name="centro" class="cajatexto2" style="width:35%" disabled >
<%
           Centros centro = new Centros();
           Vector vc = new Vector();
           vc = centro.busquedaCentro("","CODCENTRO ASC");
           for(int int_pos = 0;int_pos < vc.size(); int_pos++){
                Centrosescam cs=(Centrosescam)vc.elementAt(int_pos);
                if (cs.getCodcentro()==sPM.getCentro()){
%>
                <option  value='<%=cs.getCodcentro()%>' selected><%=cs.getDescentro()%></option>
<%
                }else{
%>
                <option  value='<%=cs.getCodcentro()%>'><%=cs.getDescentro()%></option>
<%
                }
            }
%>
           </Select></div>
    </td>
  </tr>
  <tr>
    <td class= "texto" >Procedimiento Mapeado: </td>
    <td class= "normal" ><input type="text" disabled style="width:12%" maxlength=6 name="codprocmap" class="cajatexto2" value="<%=sPM.getProcCenCod()%>"></td>
  </tr>
  <tr>
     <td class= "texto" >Descripcion: </td>
     <td class= "normal" ><input type="text" disabled name="descprocmap" class="cajatexto3" value="<%=descprestmapeo%>"></td>
  </tr>
  <tr>
    <td class="texto">Tipo Proc.:</td>
    <td class= "normal"><input type="text" name="tipo" class="cajatexto2" style="width:10%" disabled value="<%=sPM.getProcCenTipo()%>"></td>
    <td>&nbsp;</td>
  </tr>
  <input type="hidden" name="procpk" value="">
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
