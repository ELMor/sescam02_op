<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaPacienteBean" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="../../jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../javascript/menus.js"></script>

</head>
<body class="mainFondo" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0">
<!--
<table border="1" cellspacing="0" cellpadding="0" width="100%" align="center">
 <tr>
   <td align="center">
-->
	<table border="0" width="100%">
<%
  Centros cent = new Centros();
  Paciente pacRet=null;
  Vector vp=null;
  ConsultaPacienteBean cp=new ConsultaPacienteBean();
  cp.setCip(request.getParameter("cip"));
  if (request.getParameter("historico").equals("1")){
	cp.setHistorico(true);
  }
  vp=cp.resultado();
  if (vp.size()>0){
      Enumeration e = vp.elements();
      pacRet=(Paciente)e.nextElement();
      }
  else{
    pacRet=new Paciente();
    }
%>
	   <tr style={padding: 0px 0px 0px 0px;}>
	     <td width="15%" class="texto" >Apellidos y Nombre:</td>
       <td width="45%" class="normal" >
         <input class="cajaTexto2" style="width:90%" type="text" ID="txAyN" readonly value="<%=pacRet.fApe1+' '+pacRet.fApe2+','+pacRet.fNomb%>">
       </td>
     	 <td width="15%" class="texto">Fecha de nacimiento:</td>
	     <td width="25%" class="normal">
	       <input class="cajaTexto2" style="width:50%" type="text" ID="txFNac" readonly value="<%=pacRet.fNac%>">
	     </td>
	   </tr>
	</table>
  <table border="0" width="100%">
	   <tr style={padding: 0px 0px 0px 0px}>
       <td width="10%" class="texto">C.I.P.:</td>
	     <td width="25%" class="normal">
	        <input class="cajaTexto2"  type="text" ID="txCIP" readonly value="<%=pacRet.fCip%>">
	     </td>
	     <td width="10%" class="texto">Telefono:</td>
	     <td width="55%" class="normal">
		      <input class="cajaTexto2"  type="text" ID="txTelefono" readonly value="<%=pacRet.fTelef%>">
	     </td>
     </tr>
	</table>
  <table border="0" width="100%">
	   <tr style={padding: 0px 0px 0px 0px;}>
       <td width="10%" class="texto">Domicilio:</td>
	     <td width="35%" class="normal" >
	        <input class="cajaTexto2" style="width:80%"type="text" ID="txDomi" readonly value="<%=pacRet.fDomi%>">
	     </td>
	   <td width="10%" class="texto">Poblacion:</td>
	     <td width="25%" class="normal">
	        <input class="cajaTexto2" type="text" ID="txPobl" readonly value="<%=pacRet.fPobl%>">
	     </td>
	   <td width="10%" class="texto">Provincia:</td>
	   	 <td width="10%" class="normal" >
	        <input class="cajaTexto2" type="text" style="width:100px"ID="txProv" readonly value="<%=pacRet.fProv%>">
	     </td>
     </tr>
  </table>
<!--
    </td>
 </tr>
</table>
-->
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
