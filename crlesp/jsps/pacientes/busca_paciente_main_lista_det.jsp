<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Paciente" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaPacienteBean" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../javascript/menus.js"></script>
<script language="javascript">
function seleccionaFila(filas, id){
    filas[id].className = "filaSelecionada";
    }
function seleccion(pSelCip,historico){
  parent.parent.parent.location='ficha_paciente_index.jsp?cip='+escape(pSelCip)+'&historico='+historico;
}
</script>
</head>
<body id="juli" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name=date>
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
    <table style="width:100%;" align="left" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral2" >
	<table class="centroTabla" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
<%
		String historico="0";
		Paciente pacRet;
		Vector vp;
		boolean bFilaPar=true;
		int idCounter=0;
        long viene_cip = 0;
        if ((request.getParameter("viene_cip") != null) && (!request.getParameter("viene_cip").equals(""))) {
           viene_cip = 1;
        }
        ConsultaPacienteBean cp=new ConsultaPacienteBean();
        if (request.getParameter("historico")!=null){
           cp.setHistorico(true);
           historico="1";
           
        }else{
           cp.setHistorico(false);
        }
		cp.setNombre(request.getParameter("nombre"));
		cp.setApellido1(request.getParameter("apellido1"));
		cp.setApellido2(request.getParameter("apellido2"));
		cp.setCip(request.getParameter("cip"));
        cp.setCentro(request.getParameter("centro"));
  		cp.setHicl(request.getParameter("hicl"));
        cp.setOrden(request.getParameter("orden"));
        cp.SetNegEvento(accesoBeanId.getNegEventoInstance(20));
		vp=cp.resultado();
		if (vp!=null){
%>
                 <script language="javascript">
                    parent.pie.location="../pie_buscador.jsp?estado=Generando resultado";
                 </script>
<%
        if (vp.size()>0){
	  			Enumeration e = vp.elements();
          pacRet = new Paciente();
          String numhc="S/D"; 
	    		while(e.hasMoreElements()) {
	      			pacRet=(Paciente)e.nextElement();
	      			//System.out.println("NHC:"+pacRet.fHicl);
							if (pacRet.fHicl!=0){
								numhc= new Integer(pacRet.fHicl).toString();
							}
							if ((pacRet.fDomi==null)||(pacRet.fDomi.equals(""))) pacRet.fDomi=" ";
							if ((pacRet.fPobl==null)||(pacRet.fPobl.equals(""))) pacRet.fPobl=" ";
							if ((pacRet.fProv==null)||(pacRet.fPobl.equals(""))) pacRet.fProv=" ";
						
	      			if (bFilaPar){
	      			   	bFilaPar=false;
	%>
						<tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('<%=pacRet.fCip%>',<%=historico%>)" style="cursor:hand;">
		<%				}else{
									bFilaPar=true;

	%>
						<tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('<%=pacRet.fCip%>',<%=historico%>)" style="cursor:hand;">
	<%
						}
	%>

				    <td width="20%"  height="12px" name="cip"><%=pacRet.fCip%></td>
				    <td width="10%"  height="12px" name="centro" title="<%=pacRet.fDesCentro%>"><%=pacRet.fCentro%></td>
				    <td width="12%"  height="12px" name="nhc"><%=numhc%></td>
				    <td width="12%"  height="12px" name="fnac"><%=pacRet.fNac%></td>
				    <td width="26%" height="12px" name="nom"><%=pacRet.fApe1+", "+pacRet.fApe2+", "+pacRet.fNomb%></td>
				    <td width="20%"  height="12px" name="dir"><%=pacRet.fDomi+", "+pacRet.fPobl+", "+pacRet.fProv%></td>
			   	</tr>
<%
  				idCounter++;
  				}
                                %>
                                <script language="javascript">
                                     parent.pie.location="../pie_buscador.jsp?estado=Listo.&filas=<%=vp.size()%>";
                                </script>
<%
                                if ((vp.size() == 1) && (viene_cip > 0)) {
                                 %>
                                  <script language="javascript">
                                      parent.parent.parent.location="ficha_paciente_index.jsp?cip="+escape('<%=pacRet.fCip%>')+"&historico="+<%=historico%>;
                                  </script>
                                <%

                                }
                  }else{
%>		   <Script language="javaScript">
                        parent.pie.location="../pie_buscador.jsp?estado=No se han encontrado filas.";
                   </Script>
<%
                  }
		}
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
