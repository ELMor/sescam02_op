<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<%
                /* Variables */
                long l_centro,l_norden;
                l_norden = 0;
                String s_garcq, str_pacnss;
                boolean imprimir=false;
                SesGarantia sG = new SesGarantia();

                /* Recojida de parametros */
                s_garcq = new String((String)beanParamListaSolic.get("GAR_CQ").toString());
                l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
                /* Diferenciacion de registro de LESP o LESPCEX */
                if (s_garcq.equals("Q")) {
                  l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
                } else {
                  l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
                }
                String str_nombre = request.getParameter("nombre");
                String str_dni = request.getParameter("dni");
                String str_letra = request.getParameter("letradni");
                String str_nif = str_dni + str_letra;
                str_pacnss = request.getParameter("nss");
                Garantia gar = new Garantia();
                /* Fin Recojida de Parametros */

                /* Insercion de la solicitud */
                try{
                  if (s_garcq.equals("Q")) {
                    sG.setNorden(l_norden);
                    sG.setCentro(l_centro);
                  }else{
                    sG.setNcita(l_norden);
                    sG.setCega(l_centro);
                  }
                  sG.setGarCq(s_garcq);
                  sG.setGarTipo(1); //Tipo 1: Solicitud
                  sG.setGarEstado(1); //Estado 1: Solicitada
                  sG.setPacNss(str_pacnss);
                  try{
                   gar.SetNegEvento(accesoBeanId.getNegEventoTramInstance(15),61,"DOC01",l_centro,l_norden);
                   gar.insertarGaran(sG,1,accesoBeanId.getUsuario(),str_nombre,str_nif);
                  }catch ( Exception eses ){
                           System.out.println("Excepcion Sescam capturada en guardarSolicitud.jsp");
%>
                          <script language="javascript">
                           alert("<%=eses%>");
                          </script>
<%
                  }
                  imprimir=true;
                }catch(Exception e){
                  System.out.println(" Error " +e+ "al generar la solicitud");
                  imprimir=false;
                }
  %>
<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript">
    var doc="DOC01";
    function ImprimirDoc(){
        //llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&usuario=<%=accesoBeanId.getUsuario()%>';
        llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&garpk=<%=sG.getGarPk()%>&operacion=1&usuario=<%=accesoBeanId.getUsuario()%>';
        window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }
</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
   /* Si la insercion ha sido correcta imprimir documento */
   if (imprimir){
%>
                <script language="javascript">
                        ImprimirDoc();
                        parent.centro.location.href = "lista_solicitudes_main.jsp";
                        parent.botonera.location.href = "lista_solicitudes_down.jsp";
                </script>
<%
     }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>