<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">

      function cancelar(){
                      parent.centro.document.location.href = "lista_solicitudes_main.jsp";
                      parent.botonera.document.location.href = "lista_solicitudes_down.jsp";
      }

      function imprimir(){
                var cq,doc,garpk,operacion,centro,norden;
                resp = confirm("Desea imprimir la Anulación?")
                if (resp == true){
                     doc="DOC04";
                     cq = parent.centro.document.renuncia.gar_cq.value;
                     garpk = parent.centro.document.renuncia.garpk.value;
                     operacion = parent.centro.document.renuncia.op.value;
                     if (cq == "Q") {
                       centro=parent.centro.document.renuncia.centro.value;
                       norden=parent.centro.document.renuncia.norden.value;
                     }else{
                       centro=parent.centro.document.renuncia.centro.value;
                       norden=parent.centro.document.renuncia.ncita.value;
                     }
                     //llamado='../imprime_doc.jsp?centro='+centro+'&norden='+norden+'&cq='+cq+'&documento='+doc+'&usuario=<%=accesoBeanId.getUsuario()%>';
                     llamado='../imprime_doc.jsp?centro='+centro+'&norden='+norden+'&cq='+cq+'&documento='+doc+'&garpk='+garpk+'&operacion='+operacion+'&usuario=<%=accesoBeanId.getUsuario()%>';
                     window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
                     parent.centro.document.location.href = "lista_solicitudes_main.jsp";
                     parent.botonera.document.location.href = "lista_solicitudes_down.jsp";
               }else{
                     parent.centro.document.location.href = "lista_solicitudes_main.jsp";
                     parent.botonera.document.location.href = "lista_solicitudes_down.jsp";
                }
      }
</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="50%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="imprimir();">Imprimir</td>
    <td  width="50%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar();">Volver</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
