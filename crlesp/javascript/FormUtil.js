/****Prototipo de Objeto FormUtil****/

/*
 Funci�n guardar:
		Almacena el contenido del formulario,
		para todo tipo de elementos excepto los botones
*/

function guardarFormulario(form) {
	 var auxObj=null;
	 var elem=null;
	 var j=0; //Contador sobre elementos guardados

	 if (form==null && this.formulario==null) return;
	 if (form!=null) this.setForm(form);

	 for (i=0;i<this.formulario.elements.length;i++) {
				elem=this.formulario.elements[i];

				//Comprobamos que el elemento tiene un nombre asignado
				if (elem.name=="") continue;

				if (this.contenido[i]==null)
					 auxObj=new Object();
				else
					 auxObj=this.contenido[i];

				auxObj["tipo"]=elem.type;
				auxObj["nombre"]=elem.name;

				switch (elem.type) {
					 //Boton Checkbox
					 case "checkbox":
							auxObj["valor"]=elem.checked;
							break;

					 //Campo oculto
					 case "hidden":
							auxObj["valor"]=elem.value;
							break;

					 //Campo contrase�a
					 case "password":
							auxObj["valor"]=elem.value;
							break;

					 //Botones de Radio
					 case "radio":
							auxObj["valor"]=elem.checked;
							break;

					 //Listas de selecci�n simple
					 case "select-one":
							auxObj["valor"]=elem.selectedIndex;
							break;

					 //Listas de selecci�n m�ltiple (por implementar)
					 case "select-multiple":
							auxObj["valor"]=elem.selectedIndex;
							break;

					 //Entrada de texto
					 case "text":
							auxObj["valor"]=elem.value;
							break;

					 //Textarea
					 case "textarea":
							auxObj["valor"]=elem.value;
							break;

					 default:
							break;
				}

				this.contenido[j++]=auxObj;
	 }

	 this.puedeRestablecer=true;

	 if (this.formulario.chbox != null)
	    for (i=0; i < this.formulario.chbox.length; i++) {
	    	this.contchbox[i] = this.formulario.chbox[i].checked;
	    }
}

/*
 Funci�n restablecerFormulario
	 Restablece los valores de un formulario guardado anteriormente
*/
function restablecerFormulario() {
	var auxObj=null;

	if (!this.puedeRestablecer) return;
	for (var i=0;i<this.contenido.length;i++) {
		auxObj=this.contenido[i];

		switch(auxObj["tipo"]) {
					 //Boton Checkbox
					 case "checkbox":
							this.formulario[auxObj["nombre"]].checked=auxObj["valor"];
							break;

					 //Campo oculto
					 case "hidden":
							this.formulario[auxObj["nombre"]].value=auxObj["valor"];
							break;

					 //Campo contrase�a
					 case "password":
							this.formulario[auxObj["nombre"]].value=auxObj["valor"];
							break;

					 //Botones de Radio
					 case "radio":
							this.formulario[auxObj["nombre"]].checked=auxObj["valor"];
							break;

					 //Listas de selecci�n simple
					 case "select-one":
							this.formulario[auxObj["nombre"]].selectedIndex=auxObj["valor"];
							if (this.formulario[auxObj["nombre"]].onchange!=null) this.formulario[auxObj["nombre"]].onchange();
							break;

					 //Listas de selecci�n m�ltiple (por implementar)
					 case "select-multiple":
							this.formulario[auxObj["nombre"]].selectedIndex=auxObj["valor"];
							break;

					 //Entrada de texto
					 case "text":
							this.formulario[auxObj["nombre"]].value=auxObj["valor"];
							break;

					 //Textarea
					 case "textarea":
							this.formulario[auxObj["nombre"]].value=auxObj["valor"];
							break;

					 default:
							break;
				}
	}

	if (this.formulario.chbox != null)
		for (i=0; i < this.formulario.chbox.length; i++) {
			this.formulario.chbox[i].checked = this.contchbox[i];
		}
}

/*
 Funci�n restablecerFormulario
	 Establece el formulario sobre el que se quieren aplicar
	 m�todos de guardar y restablecer
*/
function setFormulario(form) {
	this.formulario=form;
}

/*
 Funci�n borraOpcionesSelect
	 Borra las opciones de una lista desplegable
	 Par�metros:
		 select: Lista que se quiere limpiar  (objeto 'select')
		 index1: indice de la primera opcion que se quiere borrar
		 index2: indice de la ultima opcion que se quiere borrar
*/
function borraOpsSelect(select,min,max) {
	 var index1,index2;
	 if (select.type!='select-one' && select.type!='select-multiple') return;
	 index1=min||0;
	 index2=max||select.options.length-1;
	 for (var i=index2;i>=index1;i--) {
			select.options[i]=null;
	 }
}

/*
 Funci�n borraOpSelect
	 Borra una opcion de una lista desplegable
	 Par�metros:
		 select: Lista desplegable
		 valor: Valor de la opcion que se quiere borrar
*/
function borraOpSelect(select,valor) {
	 if (select.type!='select-one' && select.type!='select-multiple') return;
	 for (var i=0;i<select.options.length;i++) {
			if (select.options[i].value==valor) {
				select.options[i]=null;
				return;
			}
	 }
}

/*
 Funci�n opSelect
	 Crea una opcion en una lista desplegable, insert�ndola al final
	 Par�metros:
		 select: Lista en la que se quiere introducir la opcion (objeto 'select')
		 texto : texto de la opcion (String)
		 valor:  valor que se devuelve al servidor (String)
*/
function opSelect(select,texto,valor,indice) {
	 if (select.type!='select-one' && select.type!='select-multiple') return;
	 for (var i=0; i < select.options.length; i++)
		if (select.options[i].value == valor) return;
	 select.options[indice||select.options.length]=new Option(texto,valor);
}

/*
 Funci�n opsSelect
	 Crea una serie de opciones en una lista desplegable
	 Par�metros:
			select: Lista en la que se quiere introducir las opciones (objeto 'select')
			opciones : Array de cadenas formando parejas {valor,texto}
								 Texto es el texto de la opcion
								 Valor es el valor que se devuelve al servidor
*/
function opsSelect(select,opciones,valorSeleccionado) {
	 var ind;
	 //Comprobamos que se le ha pasado al m�todo un elemento de tipo lista
	 if (select.type!='select-one' && select.type!='select-multiple') return;

	//Resetea el select
	this.borraOpcionesSelect(select);

	 for (var i=0;i<opciones.length;i+=2) {
			this.opcionSelect(select,opciones[i+1],opciones[i]);
	 }

	 if (valorSeleccionado != null) {
	 		ind = this.getIndice(select,valorSeleccionado);
      select.selectedIndex = (ind == -1) ? 0 : ind;
	 } else {
      select.selectedIndex = 0;
	 }
}

/*
 Funci�n retIndex
	Devuelve el �ndice de la opci�n con un valor determinado
	 Par�metros:
		 select: Lista desplegable
		 valor: Valor de la opcion cuyo �ndice se busca.
*/
function retIndex(select,valor) {
	for (var i=0; i<select.length;i++) {
		if (select.options[i].value == valor)
			return i;
	}
	return -1;
}



/*
	Funci�n validaNumero
	Valida si una cadena de texto contiene caracteres validos para
	formar un numero
*/

function validaNumero(numero) {
	var num = "0123456789+-.,";  // N�meros v�lidos

	/* Ciclar sobre el contenido restante. Si alg�n car�cter no es un n�mero,
      establecer false como valor de vuelta. */
   for (var intLoop = 0; intLoop < numero.length; intLoop++){
   	if ( num.indexOf(numero.charAt(intLoop)) == -1 ) {
     		return false;
     	}
   }
   return true;
};



/*
	Comprueba si una cadena de texto es un n�mero v�lido
*/
function esNumero(texto) {

	if( !validaNumero(texto) ){
		return false;
	}
	return (!isNaN(parseInt(texto, 10)));
}

/*
	Comprueba si una cadena de texto es una direcci�n web v�lida
*/
function esWeb(s) {
    var i = 1;
    var sLength = s.length;


    while ((i < sLength) && (s.charAt(i) != "."))
    { i++
    }

    if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
    else return true;
}

/*
	Valida una direcci�n web
*/
function validacionWeb(web) {

	if (web.value.substring(0,7) == "http://")
		web.value = web.value.substring(7,web.value.length);

	if (esWeb(web.value)) {
		return true;
	} else {
		alert ("La direcci�n web no es v�lida.");
		return false;
	}
}

/*
	Comprueba si una cadena de texto es un e-mail v�lido
*/
function esEmail(s) {
    var i = 1;
    var sLength = s.length;
    while ((i < sLength) && (s.charAt(i) != "@"))
    { i++
    }

    if ((i >= sLength) || (s.charAt(i) != "@")) return false;
    else i += 2;

    while ((i < sLength) && (s.charAt(i) != "."))
    { i++
    }

    if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
    else return true;
}

/*
	Valida un e-mail
*/
function validacionEmail(email) {
	if (esEmail(email.value)) {
		return true;
	} else {
		alert ("El e-mail no es v�lido.");
		return false;
	}
}

/*
	Valida una fecha
*/
function validacionFecha (dia, mes, anio)
{
	var meses = new Array(31,29,31,30,31,30,31,31,30,31,30,31);
	var nmes, ndia, nanio;

	if ((!esNumero(dia.value)) || (!esNumero(mes.value)) || (!esNumero(anio.value) || (anio.value.length < 4)))
		return false;

	nmes = parseInt(mes.value);
	ndia = parseInt(dia.value);
	nanio = parseInt(anio.value);

	if (nmes > 12){
		return false;
	}

	if (ndia > meses[nmes-1]){
		return false;
	}

	// Validacion del a�o bisiesto
	if ((nmes==2) && (ndia==29)){
		if (((nanio % 4 == 0) &&
				(nanio % 100 != 0)) ||
				(nanio % 400 == 0)) {
			return true;
		} else {
			return false;
		}
	}

	return true;
}

function formatoFecha (stDia, stMes, stAnio)
{
	var dia = stDia.value == "dd" ? "" : stDia.value;
	var mes = stMes.value == "mm" ? "" : stMes.value;
	var anio = stAnio.value == "aaaa" ? "" : stAnio.value;
	var strFecha = "";

	if (dia=="" && mes=="" && anio=="") return "";

	strFecha = (dia.length < 2 ? "0" + dia : dia) + "/";
	strFecha += (mes.length < 2 ? "0" + mes : mes) + "/";
	strFecha += anio;

	return strFecha;
}


/*
	valida que la fecha tenga el formato dd/mm/aaaa
*/
function estaBienFecha(fecha) {

	if (fecha!=""){
		if(fecha.length!=10){  //Comprobar si se han introducidos todos los caracteres
			return false
		}
		else {
			dia = fecha.substring(0,2);
			separador1 = fecha.substring(2,3);
			mes = fecha.substring(3,5);
			separador2 = fecha.substring(5,6);
			anio = fecha.substring(6,10);

			if( (separador1 != "/") || (separador2 != "/") ||
				!esNumero(dia) || !esNumero(mes) || !esNumero(anio) ||
				 (anio.length < 4) ){
				return false
			}
			else {
				var meses = new Array(31,29,31,30,31,30,31,31,30,31,30,31);
				if (mes > 12)	return false;
				if (dia > meses[mes-1]) return false;

				// Validacion del a�o bisiesto
				if ((mes==2) && (dia==29)){
					if (((anio % 4 == 0) && (anio % 100 != 0)) || (anio % 400 == 0))
						return true;
					else 	return false;
				}
				return true;
			}
		}
	}
	else return false;
}

/*
	Limpia el contenido del campo actual si su valor es igual a texto
*/
function limpiaCampo(campo, texto){
	if(campo.value == texto) campo.value = "";
}

/*
        Abrir una ventana del explorador
*/
function abrirVentanaDocumento(documento, opciones)
{
        op1 = "status=yes,toolbar=no,scrollbars=no,menubar=no,width=790,height=550,dependent=no,resizable=no,top=0,left=0";
        op2 = "status=yes,toolbar=yes,scrollbars=yes,menubar=no,width=790,height=550,dependent=no,resizable=yes,top=0,left=0";
        op3 = "status=yes,toolbar=no,scrollbars=yes,menubar=no,width=790,height=550,dependent=no,resizable=no,top=0,left=0";
        op4 = "status=yes,toolbar=no,scrollbars=yes,menubar=yes,width=790,height=550,dependent=no,resizable=yes,top=0,left=0";

        if (opciones == null) {
                opciones = op4;
        }
        else if (!isNaN(opciones)) {
                opciones = eval("op" + opciones);
        }

        v = window.open(documento, "", opciones);

}

/*CONSTRUCTOR DEL OBJETO*/
function FormUtil(form) {
	//Propiedades
	this.contenido=new Array();
	this.contchbox=new Array();
	this.formulario=form;
	this.puedeRestablecer=false;

	//Metodos
	this.guardar=guardarFormulario;
	this.restablecer=restablecerFormulario;
	this.setForm=setFormulario;
	this.opcionSelect=opSelect;
	this.opcionesSelect=opsSelect;
	this.borraOpcionSelect=borraOpSelect;
	this.borraOpcionesSelect=borraOpsSelect;
	this.getIndice=retIndex;
	this.validaWeb=validacionWeb;
	this.validaEmail=validacionEmail;
	this.validaFecha=validacionFecha;
	this.formateaFecha=formatoFecha;
	this.bienFecha=estaBienFecha;

}

